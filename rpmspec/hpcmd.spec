# RPM spec file for hpcmd

%define propername hpcmd
%define my_prefix /opt/hpcmd
%define _unpackaged_files_terminate_build 0


Name:           %{propername}
Version:        0.5.0
Release:        0
Summary:        HPC monitoring daemon
License:        MIT
Group:          System/Monitoring
Url:            https://gitlab.mpcdf.mpg.de/mpcdf/hpc_monitoring
Source0:        setuptools-39.0.1.zip
Source1:        lockfile-0.12.2.tar.gz
Source2:        docutils-0.14.tar.gz
Source3:        python-daemon-2.1.2.tar.gz
Source4:        hpcmd.tar.gz
Source5:        pbr-4.0.2.tar.gz
Source6:        PyYAML-3.12.tar.gz
BuildRoot:      %{_tmppath}/%{propername}-%{version}-build
BuildRequires:  python
BuildRequires:  unzip


%description
MPCDF HPC monitoring daemon


%prep
unzip %{SOURCE0}
tar -xzf %{SOURCE1}
tar -xzf %{SOURCE2}
tar -xzf %{SOURCE3}
tar -xzf %{SOURCE4}
tar -xzf %{SOURCE5}
tar -xzf %{SOURCE6}


%build
# # nothing to build here


%install
mkdir -p %{buildroot}%{my_prefix}/lib/python2.7/site-packages
# extend PYTHONPATH during installation
export PYTHONPATH=%{buildroot}%{my_prefix}/lib/python2.7/site-packages
(
    cd PyYAML-*
    # we need to provide install-lib because otherwise it ends up in lib64
    python setup.py --without-libyaml install --prefix=%{my_prefix} --root=%{buildroot} --install-lib=%{my_prefix}/lib/python2.7/site-packages
)
# install all the packages in the right order
for PACKAGE in setuptools pbr lockfile docutils python-daemon hpcmd
do
    (
        cd $PACKAGE-*
        python setup.py install --prefix=%{my_prefix} --root=%{buildroot}
    )
done
# disarm unwanted executables
rm -vf %{buildroot}%{my_prefix}/bin/{easy_install*,rst*,pbr}
# enable the systemd service
(
    cd hpcmd-*
    mkdir -p %{buildroot}/etc/systemd/system
    cp systemd/hpcmd.service %{buildroot}/etc/systemd/system/hpcmd.service
)

chmod -Rc a+rX %{buildroot}%{my_prefix}


%files
%defattr (-, root, root)
%dir %{my_prefix}/..
%dir %{my_prefix}
%{my_prefix}/*

%dir /etc
%dir /etc/systemd
%dir /etc/systemd/system
/etc/systemd/system/hpcmd.service


%clean
rm -rf %{buildroot}


%pre
if [ "$1" == "1" ]; then
    # installation
    true
elif [ "$1" == "2" ]; then
    # upgrade
    systemctl stop hpcmd
fi


%post
mkdir -p /var/lib/hpcmd
ln -fs /opt/hpcmd/bin/hpcmd         /usr/local/bin/hpcmd
ln -fs /opt/hpcmd/bin/hpcmd_slurm   /usr/local/bin/hpcmd_slurm 
ln -fs /opt/hpcmd/bin/hpcmd_suspend /usr/local/bin/hpcmd_suspend 
systemctl daemon-reload
systemctl enable hpcmd
systemctl start hpcmd


%preun
if [ "$1" == "0" ]; then
    # uninstallation
    systemctl stop hpcmd
    systemctl disable hpcmd
elif [ "$1" == "1" ]; then
    # upgrade
    true
fi


%postun
systemctl daemon-reload
if [ "$1" == "0" ]; then
    # uninstallation
    rm -f /usr/local/bin/hpcmd /usr/local/bin/hpcmd_slurm /usr/local/bin/hpcmd_suspend
    rm -f /var/run/hpcmd.pid
    rm -f /var/log/hpcmd.yaml /var/log/hpcmd_stderr.log /var/log/hpcmd_stdout.log
    rm -rf /var/lib/hpcmd
elif [ "$1" == "1" ]; then
    # upgrade
    true
fi


%changelog

