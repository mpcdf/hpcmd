#!/bin/bash
#
# Create a virtual environment for development
#

VENV=$HOME/venv_hpcmd
mkdir -p $VENV

python3 -m venv $VENV
source $VENV/bin/activate

python3 -m pip install --upgrade pip
pip3 install --upgrade setuptools
pip3 install --upgrade wheel

pip3 install -e '.[daemon,test,doc]'

