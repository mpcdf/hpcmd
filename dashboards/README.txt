Dashboards for Splunk in XML format:

* roofline view
* detailed job view
* aggregated job view
* top applications
* proper use of partition resources
