# Changelog

All notable changes to hpcmd will be documented in this file.

## [0.8.1] - 2025-02-10

* revisit per-numa-domain memory monitoring
* add Viper-CPU config files
* improve cleanup of potential remnants from previous jobs on the same node

## [0.8.0] - 2024-09-24

* support for flops on AMD ZEN4 CPUs, mem-bw yet to be done
* support for tracing compiled Python modules

## [0.7.2] - unreleased

* make 'python-daemon' an optional dependency

## [0.7.1] - 2022-04-28

* fix to improve the detection of the flavor of the GPU nodes on Raven
* move temporary directory from /tmp to /var/tmp to better handle cgrouped Slurm jobs

## [0.7.0] - 2021-05-14

* Implement new GPU monitoring capabilities via NVIDIA DCGM
* Refactor GPU monitoring into nvgpu module, using `nvidia-smi` and `dcgmi` simultaneously, implement spawn-join model
* Refactor perf module to be called in a spawn-join-like fashion (same as nvgpu)
* Introduce new sleep module to replace perf as the blocking entity during the epoch loop
* Improve detection of uncore events related to memory traffic
* Minor fixes and improvements throughout the code

## [0.6.9] - unreleased

* Adapt to MPCDF Raven-final
* Add support for Intel Ice Lake via raw counter ids
* Improve autodetection of memory channels, fix on Ice Lake

## [0.6.8] - 2021-01-14

* Add extensive configurable monitoring of the environment of user processes
* Fix bug in the parsing of nvidia-smi output, leading to wrong mem usage numbers

## [0.6.7] - 2020-09-23

* Restrict reporting of 'libs' to the headnode to avoid unnecessary traffic
* Improve job change detection and handling
* Minor improvements w/r/t code quality and performance

## [0.6.6] - 2020-09-11

* Adapt to MPCDF RAVEN

## [0.6.5] - 2020-05-01

* Fix bug that had consumed unnecessary cpu time on shared nodes.

## [0.6.4] - 2020-03-06

* Add reporting of the shared objects used by the user application.
* Add reporting of the loaded environment modules seen by the application.

## [0.6.3] - 2020-01-16

* Add monitoring of the system load
* Add environment variable to allow user-specific paths
* Fix SLURM job change detection for the transition from job to no-job
* Fix process monitoring by PID

## [0.6.2] - 2019-12-20

* Making trigger directory and epilog files configurable

## [0.6.1] - 2019-12-20

* Minor fix regarding startup with slurm.conf
* Better slurm job change detection
* Cleaner logs for physics

## [0.6.0] - 2019-10-30

* Complete rewriting of the tools, now using polymorphism and inheritance
* Adding GPFS opens and closes to the job_summary (for "black sheep" analysis)
* New tool 'physics' for measuring node power (although still disabled on non-draco nodes, as the output on other machines is different)
* Measuring GPU power
* Autodetection of fp and uncore events
* Allowing multiple network sources
* Bugfixes regarding epilog VEC_RATIO and gpfs values
* Bugfix regarding parsing the executable name
* Major code cleanup

## [0.5.4] - 2019-09-02

* fix high CPU utilization bug on blacklisted nodes
* improve laucher code to work directly from the source tree
* laucher code now always uses the interpreter hpcmd has been installed with

## [0.5.3] - 2019-07-19

* disable epochs for user mode
* fix log message of measurement interval by introducing loop_dt
* better managing of shared nodes
* fixing AVG_GPU error

## [0.5.1, 0.5.2] - 2019-06-14

* fix job change detection
* support non-srun single-node jobs
* major code refactoring and cleaning of log.py

## [0.5.0] - 2019-04-10

* fix detection of the number of local jobs
* clean up command line options
* improve util.run
* replace top with ps for getting the executable name more efficiently
* disable memory consumption per socket
* add proper job-change detection
* add gpu prolog-like message
* add new tests, unit tests, improve CI
* add system module to report CPU time and RSS of system services
* extend documentation significantly

## [0.4.7] - 2019-02-26

* fix uncore events for MEM-BW on Skylake/COBRA
* re-enable OPA monitoring on COBRA

## [0.4.6] - 2019-02-06

* fix thread leak for interrupt watchdog
* fix prolog and epilog information handling
* disable OPA monitoring for the moment on COBRA
* add suspend verification test scripts

## [0.4.5] - 2019-01-15

* version for internal testing on Cobra
* various bugfixes

## [0.4.4] - 2018-12-20

* many Splunk performance improvements
* adding "cores", and editing GF and AI in the epilog msg
* fixing array jobs epilog typo that led to bigger issues

## [0.4.3] - 2018-12-19

### Added

* obtain node specs from slurm.conf, support heterogeneous clusters
* enable GPU monitoring, auto-disable if no GPU is available
* exemplary url generation in epilog, for future PDF reports

### Changed

* change how slurm, exe, job_start messages are printed

### Removed


## [0.4.2] - 2018-12-12

### Added

* GroupID info in epilog
* short prolog-like messages by hpcmd for the first job sample
* more tests

### Changed

* few fixes in epilog
* changing op.py into primitives, and adding few functions there

### Removed


## [0.4.1] - 2018-12-09

### Added

* save averaged information on FLOPS and ALGO-INT to suspend trigger file
* save exe and nnodes info to suspend trigger file

### Changed

* clean internal handling of SLURM job information
* minor code refactoring

### Removed


## [0.4.0] - 2018-10-25

### Added

* new hierarchical YAML input files and their parsing
* support for gpfs counters

### Changed

* major configuration code restructuring, not only related to the YAML change
* improving gathering of slurm info
* improving documentation in general
* improving performance of Splunk queries

### Removed

* process and thread files, now they are merged into software

## [0.3.3] - 2018-09-21

### Added

### Changed

* change link directory
* improve Splunk queries for Cluster form input

### Removed


## [0.3.2] - 2018-09-20

### Added

* support for the COBRA Skylake HPC system

### Changed

* YAML config file format

### Removed

* CFG config file format
* boolean string evaluations


## [0.3.1] - 2018-07-27

### Added

### Changed

* various bug fixes

### Removed


## [0.3.0] - 2018-06-28

### Added

* suspend synchronization mechanism between systemd service and user daemon
* fix local determination of SLURM jobid to work on any node

### Changed

### Removed


## [0.2.1] - 2018-06-25

### Added

* suspend file correctly interrupts any running perf instance
* add hpcmd_suspend and hpcmd_slurm shell wrapper scripts

### Changed

* replaced sacct-based job information determination with a local method

### Removed



## [0.2.0] - 2018-06

### Added

* trigger suspend via suspend file

### Changed

* improved logging messages and optional verbosity

### Removed



## [0.1.0] - 2018-05

### Added

* first version correctly running and sampling on Broadwell nodes

### Changed

### Removed
