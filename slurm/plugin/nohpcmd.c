/**
 * Copyright (c) 2021, Max Planck Computing and Data Facility, Garching, Germany
 *
 * hpcmd.c   SPANK plugin to enable and disable hpcmd monitoring, based
 *           on the nvmps.c of Mykola Petrov. Written by Carlos Lopez.
 *
 * Compile using the provided Makefile, or directly via
 *           gcc -fPIC -shared -o nohpcmd.so nohpcmd.c
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <unistd.h>
#include <slurm/spank.h>

#define LOCK_DIR "/var/tmp/hpcmd"
#define LOCK_FILE LOCK_DIR "/suspend"

#ifdef VERBOSE
  #define SLURM_INFO(...) slurm_info(__VA_ARGS__)
  #define SLURM_ERROR(...) slurm_error(__VA_ARGS__)
  #define SLURM_SPANK_LOG(...) slurm_spank_log(__VA_ARGS__)
#else
  static void _slurm_quiet(char * msg, ...) { }
  #define SLURM_INFO(...) _slurm_quiet(__VA_ARGS__)
  #define SLURM_ERROR(...) _slurm_quiet(__VA_ARGS__)
  #define SLURM_SPANK_LOG(...) _slurm_quiet(__VA_ARGS__)
#endif

SPANK_PLUGIN(nohpcmd, 1);
static int nohpcmd = 0;
static int nohpcmd_flag = 0;
#define NOHPCMD_ENV_VAR "SPANK_NOHPCMD"

static int _nohpcmd_callback(int val, const char *optarg, int remote);
static int _create_lock_dir(void);
static int _create_lock_file(void);
static int _delete_lock_file(void);

struct spank_option spank_options[] = {
    {
        "no-hpcmd",
        NULL,
        "Disable HPC monitoring for job",
        0,
        0,
        _nohpcmd_callback
    },
    SPANK_OPTIONS_TABLE_END
};

int slurm_spank_init(spank_t sp, int ac, char **av) {
  int sc = spank_context();
  if (sc == S_CTX_ALLOCATOR || sc == S_CTX_REMOTE || sc == S_CTX_LOCAL) {
    spank_option_register(sp, spank_options);
  }
  return ESPANK_SUCCESS;
}

int slurm_spank_init_post_opt(spank_t sp, int ac, char **av)
{
  if (spank_context() == S_CTX_ALLOCATOR) {
    if (nohpcmd_flag == 1)
      spank_setenv(sp, NOHPCMD_ENV_VAR, "1", 1);
  }
  return ESPANK_SUCCESS;
}

int slurm_spank_task_init(spank_t sp, int ac, char **av) {
  char hostname[HOST_NAME_MAX];
  uint32_t stepid, localtaskid;
  const size_t buf_sz = 64;
  char buf[buf_sz];

  if (!nohpcmd_flag && (!spank_getenv(sp, NOHPCMD_ENV_VAR, buf, buf_sz-1))) {
    SLURM_INFO("do not have mps flag, will use the one from the environment");
    nohpcmd = 1;
  }

  if (nohpcmd) {
    memset(hostname, '\0', HOST_NAME_MAX);
    gethostname(hostname, HOST_NAME_MAX);

    if (spank_get_item(sp, S_TASK_ID, &localtaskid) != ESPANK_SUCCESS) {
      SLURM_ERROR("Failed to get local task id from SLURM");
      return ESPANK_ERROR;
    }

    if (spank_get_item(sp, S_JOB_STEPID, &stepid) != ESPANK_SUCCESS) {
      SLURM_ERROR("Failed to get job step id from SLURM");
      return ESPANK_ERROR;
    }

    if ((localtaskid == 0) && (stepid >= 0 && stepid < 4294967294)) {
      slurm_spank_log("Disabling hpcmd on %s", hostname);

      if (_create_lock_dir() == 0)
        SLURM_ERROR("Failed to create lock directory %s", LOCK_DIR);

      if (_create_lock_file() == 0)
        SLURM_SPANK_LOG("Lock file %s could not be created", LOCK_FILE);
      else
        SLURM_SPANK_LOG("Lock file %s created", LOCK_FILE);
    }
  }

  return ESPANK_SUCCESS;
}

int slurm_spank_task_exit(spank_t sp, int ac, char **av) {
  char hostname[HOST_NAME_MAX];
  uint32_t stepid, localtaskid;

  if (nohpcmd) {
    memset(hostname, '\0', HOST_NAME_MAX);
    gethostname(hostname, HOST_NAME_MAX);

    if (spank_get_item(sp, S_TASK_ID, &localtaskid) != ESPANK_SUCCESS) {
      SLURM_ERROR("Failed to get local task id from SLURM");
      return ESPANK_ERROR;
    }

    if (spank_get_item(sp, S_JOB_STEPID, &stepid) != ESPANK_SUCCESS) {
      SLURM_ERROR("Failed to get job step id from SLURM");
      return ESPANK_ERROR;
    }

    if ((localtaskid == 0) && (stepid >= 0 && stepid < 4294967294)) {
      SLURM_SPANK_LOG("Enabling hpcmd on %s", hostname);
      _delete_lock_file();
    }
  }

  return ESPANK_SUCCESS;
}

static int _nohpcmd_callback(int val, const char *optarg, int remote) {
  nohpcmd = 1;
  nohpcmd_flag = 1;
  return ESPANK_SUCCESS;
}

static int _create_lock_dir() {
  int success = 0;
  struct stat sb;
  if (stat(LOCK_DIR, &sb) == 0 && S_ISDIR(sb.st_mode)) {
    success = 1;
  } else {
    if (mkdir(LOCK_DIR, 0) != -1) {
      mode_t target_mode = 0777;
      chmod(LOCK_DIR, target_mode);
      success = 1;
    }
  }
  return success;
}

static int _create_lock_file() {
  int pfd;
  int success = 0;
  if ((pfd = open(LOCK_FILE, O_WRONLY | O_CREAT, 0)) == -1) {
    SLURM_ERROR("Cannot open %s", LOCK_FILE);
  } else {
    if (close(pfd) == 0) {
      mode_t target_mode = 0666;
      chmod(LOCK_FILE, target_mode);
      success = 1;
    }
  }
  return success;
}

static int _delete_lock_file() {
  return remove(LOCK_FILE);
}
