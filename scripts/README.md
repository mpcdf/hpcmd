The main program 'hpcmd.exe' was relocated closer to the hpcmd Python module,
also in order to generate Sphinx documentation.  To be able to call it via
'hpcmd' directly from here, we added the symlink.
