#!/usr/bin/env python

# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

import os
import subprocess as sub

# check if this script is executed from the packages root directory
assert(os.path.isfile("./scripts/update_git_hash.py"))

package_name = "hpcmd"

try:
    cmd = "git describe --all --long --dirty --tags".split()
    raw = sub.check_output(cmd).rstrip().split("/")[1]
except Exception:
    raw = "undefined"
with open("./" + package_name + "/githash.py", "w") as fp:
    fp.write("human_readable = \"" + raw + "\"\n")
