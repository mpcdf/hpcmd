#!/usr/bin/env python
import sys, json

MAX_CPU_GFLOPS = 5530
MAX_CPU_MEMBW = 320
MAX_GPU_MEMBW = 1555
MAX_GPU_FP32_GFLOPS = 19.5 * 1e5
MAX_GPU_FP64_GFLOPS = 9.7 * 1e5
IO_THRESHOLD = 7.6 *1e5


def cal_cpu_perf(GF, AI):
    AI_boundary = MAX_CPU_GFLOPS / MAX_CPU_MEMBW
    membw = GF/AI
    if (AI >= AI_boundary):
        return MAX_CPU_GFLOPS/GF
    else:
        return MAX_CPU_MEMBW/membw


def get_gpu_grade(gpu_util):
    # temp
    if gpu_util < 30:
        return "Terrible"
    elif gpu_util < 50:
        return "Bad"
    elif gpu_util < 70:
        return "Average"
    elif gpu_util < 90:
        return "Good"
    elif gpu_util <= 100:
        return "Perfect"


def cpu_grade(BIND_DIR, exe, cpu_perf):
    with open(f'{BIND_DIR}/perf_quantile_data.json', 'r') as f:
        perf_quantile_data = json.load(f)

    def get_percentile_rank(perf_factor, quantiles):
        """Determine the percentile rank for a given perf_factor."""
        for percentile, value in quantiles.items():
            if perf_factor <= value:
                return percentile
        return "Top 100%"

    overall_rank = get_percentile_rank(cpu_perf, perf_quantile_data['overall'])
    
    if exe in perf_quantile_data['exe_groups']:
        group_rank = get_percentile_rank(cpu_perf, perf_quantile_data['exe_groups'][exe])

        group_rank_percentage = int(group_rank.split()[1].replace("%", ""))
    
        if group_rank_percentage <= 50 or cpu_perf <= 11:
            return print(f"Your job utilized CPU resources well. Overall, it is in the {overall_rank} and in your group, it’s in the {group_rank}")
        else:
            return print(f"Your job did not utilize CPU resources well. Overall, it is in the {overall_rank}, and in your group, it’s in the {group_rank}. Action is needed.")
    
    else:
        if cpu_perf <= 11:
            return print(f"Your job utilized CPU resources well. Overall, it is in the {overall_rank}")
        else:
            return print(f"Your job did not utilize CPU resources well. Overall, it is in the {overall_rank}. Action is needed.")


def io_warning(io):
    if io >= IO_THRESHOLD:
        return print("Your job’s I/O operations are unusually high and may be affecting performance.")
    else:
        return 


def main(BIND_DIR, OUT_DIR, exe, AI, GF, AVG_O_PTMP, AVG_O_U, AVG_GPU):

    GPUS_USED = (AVG_GPU > 0)
    IO = AVG_O_PTMP + AVG_O_U

    with open(f'{OUT_DIR}/performance_warning.log', 'w', encoding="utf-8") as f:
        sys.stdout = f
        cpu_grade(BIND_DIR, exe, cal_cpu_perf(GF, AI))

        if GPUS_USED:
            gpu_grade = get_gpu_grade(AVG_GPU)
            print(f"Your job utilized GPU resources: {gpu_grade}\n")

        io_warning(IO)
    return


if __name__ == "__main__":
    if len(sys.argv) != 9:
        print("Usage: perf_out.py BIND_DIR OUT_DIR exe AI GF AVG_O_PTMP AVG_O_U AVG_GPU")
        sys.exit(1)

    BIND_DIR = sys.argv[1]
    OUT_DIR = sys.argv[2]
    exe = sys.argv[3]
    AI = float(sys.argv[4])
    GF = float(sys.argv[5])
    AVG_O_PTMP = float(sys.argv[6])
    AVG_O_U = float(sys.argv[7])
    try:
        AVG_GPU = float(sys.argv[8])
    except (ValueError, TypeError):
        AVG_GPU = -1

    main(BIND_DIR, OUT_DIR, exe, AI, GF, AVG_O_PTMP, AVG_O_U, AVG_GPU)