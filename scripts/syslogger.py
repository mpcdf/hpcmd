#!/usr/bin/env python

# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Log to syslog, similarly to the `logger` command, however,
with secure support to add content from a file to the log line.
"""

import syslog
import argparse


parser = argparse.ArgumentParser(description='job_summary logger for hpcmd')
parser.add_argument('-d', '--debug', action='store_true',
                    help='print the message to stdout, not to syslog')
parser.add_argument('-f', '--file', type=str, metavar='FILENAME',
                    help='read the first line from file and append it to the message')
parser.add_argument('-t', '--tag', type=str, metavar='SYSLOG_TAG',
                    help='specify the syslog tag')
parser.add_argument('values', metavar='V', type=str, nargs='*',
                    help='positional parameters to be logged')
args = parser.parse_args()


# 1) add positional parameters to the message
if args.values:
    msg = " ".join(args.values)
else:
    msg = ""


# 2) add file content to the message
line = ""
if args.file:
    try:
        with open(args.file, 'r') as fp:
            line = fp.readline()
    except Exception as e:
        pass
    else:
        msg = " ".join((msg, line))


# 3) write message to syslog, incl. an optional tag
if args.debug:
    print(msg)
else:
    if args.tag:
        syslog.openlog(args.tag)
    syslog.syslog(msg)
