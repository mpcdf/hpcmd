# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

# Epilog script for hpcmd, to be sourced into the system-wide epilog as follows:
#
# HPCMD_EPILOG="/opt/hpcmd/bin/hpcmd_slurm_epilog.sh"
# if [ -e $HPCMD_EPILOG ]; then
#     source $HPCMD_EPILOG
# fi
#
(
    SYSLOG_TAG="hpcmd_slurm_epilog.sh"
    HPCMD_CHECKPOINT_TAG="job_summary"
    HPCMD_TMP_DIR=/var/tmp/hpcmd

    if [ x"${SLURM_JOB_ID}" == x"" ]; then
        exit
    else
        # only output syslog line if triggered by hpcmd daemon
        if [ -e ${HPCMD_TMP_DIR}/epilog ]; then

            # --- Code adapted from the files <prolog.sh, resource_summary.bash, epilog.sh>
            #     Credit: 2018 Renate Dohmen, Lorenz Huedepohl, MPCDF ---
            JOB_INFO=$(scontrol -o show job ${SLURM_JOB_ID} | grep "\<JobId=${SLURM_JOB_ID}")
            # substitute blanks with newlines
            JOB_INFO="${JOB_INFO// /
}"

            function value() {
                grep "^$1" <<< "$JOB_INFO" | cut -d= -f2
            }

            JOBID="None"
            # in case of array jobs, want to get the nicely formatted jobid
            if [ $(value ArrayJobId) ]; then
                ARRAY_JOB_ID=$(value ArrayJobId)
                ARRAY_TASK_ID=$(value ArrayTaskId)
                JOBID="${ARRAY_JOB_ID}_${ARRAY_TASK_ID}"
            else
                JOBID="$SLURM_JOB_ID"
            fi

            GROUP_ID=$(value GroupId)
            PARTITION=$(value Partition)

            EXIT_CODE=$(value ExitCode)

            JOB_START=$(value StartTime)
            JOB_START_IN_SEC=$(date -d $JOB_START +%s)
            JOB_START="${JOB_START//T/ }"
            JOB_START=$(date -d "$JOB_START")

            JOB_END=$(value EndTime)
            JOB_END_IN_SEC=$(date -d $JOB_END +%s)
            JOB_END="${JOB_END//T/ }"
            JOB_END=$(date -d "$JOB_END")

            ELAPSED=$(awk 'BEGIN{print '$JOB_END_IN_SEC'-'$JOB_START_IN_SEC'}')

            JOB_NAME=$(value JobName)

            # Additional info from sacct
            JOB_INFO2=$(sacct --format=JobIDRaw,MaxRSS,Timelimit -j ${SLURM_JOB_ID})
            # Get MaxRSS reported by slurm
            MAXRSS=$(echo "${JOB_INFO2}" | grep "${SLURM_JOB_ID}\." | awk '{print $2}' | sed 's/.$//' | awk 'BEGIN {maxval=0} {for(i=1;i<=NF;i++) if($i>maxval) maxval=$i;}; END { print maxval;}')
            MAXRSS=$(($MAXRSS / 1024))
            # Count number of jobsteps and print that info as well
            NJOBSTEPS=$(echo "${JOB_INFO2}" | grep "${SLURM_JOB_ID}\.[0-9]" | wc -l)
            # Time reserved by the job
            TIMELIMIT=$(echo "${JOB_INFO2}" | grep "${SLURM_JOB_ID} " | awk '{print $2}')

            # --- log values plus the content of the epilog trigger file ---

            # determine the directory of this script in a reliable way in all situations
            BINDIR=$(dirname $(readlink -f $BASH_SOURCE))

            # syslogger.py provides the same functionality as logger, but opens and reads the epilog file explicitly
            ${BINDIR}/syslogger.py \
                -t ${SYSLOG_TAG} \
                -f ${HPCMD_TMP_DIR}/epilog \
                HPCMD_CHECKPOINT=\"$HPCMD_CHECKPOINT_TAG\" \
                jobid=\"$JOBID\" \
                userid=\"$SLURM_JOB_USER\" \
                groupid=\"$GROUP_ID\" \
                partition=\"$PARTITION\" \
                exit_code=\"$EXIT_CODE\" \
                jobstart=\"$JOB_START_IN_SEC\" \
                jobend=\"$JOB_END_IN_SEC\" \
                elapsed=\"$ELAPSED\" \
                maxrss=\"$MAXRSS\" \
                njobsteps=\"$NJOBSTEPS\" \
                timelimit=\"$TIMELIMIT\"

            # trigger the generation of a PDF report file, derive cluster id from hostname
            # because CLUSTER might not be set (depending on the shell)
            HOSTID=`hostname`

            if [[ $HOSTID == talos* ]]
            then
                CLUSTER=talos
            elif [[ $HOSTID == ada* ]]
            then
                CLUSTER=ada
            elif [[ $HOSTID == rav* ]]
            then
                CLUSTER=raven
            elif [[ $HOSTID == vip* ]]
            then
                CLUSTER=viper
            fi

            # get 'nnodes', 'cores' and 'iter' from the epilog file
            EPILOG=$(cat ${HPCMD_TMP_DIR}/epilog | tr " " "\n" | tr -d '"')
            NNODES=$(grep "^nnodes" <<< "${EPILOG}" |  cut -d= -f2)
            CORES=$(grep "^cores" <<< "${EPILOG}" |  cut -d= -f2)
            ITER=$(grep "^iter" <<< "${EPILOG}" |  cut -d= -f2)

# Metrics required for perf_out.py, disabled until final testing has passed.
#
#            exe=$(grep "^exe" <<< "${EPILOG}" |  cut -d= -f2)
#            AI=$(grep "^AI" <<< "${EPILOG}" |  cut -d= -f2)
#            GF=$(grep "^GF" <<< "${EPILOG}" |  cut -d= -f2)
#            AVG_O_PTMP=$(grep "^AVG_O_PTMP" <<< "${EPILOG}" |  cut -d= -f2)
#            AVG_O_U=$(grep "^AVG_O_U" <<< "${EPILOG}" |  cut -d= -f2)
#            AVG_GPU=$(grep "^AVG_GPU" <<< "${EPILOG}" |  cut -d= -f2)

            if [[ ! -z "$CLUSTER" ]] && [[ ! -z "$JOBID" ]] && \
               [[ ! -z "$EXIT_CODE" ]] && [[ ! -z "$JOB_START_IN_SEC" ]] && \
               [[ ! -z "$JOB_END_IN_SEC" ]] && [[ ! -z "$ELAPSED" ]] && \
               [[ ! -z "$NNODES" ]] && [[ ! -z "$CORES" ]] && \
               [[ ! -z "$ITER" ]] && [[ ! -z "$SLURM_JOB_USER" ]] && \
               [[ ! -z "$JOB_NAME" ]];
            then
                ${BINDIR}/hpcmr_request.sh $CLUSTER $JOBID $EXIT_CODE \
                                           $JOB_START_IN_SEC $JOB_END_IN_SEC \
                                           $ELAPSED $NNODES $CORES $ITER \
                                           $SLURM_JOB_USER $JOB_NAME
            fi

# perf_out.py, disabled until final testing has passed.
#
#            OUT_DIR=${HPCMD_TMP_DIR}
#            if [ -f ${BINDIR}/perf_out.py ] && \
#                [[ ! -z "$exe" ]] && [[ ! -z "$AI" ]] && \
#                [[ ! -z "$GF" ]] && [[ ! -z "$AVG_O_PTMP" ]] && \
#                [[ ! -z "$AVG_O_U" ]] && [[ ! -z "$AVG_GPU" ]];
#            then
#                ${BINDIR}/perf_out.py $BINDIR $OUT_DIR $exe $AI $GF $AVG_O_PTMP $AVG_O_U $AVG_GPU
#            else
#                echo "Unable to execute perf_out.py"
#            fi

        fi
    fi

    # clean up in any case
    rm -f ${HPCMD_TMP_DIR}/suspend*
    rm -f ${HPCMD_TMP_DIR}/epilog
    rm -rd ${HPCMD_TMP_DIR}
) || true
