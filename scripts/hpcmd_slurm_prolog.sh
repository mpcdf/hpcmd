# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

# Prolog script for hpcmd, to be sourced into the system-wide prolog as follows:
#
# HPCMD_PROLOG="/opt/hpcmd/bin/hpcmd_slurm_prolog.sh"
# if [ -e $HPCMD_PROLOG ]; then
#     source $HPCMD_PROLOG
# fi
#

# Note: "job_start" message was moved into hpcmd

# clean up files from previous jobs, just to make sure
HPCMD_TMP_DIR=/var/tmp/hpcmd
rm -f ${HPCMD_TMP_DIR}/suspend
rm -f ${HPCMD_TMP_DIR}/epilog
# rm -fd ${HPCMD_TMP_DIR}
