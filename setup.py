# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

import os
import sys
import atexit
import shutil
import tempfile
from setuptools import setup, Command


def get_version():
    """Get the version number from version.py in the hpcmd source tree."""
    try:
        pkg_dir = os.path.abspath("hpcmd")
        sys.path.insert(0, pkg_dir)
        from hpcmd.version import get_version_string
        sys.path.pop(0)
        ver = get_version_string()
    except Exception:
        ver = "n/a"
    return ver


class CleanCommand(Command):
    """Custom clean command to remove unnecessary files."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        rm_args = ['build', 'dist', 'doc/html', 'doc/doctrees', 'hpcmd.egg-info', '.cache']
        os.system("rm -vrf " + " ".join(rm_args))
        os.system("find . -name '*.pyc' -delete -print")
        os.system("find . -name '*.so' -delete -print")


# GIT update does not work on OBS server, so we rather disable it!
# try:
#    os.system("./scripts/update_git_hash.py")
# except:
#    pass

# print version to enable other scripts (OBS) to easily access it
ver = get_version()
print("hpcmd " + ver)


# hard-wire the current interpreter into the launcher script to avoid issues with other Python environments
tmp_dir = tempfile.mkdtemp()
hpcmd_mod = os.path.join(tmp_dir, "hpcmd")

def prepare_hpcmd_mod():
    replace_s = "__SYS_EXECUTABLE__"
    with open("scripts/hpcmd", 'r') as fp_r, open(hpcmd_mod, 'w') as fp_w:
        do_replace = True
        for line in fp_r:
            if do_replace and replace_s in line:
                line = line.replace(replace_s, sys.executable)
                do_replace = False
            fp_w.write(line)

def cleanup_hpcmd_mod():
    if os.path.exists(hpcmd_mod):
        shutil.rmtree(tmp_dir)

prepare_hpcmd_mod()
atexit.register(cleanup_hpcmd_mod)


setup(
    name='hpcmd',
    version=ver,
    description='HPC performance monitoring daemon',
    author='Luka Stanisic, Klaus Reuter',
    author_email='klaus.reuter@mpcdf.mpg.de',
    url='https://gitlab.mpcdf.mpg.de/mpcdf/hpcmd',
    packages=['hpcmd'],
    package_dir={'hpcmd': 'hpcmd'},
    package_data={'hpcmd': ['config/*']},
    scripts=[
        hpcmd_mod,
        'scripts/hpcmd.exe',
        'scripts/hpcmd_slurm',
        'scripts/hpcmd_suspend',
        'scripts/hpcmd_slurm_prolog.sh',
        'scripts/hpcmd_slurm_epilog.sh',
        'scripts/syslogger.py',
    ],
    python_requires='>=3.2',
    install_requires=[
        'pyyaml'
    ],
    # Standalone daemon support needs to be enabled as an extra, because
    # * systemd uses its 'simple' wrapper to launch hpcmd as a daemon perfectly well
    # * python-daemon pulls in several unwanted dependencies
    # (Use `pip install 'hpcmd[daemon]'` to install with daemon support.)
    extras_require={
        'daemon': ['python-daemon<3'],
        'test': ['six', 'pytest'],
        'doc': ['sphinx'],
    },
    cmdclass={
        'clean': CleanCommand
    },
    zip_safe=False
)
