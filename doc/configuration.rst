Configuration
=============

hpcmd is configured via configuration files in YAML format. The format was
designed hierarchically to enable quick customization for individual HPC
systems.

First, hpcmd always loads the file ``config/default.yaml``. The location is
relative to the installation of the hpcmd Python package. As a second step,
it loads one of the following configuration files:

* ``config/CLUSTER_systemd.yaml`` in :doc:`systemd service
  mode<systemdmode>`, where CLUSTER is the name of the HPC system.
* ``config/CLUSTER.yaml`` in :doc:`user mode<usermode>`, where CLUSTER
  is the name of the HPC system.
* An optional configuration file passed to hpcmd via command line and
  the ``-c`` flag, relevant only in :doc:`user mode<usermode>`.

The settings from the second configuration file are merged into the ones
from the first configuration file. This allows to keep useful defaults
and only alter individual, cluster-specific parameters.

In addition, there are a few influential environment variables that
are relevant for the customization in :doc:`user mode<usermode>`.
