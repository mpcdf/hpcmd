Installation
============

.. contents:: :local:

Basic Installation
------------------

hpcmd is written in Python and is compatible with Python 2.7 and 3. The
software can be installed in the standard way using ``setup.py``. For
example, to install the package into the home directory of the current user,
issue the command::

    python setup.py install --user


Large Scale Deployment via RPM
------------------------------

An RPM spec file is provided which can be used to package hpcmd as an RPM for
the deployment on HPC systems.


Dependencies
------------

hpcmd may call the following standard Linux tools to query performance data:

- ``perf``: Query the PMUs, core, and uncore counters of processors
  via the perf subsystem. To enable regular users to access perf counters set
  ``/proc/sys/kernel/perf_event_paranoid`` to ``0``.
- ``ps``: Obtain information about the threads running on different
  cores. Independently, also obtain information about RSS memory.
- ``numastat``: Query the amount of memory used per socket.
- ``ip``: Collect information about generic IP network traffic.

In addition, depending on the hardware and software setup, the
following tools may be used:

- SLURM (``scontrol``, ``squeue``, ``sacct``, and temporary SLURM files):
  hpcmd integrates tightly with SLURM to complement performance data with
  job information.
- ``nvidia-smi``: Command line tool provided by the NVIDIA driver
  package, necessary to query GPU performance metrics.
- InfiniBand drivers: hpcmd reads counters from
  ``/sys/class/infiniband/`` to derive network metrics.
- ``opainfo``: Command line tool provided by the Intel OmniPath software
  package, used to query OmniPath network metrics. Requires root privileges.
- ``mmpmon``: Command line tool provided by the IBM SpectrumScale (GPFS)
  software, used to query file system metrics. Requires root privileges.
- ``ipmitool``: Intelligent Platform Management Interface, command line utility
  for controlling IPMI-enabled devices. Requires root privileges.
