User Mode
=========

.. contents:: :local:

hpcmd can be launched manually, either as a daemon in the background or in the
foreground. The output can either be written to syslog lines, and/or to
stdout, or redirected to a file.

Command line parameters
-----------------------

The following command line parameters are supported::

    $ hpcmd -h
    usage: hpcmd [-h] [-c file_name] [-g] [-n] [-p] [-s]
                 [-d | -k | -t | -m message]

    optional arguments:
    -h, --help            show this help message and exit
    -c file_name, --config file_name
                          specify configuration file
    -g, --debug           enable debug mode
    -n, --nosuspend       override suspend detection
    -p, --profile         run cProfile on hpcmd
    -s, --stdout          enable output to stdout/stderr

    optional, mutually exclusive arguments:
    -d, --daemon          fork hpcmd to background daemon
    -k, --kill            kill background daemon
    -t, --terminate       terminate background daemon
    -m, --msg             write a user-defined 'message' or 'variable=value'

The command line flags have higher priority than switches already set in the
configuration file.


Sampling frequency
------------------

Performance data is sampled at regular intervals by hpcmd. In user mode, the
sum of *loop_awake* and *loop_sleep* defines these intervals. Hpcmd calls the
``perf`` tool in a blocking way: perf sets up counters, measures for some
time (defined by the parameter ``daemon:loop_awake`` in the parameter file),
and finally reads from these counters. All other tools called by hpcmd return
within a fraction of a second. To measure (roughly) in a 10 minute loop, the
following configuration can be used::

    daemon:
        loop_awake: 590.0
        loop_sleep: 10.0

In this example, ``perf`` would measure for 590 seconds, then the
remaining measurements (network, gpfs, threads) would be performed,
and the output would be written to syslog. After that, hpcmd would
sleep for 10 seconds. Note that minimum value for ``loop_awake`` is 10
seconds, while ``loop_sleep`` can also be set to 0 (see `Monitoring
vs. profiling`_ for a detailed explanation). Note, moreover, that
epochs as in systemd mode are typically not useful in user mode since
a user wants to start the measurements immediately.


.. _runuser:

SLURM integration, process binding, and custom parameters
---------------------------------------------------------

To launch hpcmd in user mode in the context of a SLURM batch job, we
provide the wrapper script ``hpcmd_slurm``. Its purpose is twofold:
first, it triggers the suspension of an already running systemd
instance of hpcmd on each node; second, it launches a new user mode
instance of hpcmd on each node. To use the ``hpcmd_slurm`` wrapper,
extend your batch script with the following lines::

    export HPCMD_AWAKE=18
    export HPCMD_SLEEP=2

    srun hpcmd_slurm YOUR_EXECUTABLE

Few important points should be noted here:

1. There is the possibility to set the sleep and awake times via
   environment variables to custom values. The ``HPCMD_AWAKE`` and
   ``HPCMD_SLEEP`` environment variables basically override the
   ``loop_awake`` and ``loop_sleep`` settings from the parameter file.
   In this example, hpcmd would run in a 20 second loop.
2. In user mode, hpcmd disables any process binding for itself and its child
   processes, i.e., it enables the Linux scheduler to move these between cores
   in a non-intrusive way. The process binding of the application is not
   affected.
3. Use the ``HPCMD_PREFIX`` environment variable to specify the folder
   in which the output files are stored.


.. _profiling:

Monitoring vs. profiling
------------------------

The hpcmd tool was designed for the *monitoring* of large scale HPC
systems, acting as a lightweight middleware in the background, and
operating in epochs of several minutes. It is tempting to use the tool
in user mode for the *profiling* of applications at a highly increased
sampling frequency.  However, we learned from experiments that at
awake times of about 1 second the hpcmd process and its child
processes may not get enough CPU cycles to fulfill the measurement
loop, especially, when sharing the machine with a highly optimized
application. Therefore, the minimum possible awake time was limited to
10 seconds, to make hpcmd more reliable and keep potential overhead
low. If accurate profiling with even higher frequencies is needed,
please use an adequate profiling tool.


User-defined messages
---------------------

Some applications contain markers that notify about the beginning or the end
of an application phase. Moreover, some codes already contain certain
application-specific performance metrics. These messages and metrics be
forwarded by calling hpcmd with the ``-m`` command line option, to be later
analyzed in SPLUNK and correlated with the regular performance metrics. If
the message string contains a ``=`` character, the argument will be split in
two and interpreted as ``variable_name_string=numerical_value``. Otherwise,
the argument will be treated as a pure ``message_string``.


Limitations of user mode
------------------------

Note that certain functionalities supported in systemd mode are not
available in user mode, simply due to the fact that a non-privileged
user may not access certain tools or counters. The following
functionalities are unavailable:

* GPFS monitoring
* OmniPath monitoring

To enable the use of ``perf`` with hpcmd user mode, your system administrator
needs to set the special file ``/proc/sys/kernel/perf_event_paranoid`` to ``0``
to enable regular users access all perf counters.
