FAQ
===

.. contents:: :local:

Does hpcmd have any impact on the performance of my application?
----------------------------------------------------------------

.. No, it introduces typically less than 0.004% overhead when running in systemd mode.

Systematic comparison of the performance of various HPC applications with and
without hpcmd running in the background indicates that the impact on the
performance is significantly below the machine jitter and not measurable.
Note that hpcmd is running with reduced priority (niceness 10) and it is not
pinned to any CPU core, therefore the Linux scheduler moves it efficiently
between unused resources. More information is given in :ref:`overhead`.


Can I do performance profiling of my code while hpcmd is running?
-----------------------------------------------------------------

You need to disable the hpcmd background daemon in this situation to
give the profiler exclusive access to the hardware counters.
See :ref:`profiling` for more information.


How can a user suspend hpcmd running in systemd mode?
-----------------------------------------------------

::

    srun hpcmd_suspend YOUR_EXECUTABLE

More information is given in :ref:`suspend`.


How do I use hpcmd in user mode?
--------------------------------

::

    srun hpcmd_slurm YOUR_EXECUTABLE

More information is given in :ref:`runuser`.


How do I change frequency of sampling?
--------------------------------------

::

    export HPCMD_AWAKE=18
    export HPCMD_SLEEP=2

More information is given in :ref:`runuser`.


What is the highest frequency of sampling that I can use?
---------------------------------------------------------

One measurement per 10 seconds. Hpcmd is designed for performance monitoring
and not for profiling, so if you need higher frequency sampling please use an
appropriate profiler. More info in :ref:`profiling`.


Which Splunk dashboard do I need?
---------------------------------

Depends on the analysis you want to perform. More info in
:ref:`dashboards`.


I know which dashboard I want, but where do I find it?
------------------------------------------------------

From the main Splunk window just click on `Dashboards`, and then choose the
dashboard that you need from the list. More info in :ref:`dashboards`.


I have a problem with Splunk, what should I do?
-----------------------------------------------

There is a lot of documentation online about the Splunk, but you should start
with checking :ref:`splunktips`.
