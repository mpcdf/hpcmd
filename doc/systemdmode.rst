systemd Mode
============

.. contents:: :local:

In case hpcmd is installed via the RPM package, it is automatically installed
as a systemd service and launched afterwards. By default, it performs
measurements at regular intervals (*epochs*, see below) and writes the output
as syslog messages. Systemd service mode is the mode of operation hpcmd was
primarily designed for.


Epochs
------

Performance data is sampled at regular intervals by hpcmd. It calls the
``perf`` tool in a blocking way: perf sets up counters, measures for some
time (defined by the parameter ``daemon:loop_awake`` in the parameter file),
and finally reads from these counters. All the other tools called by hpcmd
return within a fraction of a second. When monitoring multiple nodes there is
the need to synchronize the measurements in order to have a more coherent
analysis. Therefore, we introduce the concept of an *epoch* which is defined
as a time duration synchronized to multiples of itself relative to midnight.
Thus, the duration of a day in seconds must be divisible by the duration of
the epoch. At the beginning of each epoch, a new measurement is started. On
the HPC systems, we typically measure in epochs of 10 minutes duration,
configured as follows in the parameter file::

    daemon:
        loop_awake: 590.0
        loop_epoch: 600.0

In this example, ``perf`` would measure for 590 seconds, then the remaining
measurements (network, gpfs, threads) would be performed, the output would be
written to syslog. After that, hpcmd would sleep until the beginning of the
next epoch. Note that ``loop_awake`` needs to be smaller than ``loop_epoch``.

.. In systemd service mode, the so-called *epoch* defines these intervals.

.. _overhead:

Overhead
--------

hpcmd is launched with a reduced scheduling priority (*niceness 10*) in the
background. The Linux scheduler therefore moves hpcmd and its child processes
to cores that are not fully used by the application at a given time. Note
that ``perf`` measures passively, mostly using programmable hardware
counters, hence the overhead from ``perf`` is negligible. After extensive
testing and several months in production on two HPC systems, we did not
experience any measurable overhead or impact on the applications when running
hpcmd in epochs of 10 minutes duration.


.. _suspend:

Suspend hpcmd for the duration of a single job
----------------------------------------------

Users of a cluster might want to suspend the hpcmd systemd service for the
duration of their job. This can be essential if one wants to do some
application profiling, since hardware counters, which are already queried by
hpcmd via ``perf``, cannot be accessed by a second tool. To suspend hpcmd,
simply insert ``hpcmd_suspend`` after ``srun`` in your batch script::

    srun hpcmd_suspend YOUR_EXECUTABLE

When the job has finished, the hpcmd systemd service will automatically be
enabled for the subsequent jobs.


Internal error detection
------------------------

hpcmd was developed and debugged carefully. Nevertheless, to enable the
detection of errors that may happen sporadically during run times of weeks or
months, the stderr stream is redirected to ``/var/log/hpcmd_stderr.log``. In
case Python exceptions occur, they are caught and logged to this file.
