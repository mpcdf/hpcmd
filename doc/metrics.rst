Metrics
=======

.. contents:: :local:

hpcmd supports a plethora of performance metrics that are mostly configurable
via the YAML file. For an actual example of a Skylake configuration, please
see the files ``config/default.yaml`` and ``config/cobra_systemd.yaml``
in the source distribution of hpcmd.

Linux perf counters
-------------------

The Linux perf subsystem provides a common interface to the performance
monitoring units (PMUs) of the processor, to core and uncore events, but also
to software events counted by the Linux kernel. hpcmd calls the ``perf``
command in machine-wide mode to measure such events. By default it is done on
a per-socket level of detail, but this can be modified to be per-core or
per-thread. Useful metrics enabled in the default configuration are, e.g.,
the number of floating point instructions (resolved by the SIMD instruction
set: scalar, SSE, AVX, AVX512), memory transfers, cache misses, and many
more. From the floating point instructions and the sampling interval, hpcmd
computes derived metrics, such as GFLOP/s, memory bandwidth, algorithmic
intensity, etc.

Filesystem
-----------

Using the command line tool ``mmpmon``, hpcmd queries node-local counters for
the GPFS filesystems, including the bytes read and written, as well as the
files opened and closed.

Network
-------

For Ethernet, InfiniBand, and OmniPath networks, hpcmd is able to count the
received and sent bytes, in order to later compute average transfer rates.

GPU
---

Using the tool ``nvidia-smi``, hpcmd queries the utilization and
memory usage during a short sampling period, for each GPU in a node.

SLURM
-----

hpcmd was designed from the beginning to integrate tightly with SLURM.

In systemd mode, hpcmd monitors if a SLURM job is running on the node by
checking the local spool directory. hpcmd then only performs measurements if
a job is active, and sleeps otherwise. For the active job, hpcmd reports
parts of the environment, such as the requested resources and influential
environment variables like ``OMP_NUM_THREADS``.

In user mode, hpcmd is launched together with the application by the ``srun``
command.  It has direct access to the environment of the job.

Job summary
-----------

During the job execution, averaged or cumulated values for several important
metrics (e.g., GFLOP/s) are updated each time a new sample is added. At the
end of the job, these values are written in a separate job-summary message,
which then contains a good overview of the job performance. The write-out of
this message is done by a SLURM epilog script.
