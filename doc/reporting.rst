Data Analysis
=============

.. contents:: :local:

This section briefly describes the components necessary beyond hpcmd to set
up the monitoring infrastructure on an HPC system.


Data transport and storage
--------------------------

The syslog messages written by hpcmd on each node are transported and
collected by the rsyslog tool, and finally fed into a SPLUNK
database. Note that on MPCDF systems, these transfers go via Ethernet,
thus this has no impact on the application communications that happen
via Infiniband or OmniPath.


SPLUNK
------

The SPLUNK analysis platform enables quick access to large volumes of
temporally ordered log line data via a powerful query language. SPLUNK
is designed for Big Data analysis, and it is therefore suitable for
MPCDF use cases, since there is also a need to inspect voluminous data
collected over a long period.

We are currently logging approximately 1.5 GiB of data per day for two large
MPCDF clusters combined. In order to make Splunk queries faster, this data is
stored in two separate databases. The primary database contains all the
measurements for all the jobs, and it takes most of the storage space. For
each job, the secondary database only contains one message from the beginning
and the job summary, i.e., the most important metrics to characterize the
job. This design was chosen to make it possible to implement a quick overview
of all jobs in a roofline view efficiently. Moreover, it allows SPLUNK to
perform faster as any query can start by doing a sub-query on a secondary
database which narrows down the main search on the primary database. The
resulting hierarchical queries are complex, but they are much more efficient,
and thus drastically improve the user experience.

Alternative frameworks to SPLUNK such as ELK or custom solutions can
certainly be used for the analysis with similar success. Our main motivation
for choosing SPLUNK is that it was already configured and successfully used
on MPCDF systems for other purposes, and it provides simple yet powerful
tools to perform all the analysis and visualization tasks necessary.


.. _dashboards:

SPLUNK dashboards
-----------------

To avoid SPLUNK users to manually and repeatedly perform the same
analysis, the most common queries and visualizations have been saved
into so-called SPLUNK dashboards.

Users can then choose the dashboard they need, fill in or select
the options from ``Form Inputs`` on the top of the web-page, and do
the envisioned analysis. Typically these ``Form Inputs`` are populated
from queries to the secondary database, thus they are very fast.

After choosing input parameters, depending on the specific dashboard,
main queries are then performed on primary, secondary or a combination
of both databases. Results of these queries are visualized in
``Panels``. ``Panels`` can be clicked and zoomed interactively,
refreshed and exported, and most importantly one can create a new
search (opened in a new tab), which will perform exactly the same
query and visualize that same ``Panel``. This can help understanding
the complex hierarchical query responsible for generating a specific
plot, and it can serve as a template for a slightly different search
and analysis.

At the moment, 5 main SPLUNK dashboards are prepared to enable HPC
support staff analyze the performance data in real time or
historically.


.. _fig_roofline:
.. figure:: ./fig/roofline2.png

   Overview on a selection of jobs from the previous 24 hours in a
   roofline plot on a specific HPC system. Each circle represents a
   job with its average performance, where the circle sizes are scaled
   by the actual CPU core hours of the jobs.


hpcmd_roofline: Roofline view
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The roofline view serves as the entry point to explore the performance data
from a complete HPC system. On a 2D coordinate system, where the x axis
denotes the arithmetic intensity and the y axis denotes the achieved
performance in GFLOPs, each job appears as a colored circle, scaled in size by
the CPU hours. When populated with enough jobs, this plot clearly
demonstrates which jobs are using the full potential of the nodes and which
ones on the other hand are far from the theoretical peak of the machine.

A table below the roofline model contains the same data, which allows for an
easy sorting by column.

This roofline plot represents a performance map for a specific HPC
system, where the time frame can be selected arbitrarily. Note that
only finished jobs are shown in this dashboard, as the data is
gathered solely from the job-summary messages in the secondary data
base. Clicking on a circle in the plot or on a line in a table
forwards the user to the detailed job view.

A snapshot of a roofline dashboard is presented in
:numref:`Fig. %s<fig_roofline>`.


.. _fig_job_perf:
.. figure:: ./fig/job_perf.png

   Excerpt from a detailed view on a specific job, showing the
   achieved performance in GFLOP/s, the memory bandwidth, and the
   algorithmic intensity for each socket. In addition to the averaged
   and maximum values shown in the table, plots over time are
   available per socket. Moreover, the SPLUNK dashboard contains about
   30 more plots for other CPU, GPU, network, filesystem, and software
   metrics (not shown here).


hpcmd_job: Detailed job view
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The detailed job view offers a good performance overview for jobs running on
a smaller number of nodes. It provides plots for a plethora of performance
metrics over time: GFLOP/s, memory bandwidth, and arithmetic intensity per
socket, memory usage, GPU, network, and IO performance per node, etc. In
addition, on the top of the page, general information, influential
environment variables, the number of unused cores per node, and user-defined
messages are given.

One can use this dashboard to analyze the performance of a running job close
to real-time. In most cases, it takes only seconds for a message logged by
hpcmd on a node to be propagated and visualized in SPLUNK.

For a job that has already finished, it is possible to generate a PDF
report from this dashboard containing the same information as on the
web-page.

If a job has been running on a larger number of nodes, it might be easier to
analyze it using the aggregated job view, to which one can switch by simply
clicking the button on the top of the page (below ``Form Inputs``).

A snapshot of a detailed job view dashboard is presented in
:numref:`Fig. %s<fig_job_perf>`.


hpcmd_job_agg: Aggregated job view
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The aggregated job view provides a good performance overview for jobs running
on a larger (> 16 sockets) number of nodes. It presents finished and still
running jobs, similar to the detailed view, only that the data is presented
in a statistical way using minimum, maximum, and median values and curves.
Aggregated job dashboards can also be exported to PDF reports.

If a job has run on only few nodes, it might be better to analyze it using
the detailed job view, to which one can switch by clicking the button on the
top of the page (below ``Form Inputs``).


partitions: Statistics on specific partitions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This dashboard allows to easily inspect whether the resources of some
clusters at the MPCDF are used adequately. It will report on jobs that
required the ``gpu partition`` but actually didn't use GPUs, as well
as jobs that required a ``fat node`` but never used the extra memory.

Additionally, this dashboard was extended to show other types of wrong
cluster usage. The dashboard shows jobs that even at peak used less
than half of the cores available on the node. Dashboard also shows
jobs that reserved nodes for a much longer period than needed, which
limits the efficiency of SLURM scheduler.

The data shown in this dashboard comes solely from the secondary database,
thereby SPLUNK performs well even for large time frames.


top_apps: Most executed applications
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This dashboard shows the most executed applications ranked by core hours. The
name of the application is deduced from the name of the executable reported
by ``ps``. Even though this approach is not completely reliable it has shown
very good results in practice.

The number of applications to be shown in the plots is by default 10,
but this can be changed in the ``Form Input``. The table on the bottom
of the page provides ``jobid - executable_name`` pairs, and its main
purpose is for being exported into a .csv file that can later be used
for different analysis, outside of SPLUNK.

The data shown in this dashboard comes solely from the secondary database,
thereby SPLUNK performs well even for large time frames.


.. _splunktips:

SPLUNK tips related to HPC monitoring data
------------------------------------------

- Don't forget that when running in systemd service mode, data is
  printed every 10 minutes, so even if a job started running it might
  take up to 10 minutes to observe the first results in SPLUNK.

- Unless it is something trivial, try to avoid to write the SPLUNK
  queries from scratch. Instead, open a similar existing search from a
  ``Panel`` in a dashboard, and then modify necessary parameters.

- If you accidentally submitted a search that takes too long to
  respond, consider stopping and deleting your ``SPLUNK job``. SPLUNK
  jobs of each user can be inspected and edited through ``Activity ->
  Jobs``, located at the top right corner of the SPLUNK web-page.

- If you are interested in historic data, use a `Relative` time range
  and be as specific as possible.  Shorter periods lead to shorter
  times for SPLUNK to analyze the data.

- Dashboards can be edited through the user interface or by manually
  editing the underlying XML.

- Everyone with write access to SPLUNK dashboards can edit them, and
  any saved changes will impact all the users. Therefore, if you
  intend to experiment with a dashboard, or want to develop a personal
  one, consider cloning it into a private dashboard (top right corner,
  ``...- > Clone``).

.. [coming soon: All the changes to the dashboards are creating a Git
   commit in the gitlab/... project, so it is always possible to
   revert to the previous state.]
