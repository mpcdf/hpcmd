# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

from collections import OrderedDict
from hpcmd.filesystem import Filesystem


def setup_cfg():
    cfg = OrderedDict()
    cfg['filesystem'] = OrderedDict()
    cfg['filesystem']['enable'] = True
    cfg['filesystem']['gpfs'] = OrderedDict()
    cfg['filesystem']['gpfs']['enable'] = True
    return cfg


def test__filesystem__parse_gpfs_output():
    fs_data = OrderedDict()
    cfg = setup_cfg()

    lines = [
        "_fs_io_s_ _n_ 130.183.21.219 _nn_ dra0860 _rc_ 0 _t_ 1540390574 _tu_ 943463 _cl_ dracoio.mpcdf.mpg.de _fs_ draco_u _d_ 20 _br_ 102849566884 _bw_ 62591862863 _oc_ 401389 _cc_ 394003 _rdc_ 114124 _wc_ 234621 _dir_ 521 _iu_ 57290",
        "_fs_io_s_ _n_ 130.183.21.219 _nn_ dra0860 _rc_ 0 _t_ 1540390574 _tu_ 943463 _cl_ dracoio.mpcdf.mpg.de _fs_ draco_ptmp _d_ 36 _br_ 62309499676 _bw_ 16468325156 _oc_ 1870630 _cc_ 1726984 _rdc_ 14587 _wc_ 7650 _dir_ 0 _iu_ 2190",
        "_fs_io_s_ _n_ 130.183.21.219 _nn_ dra0860 _rc_ 0 _t_ 1540390574 _tu_ 943463 _cl_ dracoio.mpcdf.mpg.de _fs_ draco_projects _d_ 4 _br_ 0 _bw_ 0 _oc_ 1 _cc_ 1 _rdc_ 0 _wc_ 0 _dir_ 0 _iu_ 0"
    ]

    f = Filesystem(cfg)
    f.id_list = list(["filesystem", "disks"])
    f.fs_names = list(["u", "projects"])

    fs_data = f._parse_gpfs_output(cfg, lines)
    assert(fs_data["u"]["filesystem"] == "draco_u")
    assert(fs_data["projects"]["disks"] == "4")

    f.fs_names = list(["all"])
    fs_data = f._parse_gpfs_output(cfg, lines)
    assert(fs_data["u"]["filesystem"] == "draco_u")
    assert(fs_data["ptmp"]["disks"] == "36")


# These test are deprecated, but keep them just in case

# def test__filesystem__update():
#     raw = "dra[0609-0612,0624,0629-0631,0637],drag[0609-0612,0624,0629-0631,0637],drax001,dra-coo"
#     exp = slurm.expand_nodelist(raw)
#     tmp_cmd = 'tmp.cmd'
#     cfg = OrderedDict()
#     cfg['filesystem'] = OrderedDict()
#     cfg['filesystem']['gpfs'] = OrderedDict()
#     cfg['filesystem']['gpfs']['n_nodes'] = 2
#     cfg['filesystem']['gpfs']['cmd_path'] = tmp_cmd
#     filesystem.update(exp, cfg)
#     pr.rm(tmp_cmd)
#     assert(cfg['filesystem']['gpfs']['n_nodes'] == len(exp))
