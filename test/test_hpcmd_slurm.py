# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

from collections import OrderedDict
from hpcmd import slurm
from hpcmd import primitives as pr


def test__slurm__get_node_info_from_slurm_conf():
    nfo = slurm.get_node_info_from_slurm_conf(node_name="dra0001", slurm_conf="./slurm.conf")
    assert(nfo["CoresPerSocket"] == 16)


def test__update_node_specs_from_slurm_conf():
    cfg = {}
    cfg["hostname"] = "drag001"
    cfg["slurm"] = {}
    cfg["slurm"]["slurm_conf"] = "./slurm.conf"
    slurm.update_node_specs_from_slurm_conf(cfg)
    assert("gpu" in cfg["slurm"]["node"]["feature"])


def test__slurm__get_partition_info_from_slurm_conf():
    nfo = slurm.get_partition_info_from_slurm_conf(partition_name="fat", slurm_conf="./slurm.conf")
    assert(nfo["MaxNodes"] == 4)


def test__slurm__get_value_from_slurm_conf():
    val = slurm.get_value_from_slurm_conf(key="not_there", slurm_conf="./slurm.conf")
    assert(val == None)
    val = slurm.get_value_from_slurm_conf(key="not_there", slurm_conf="./slurm.conf", default_value="foobar")
    assert(val == "foobar")
    val = slurm.get_value_from_slurm_conf(key="PropagateResourceLimitsExcept", slurm_conf="./slurm.conf")
    assert(val == "MEMLOCK")


def test__slurm__get_printable_job_id():
    cfg = {}
    cfg["slurm"] = {}
    assert(slurm.get_printable_job_id(cfg) == None)
    cfg["slurm"]["job"] = {}
    cfg["slurm"]["job"]["jobid"] = 4711
    assert(slurm.get_printable_job_id(cfg) == 4711)
    cfg["slurm"]["job"]["array_job_id"] = 4713
    assert(slurm.get_printable_job_id(cfg) == 4711)
    cfg["slurm"]["job"]["array_task_id"] = 0
    assert(slurm.get_printable_job_id(cfg) == "4713_0")


def test__slurm__has_job_information():
    cfg = {}
    cfg["slurm"] = {}
    cfg["slurm"]["enable"] = {}
    cfg["slurm"]["enable"] = False
    assert(pr.has_job_info(cfg) == False)
    cfg["slurm"]["enable"] = True
    cfg["slurm"]["job"] = {}
    assert(pr.has_job_info(cfg) == True)
