# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

import os
import six
import pickle
from hpcmd import util


def test__get_environment():
    my_pid = os.getpid()
    env = util.get_environment(my_pid)
    assert("HOME" in env)


def test__get_env():
    my_pid = os.getpid()
    env = util.get_env(my_pid)
    assert("HOME" in env)


def test__get_process_ids():
    pids = util.get_process_ids("bash")
    assert(len(pids) > 0)


def test__get_child_pids():
    # TODO: make test deterministic
    pids = util.get_process_ids("bash")
    chld = util.get_child_pids(pids[0])


def test__expand_nodelist():
    raw = "dra[0609-0612,0624,0629-0631,0637],drag[0609-0612,0624,0629-0631,0637],drax001,dra-coo"
    exp = util.expand_nodelist(raw)
    assert(len(exp) == 20)
    for item in exp:
        assert(item.startswith("dra"))
    assert(exp[-2] == "drax001")
    assert(exp[-1] == "dra-coo")


def test__get_all_pylibs():
    pids = []
    pids.append(os.getpid())
    ps_libraries = util.get_all_pylibs(pids)
    assert any('_json' in lib for lib in ps_libraries), "Unlike expected, this Python process does not have '_json' imported as a shared object!"
