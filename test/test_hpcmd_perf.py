# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

import os
from hpcmd import perf


def test__check_perf_tool():
    try:
        perf.check_perf_tool()
    except RuntimeError:
        # runtime error to be expected on CI in the cloud or on desktop systems
        pass

def test__check_perf_event_paranoid():
    try:
        perf.check_perf_event_paranoid()
    except RuntimeError:
        # runtime error to be expected on CI in the cloud or on desktop systems
        pass

def test__get_cpu_vendorid():
    vendorid = perf.get_cpu_vendorid()
    assert(vendorid is not None)

def test__get_cpu_vendor():
    vendor = perf.get_cpu_vendor()
    assert(vendor in ['amd', 'intel'])

def test__map_counter_intel_skylake():
    val = perf.map_counter_intel_skylake('r5301c7')
    assert(val == 'fp_d')
    val = perf.map_counter_intel_skylake('r1337')
    assert(val == 'r1337')

def test__map_counter_amd_zen4():
    # TODO implement reasonable tests
    pass
