# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

from collections import OrderedDict
from hpcmd.gpu import Gpu


def setup_cfg():
    cfg = OrderedDict()
    cfg['gpu'] = OrderedDict()
    cfg['gpu']['enable'] = True
    cfg['gpu']['nvidia'] = OrderedDict()
    cfg['gpu']['nvidia']['enable'] = True
    return cfg


def test__gpu__parse_nvidia_output():
    gpu_data = OrderedDict()
    cfg = setup_cfg()

    # typical output of nvidia-smi --query-gpu=pstate,utilization.gpu,utilization.memory, --format=csv
    lines = ['P2, 61 %, 1397 MiB', 'P2, 85 %, 1404 MiB']

    g = Gpu(cfg)
    g.id_list = list(["pstate", "utilization.gpu", "memory.used"])
    gpu_data = g._parse_nvidia_output(cfg, lines)
    assert(gpu_data["D0"]["utilization.gpu"] == 61)
    assert(gpu_data["D1"]["memory.used"] == 1404)


def test__gpu__parse_nvidia_intro():
    gpu_data = OrderedDict()
    cfg = setup_cfg()

    # typical output of nvidia-smi --query-gpu=name,driver_version,count,memory.total--format=csv
    lines = ['GeForce GTX 980, 410.66, 2, 4043 MiB',
             'GeForce GTX 980, 410.66, 2, 4043 MiB'
    ]

    g = Gpu(cfg)
    g.intro_id_list = list(["name", "driver_version", "count", "memory.total"])
    gpu_data = g._parse_intro(lines)
    assert(gpu_data["D0"]["count"] == "2")
    assert(gpu_data["D0"]["memory.total"] == "4043 MiB")
