# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

from collections import OrderedDict

from hpcmd.tool import Tool


class Prototool1(Tool):
    def __init__(self, c):
        print("Entered constructor")
        print(c)
        super(Prototool1, self).__init__(c, "prototool1", "prototool1 tool")

    def specific_setup(self, cfg):
        print("1: Tool specific part of the constructor")


class Prototool2(Tool):
    def __init__(self, c):
        super(Prototool2, self).__init__(c, "prototool2")

    def specific_setup(self, cfg):
        print("2: Tool specific part of the constructor")


def setup_cfg(toolnames):
    cfg = OrderedDict()
    for t in toolnames:
        cfg[t] = OrderedDict()
        cfg[t]['enable'] = True
        cfg[t]['print_samples'] = True
        cfg[t]['print_summary'] = False
    return cfg


def test__tool__creation():
    toolnames = ['prototool1', 'prototool2']
    cfg = setup_cfg(toolnames)
    cfg['prototool2']['print_samples'] = False
    for t in toolnames:
        classname = t.capitalize()
        tool = globals()[classname](cfg)
        tool.setup(cfg)
        print(tool)
    assert(cfg['prototool1']['enable'] == True)
    assert(cfg['prototool2']['enable'] == False)
