# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

from collections import OrderedDict
from hpcmd.physics import Physics


def setup_cfg():
    cfg = OrderedDict()
    cfg['physics'] = OrderedDict()
    cfg['physics']['enable'] = True
    cfg['physics']['ipmi'] = OrderedDict()
    cfg['physics']['ipmi']['enable'] = True
    cfg['physics']['ipmi']['lockfile'] = "/tmp/load-sensor.pid"
    return cfg


def test__physics__parse_ipmi_output():
    physics_data = OrderedDict()
    cfg = setup_cfg()

    ##################################################
    # Draco
    # typical output of "ipmitool sdr type 'Current'"
    lines = ['Avg Power        | 2Eh | ok  | 21.0 | 100 Watts']
    phy = Physics(cfg)
    outdata = phy._parse_ipmi_output(lines)
    assert(outdata['power'] == 100)

    ##################################################
    # Cobra
    # typical output of "ipmitool sdr type 'Current'"
    lines = ['Sys Fan 3B       | 21483 RPM         | ok',
             'PS1 Input Power  | 832 Watts         | ok',
             'PS2 Input Power  | 831 Watts         | ok',
             'PS1 Curr Out %   | 30 percent        | ok',
             'PS2 Curr Out %   | 30 percent        | ok',
             'PS1 Temperature  | 33 degrees C      | ok'
    ]
    phy = Physics(cfg)
    outdata = phy._parse_ipmi_output(lines)
    assert(outdata['power'] == (832 + 831))
