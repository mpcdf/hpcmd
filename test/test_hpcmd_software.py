# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

import six
import getpass
from collections import OrderedDict
from hpcmd.software import Software


def setup_cfg():
    cfg = OrderedDict()
    cfg['software'] = OrderedDict()
    cfg['software']['enable'] = True
    # Specific for loadavg
    cfg['software']['loadavg'] = OrderedDict()
    cfg['software']['loadavg']['enable'] = True
    return cfg


def test__software__get_ps_exe_information():
    cfg = setup_cfg()
    s = Software(cfg)
    user, pid, exe = s._get_ps_exe_information(getpass.getuser())
    assert(isinstance(user, six.string_types))
    assert(isinstance(pid, six.integer_types))
    assert(isinstance(exe, six.string_types))


def test__software__parse_loadavg_output():
    cfg = setup_cfg()
    s = Software(cfg)
    s.id_list = ['load_1min', 'load_5min', 'load_15min']
    outdata = OrderedDict()
    line = '1.32 1.75 1.98 2/1185 28235'
    s._parse_loadavg_output(outdata, line)
    assert(outdata['loadavg']['load_1min'] == 1.32)
    assert(outdata['loadavg']['load_15min'] == 1.98)
