# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""hpcmd's collection of SLURM-related functions.
"""

import os
import glob
import time
import socket

from . import util
from . import log
from . import primitives as pr


# List of environment variables to be used in case 'hpcmd' is running in a user
# context to collect information. Note that update_job_info() obtains the same
# information from 'squeue' when 'hpcmd' is run as a systemd service.
slurm_env_vars = ["SLURM_JOBID", "SLURM_NODEID", "SLURM_JOB_NAME",
                  "SLURM_JOB_USER", "SLURM_JOB_NUM_NODES"]
slurm_array_env_vars = ["SLURM_ARRAY_JOB_ID", "SLURM_ARRAY_TASK_ID"]

# Additional slurm-related information to be gathered via environment variables
# on the head node for a hpcmd running as a systemd service, or at
# initialization when hpcmd is running in user context. Cannot be obtained via
# (cheap) slurm commands.
slurm_optional_env_vars = ["SLURM_NTASKS", "SLURM_NTASKS_PER_NODE",
                           "SLURM_CPUS_PER_TASK", "SLURM_HINT",
                           "OMP_NUM_THREADS", "OMP_PLACES", "OMP_PROC_BIND",
                           "LOADEDMODULES"]


def setup(cfg):
    """Set up SLURM-related configuration.

    When running as a regular user, the jobs environment is used directly.

    For root operation (systemd service), see the update() function below.
    """

    if cfg["slurm"]["enable"]:
        if "node_blacklist" in cfg["slurm"]:
            if cfg["hostname"] in util.expand_nodelist(cfg["slurm"]["node_blacklist"]):
                cfg["daemon"]["suspend"] = True
        try:
            # wait until the slurm.conf file appears
            if util.wait_for_file(cfg["slurm"]["slurm_conf"], cfg=cfg):
                cfg["slurm"]["spool_dir"] = get_value_from_slurm_conf(key="SlurmdSpoolDir",
                                                                      slurm_conf=cfg["slurm"]["slurm_conf"],
                                                                      default_value="/var/spool/slurmd/")
                # add information on the compute node
                update_node_specs_from_slurm_conf(cfg)

            if "SLURM_JOBID" in os.environ:
                cfg["slurm"]["job"] = {}
                cfg["slurm"]["job"]["job_start"] = time.time()
                # hpcmd is running within the context of a SLURM job,
                # we're therefore able to access the environment directly
                _slurm_env_to_cfg(cfg, os.environ, slurm_env_vars)
                _slurm_env_to_cfg(cfg, os.environ, slurm_array_env_vars, add_missing_none=False)
                _slurm_env_to_cfg(cfg, os.environ, slurm_optional_env_vars,
                                  add_missing_none=False, sub_category="environment")
                cfg["slurm"]["job"]["printable_jobid"] = get_printable_job_id(cfg)
                cfg["slurm"]["job"]["headnode"] = (int(cfg["slurm"]["job"]["nodeid"]) == 0)
                util.reset_stats_counters(cfg)
        except Exception as e:
            log.exception(e)
            log.debug("setup error: disable slurm")
            cfg['slurm']['enable'] = False


def update(cfg):
    """Update the SLURM job information (if it has changed)."""

    def _update_job_information(cfg, new_jobid, job_start_time):
        """Update the job information by querying SLURM via the squeue command.
        """
        pr.del_job_info(cfg)
        if new_jobid is None:
            return
        else:
            try:
                cmd = "squeue --job={} --noheader --format='%i %j %u %D %R'".format(new_jobid)
                raw, exit_status = util.run(cmd)
                out = util.get_ascii_lines(raw)
                log.debug(out)
                #
                # Passing SLURM_JOB_ID to squeue, we get for an array job, e.g.:
                # $ squeue --job=6738533 --noheader
                # 6733974_700   general  isoseq3    anon_user  R    7:31:58      1 dra0480
                #
                if exit_status:
                    raise RuntimeError("command `" + cmd + "` exited with error")
                else:
                    cfg["slurm"]["job"] = {}
                    cfg["intro_logged"] = False
                    cfg["slurm"]["job"]["job_start"] = job_start_time
                    nfo = out[0].split()
                    # "jobid" is always SLURM_JOBID, independent of array jobs
                    cfg["slurm"]["job"]["jobid"] = new_jobid
                    # Note: "printable_jobid" is for
                    #    * regular jobs: SLURM_JOBID
                    #    * array jobs: SLURM_ARRAY_JOB_ID + "_" + SLURM_ARRAY_TASK_ID
                    # which is the same result as we get from our function "get_printable_job_id()"
                    cfg["slurm"]["job"]["printable_jobid"] = nfo[0]
                    cfg["slurm"]["job"]["job_name"] = nfo[1]
                    cfg["slurm"]["job"]["job_user"] = nfo[2]
                    cfg["slurm"]["job"]["job_num_nodes"] = int(nfo[3])
                    _nodelist = util.expand_nodelist(nfo[4])
                    # Verify that this definition of nodeid actually matches the slurm-internal one:
                    cfg["slurm"]["job"]["nodeid"] = _nodelist.index(cfg['hostname'])
                    cfg["slurm"]["job"]["headnode"] = is_head_node(cfg)
                    util.reset_stats_counters(cfg)
            except (RuntimeError, ValueError) as e:
                pr.del_job_info(cfg)
                log.warning(e)
            except Exception as e:
                pr.del_job_info(cfg)
                log.exception(e)

    def _update_job_environment(cfg, env_vars):
        """Obtain information we cannot get via 'squeue', by reading directly from
        the job script's environment.

        Note: By SLURM's design, this only works on the head node of a job (NODEID=0).
        """
        if pr.has_job_info(cfg):
            sub_category = "environment"
            if sub_category in cfg["slurm"]["job"]:
                del cfg["slurm"]["job"][sub_category]
            if is_head_node(cfg):
                pids = util.get_process_ids("slurm_script")
                if len(pids) > 0:
                    env = util.get_environment(pids[0])
                    _slurm_env_to_cfg(cfg, env, env_vars, add_missing_none=False, sub_category=sub_category)


    # --- body of update() function below ---

    try:
        old_jobid = util.slurm_get_saved_job_id(cfg)
        jobid, job_start_time = util.slurm_get_running_job_id(cfg)
        if (jobid != old_jobid):
            _update_job_information(cfg, jobid, job_start_time)
            _update_job_environment(cfg, slurm_optional_env_vars)
    except Exception as e:
        log.exception(e)


def get_n_concurrent_jobs_on_node(cfg):
    """Return the number of SLURM jobs currently running on the node.
    """
    # We count unique strings of the form 'dra0415_5462825', where
    # 5462825 is SLURM_JOB_ID, which appear in the filenames of sockets
    # in the slurm spool directory.
    pattern = cfg["hostname"] + "_*.*"
    raw = glob.glob(os.path.join(cfg["slurm"]["spool_dir"], pattern))
    unique = set()
    for s in raw:
        f = os.path.basename(s)
        unique.add(f.split('.')[0])
    return len(unique)


def get_value_from_slurm_conf(key, slurm_conf="/etc/slurm/slurm.conf", default_value=None):
    """Return value from a plain key=value pair from slurm.conf.
    """
    lines = util.get_lines_from_file(slurm_conf, keywords=key, comment_char='#')
    if len(lines) == 0:
        return default_value
    else:
        key, val = lines[0].split('=', 1)
        return pr.ntype(val)


def get_printable_job_id(cfg):
    """Return a printable SLURM job ID as a string, or None.

    For array jobs, this is ARRAY_JOB_ID UNDERSCORE ARRAY_TASK_ID,
    for other jobs, this is just SLURM_JOBID.
    """
    val = None
    try:
        val = "{}_{}".format(cfg["slurm"]["job"]["array_job_id"],
                             cfg["slurm"]["job"]["array_task_id"])
    except Exception:
        try:
            val = cfg["slurm"]["job"]["jobid"]
        except Exception:
            pass
    return val


def get_slurm_script_file_name(cfg):
    """Return full path to the job's 'slurm_script' found on a head node
    of a job, or None.
    """
    if pr.has_job_info(cfg):
        # Note: The second arg of join must not start with a '/'!
        return os.path.join(cfg["slurm"]["spool_dir"],
                            "job{:0>5}/slurm_script".format(cfg["slurm"]["job"]["jobid"]))
    else:
        return None


def is_head_node(cfg):
    """Identify a job's head node from the presence of the 'slurm_script' file.
    """
    slurm_script = get_slurm_script_file_name(cfg)
    if slurm_script is None:
        return False
    else:
        return os.path.isfile(slurm_script)


def update_node_specs_from_slurm_conf(cfg):
    """Add information on the node from slurm.conf to cfg["slurm"]["node"].
    """
    specs = get_node_info_from_slurm_conf(node_name=cfg["hostname"],
                                          slurm_conf=cfg["slurm"]["slurm_conf"])
    data = {}
    # Keys are written in CamelCase in slurm.conf, but converted to lower case here.
    # 1) keys: integer values
    keys = ["Sockets", "CoresPerSocket", "ThreadsPerCore"]
    for key in keys:
        new_key = key.lower()
        try:
            data[new_key] = int(specs[key])
        except Exception:
            data[new_key] = None
    # 1a) on newer systems (Raven-final) we have SocketsPerBoard. TODO implement support for multi-board systems properly
    key = "SocketsPerBoard"
    new_key = "sockets"
    try:
        data[new_key] = int(specs[key])
    except Exception:
        data[new_key] = None
    # 2) keys: float values
    keys = ["RealMemory"]
    for key in keys:
        new_key = key.lower()
        try:
            data[new_key] = float(specs[key])
        except Exception:
            data[new_key] = None
    # 3) keys: string values
    keys = ["Feature"]
    for key in keys:
        new_key = key.lower()
        try:
            # different features are put into separate list elements
            data[new_key] = specs[key].split(',')
        except Exception:
            data[new_key] = []
    cfg["slurm"]["node"] = data


# --- module-local helper functions below ---

def _slurm_env_to_cfg(cfg, env, env_vars, add_missing_none=True, sub_category=None):
    """Add values for the keys from the list env_vars from env to cfg.
    """
    if (sub_category is not None):
        if sub_category not in cfg["slurm"]["job"]:
            cfg["slurm"]["job"][sub_category] = {}
        dic_entrypoint = cfg["slurm"]["job"][sub_category]
    else:
        dic_entrypoint = cfg["slurm"]["job"]
    for var in env_vars:
        key = var.replace("SLURM_", "", 1).lower()
        try:
            dic_entrypoint[key] = pr.ntype(env[var])
        except Exception:
            if add_missing_none:
                dic_entrypoint[key] = None


def _get_group_info_from_slurm_conf(group_name, member_identifier, slurm_conf):
    """Obtain information on a "group", i.e. partition or compute node, directly from <slurm.conf>.

    Returns dictionary with key-value pairs from the entry of the relevant line.

    Loads the DEFAULT section first, then overrides it with more specific values
    potentially available for the specific group_name.  Requires the DEFAULT section
    to be placed before the node-specific section in <slurm.conf>.
    """
    node_info_dict = {}
    lines = util.get_lines_from_file(slurm_conf, keywords=group_name, comment_char='#')
    for x in lines:
        try:
            line = x.split()
            item = line[0]
            del line[0]
            key, val = item.split('=')
            found_member = member_identifier in util.expand_nodelist(val)
            if (key == group_name) and \
                ((val == "DEFAULT") or found_member):
                for item in line:
                    key, val = item.split('=')
                    node_info_dict[key] = pr.ntype(val)
            if found_member:
                break
        except Exception:
            pass
    return node_info_dict


def get_node_info_from_slurm_conf(node_name=socket.gethostname(), slurm_conf="/etc/slurm/slurm.conf"):
    """Return the entries for a specific node specified by node_name
    from the 'NodeName' line as key value pairs in a dictionary.
    """
    return _get_group_info_from_slurm_conf("NodeName", node_name, slurm_conf)


def get_partition_info_from_slurm_conf(partition_name, slurm_conf="/etc/slurm/slurm.conf"):
    """Return the entries for a specific partition specified by partition_name
    from the 'PartitionName' line as key value pairs in a dictionary.
    """
    return _get_group_info_from_slurm_conf("PartitionName", partition_name, slurm_conf)


def intro(msg, cfg):
    """Add 'prolog-like' info about about the job and job submission script."""
    if cfg['slurm']['enable']:
        try:
            msg.append(pr.keqv("jobname", cfg["slurm"]["job"]["job_name"]))
            msg.append(pr.keqv("jobstart", cfg["slurm"]["job"]["job_start"]))
            msg.append(pr.keqv("nnodes", cfg["slurm"]["job"]["job_num_nodes"]))
            if "environment" in cfg["slurm"]["job"]:
                for key in cfg["slurm"]["job"]["environment"]:
                    msg.append(pr.keqv(key, cfg["slurm"]["job"]["environment"][key]))
            if "node" in cfg["slurm"]:
                # hyperthreading and memory information are obtained from slurm
                msg.append(pr.keqdv("threadspercore", cfg['slurm']['node']))
                msg.append(pr.keqdv("realmemory", cfg['slurm']['node']))
        except Exception as e:
            log.exception(e)
    else:
        log.debug("slurm disabled")
