# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module implementing an idle process to sleep while other tools perform
measurements.  Checks for interruptions via separate watchdog threads.
"""

import os
import re
import signal
import threading

from .tool import Tool
from . import util
from . import log
from . import cleanup
from . import primitives as pr


# flag used to notify the interrupt_watchdog to shut down from the main thread
stop_watchdog_event = threading.Event()
# flag used to notify the main thread from the watchdog if sleep was interrupted
perf_interrupt_event = threading.Event()

class Sleep(Tool):
    def __init__(self, c):
        super(Sleep, self).__init__(c, "sleep")

    def specific_setup(self, cfg):
        pass

    def run(self, cfg):
        global stop_watchdog_event
        global perf_interrupt_event
        if self.enabled:
            try:
                stop_watchdog_event.clear()
                perf_interrupt_event.clear()
                super(Sleep, self).run(cfg)
            except Exception as e:
                log.exception(e)
        else:
            log.debug("{} disabled".format(self.logname))

    def execute(self, cfg):
        """Run the perf command based on the configuration passed via cfg and return
        the data in a dictionary."""
        command = "sleep {}".format(cfg['daemon']['loop_awake'])
        log.debug(command)
        p = util.popen(command)
        # we need to run a concurrent thread to handle potential interrupts
        watchdog = threading.Thread(target=self.interrupt_watchdog, args=(cfg, p.pid))
        watchdog.start()
        # wait until perf finishes
        _out, _err = p.communicate()
        p.wait()
        # shut down the watchdog thread
        stop_watchdog_event.set()
        watchdog.join()
        if perf_interrupt_event.is_set():
            log.message("sleep.run: interrupted", cfg)
        return True

    def interrupt_watchdog(self, cfg, pid):
        """Watchdog function to be run in a separate thread inside run() in order
        to interrupt a running sleep instance asynchronously.
        """
        global stop_watchdog_event
        global perf_interrupt_event
        log.debug("sleep.interrupt_watchdog: enter")

        def _condition(cfg):
            """Evaluate the condition to continue with the polling loop below.
            """
            p = cleanup.signal_received()
            if util.superuser():
                q = util.query_suspend_file(cfg)
                r = util.slurm_query_job_change(cfg)
                return (not p) and (not q) and (not r)
            else:
                return (not p)

        while _condition(cfg):
            log.debug("sleep.interrupt_watchdog: polling")
            stop_watchdog_event.wait(cfg['daemon']['suspend_poll_time'])
            if stop_watchdog_event.is_set():
                log.debug("sleep.interrupt_watchdog: stop_watchdog_event")
                break
        else:
            log.debug("sleep.interrupt_watchdog: interrupt detected")
            log.debug("sleep.interrupt_watchdog: sending SIGINT to " + str(pid))
            try:
                os.kill(pid, signal.SIGINT)
            except Exception as e:
                log.exception(e)
            # pr.remove_epilog(cfg)
            perf_interrupt_event.set()
        log.debug("sleep.interrupt_watchdog: exit")

    def update_summary(self, cfg, outdata):
        """Update summary file with the perf overhead stats."""
        pass

    def print_sample(self, cfg, outdata):
        """Printing data collected for perf services."""
        pass
