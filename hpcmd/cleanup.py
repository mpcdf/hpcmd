# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""hpcmd's functions for clean start and stop.
"""

import threading

from . import log
from . import util
from . import primitives as pr

remove_list = []
cleanup_event = threading.Event()
job_change_event = threading.Event()
stop_watchdog_event = threading.Event()


def setup(cfg):
    """Add files to a list for potential final cleanup."""
    global remove_list
    # manual deletion of the PID file is not necessary
    remove_list.append(cfg['daemon']['pid_file'])


def clean():
    """Remove files that are on the cleanup list from disk."""
    for file_name in remove_list:
        pr.rm(file_name)


def handler(signal, frame):
    """Signal handler, used by the daemon for SIGTERM.

    Triggers a global threading. Event for synchronization.
    """
    global cleanup_event
    cleanup_event.set()


def signal_received():
    """Return the present value of the threading.Event."""
    global cleanup_event
    return cleanup_event.is_set()


def sleep(cfg, t):
    """Sleep for t seconds, can be interrupted by signals and job changes."""
    global cleanup_event
    global job_change_event
    global stop_watchdog_event

    # in case we're running as a systemd service we want to react
    # to job changes using a watchdog thread
    if util.superuser():
        job_change_event.clear()
        stop_watchdog_event.clear()
        # launch watchdog thread to asynchronously interrupt the wait(), if necessary
        watchdog = threading.Thread(target=interrupt_watchdog, args=(cfg,))
        watchdog.start()

    # sleep for t seconds, if not interrupted
    cleanup_event.wait(t)

    if util.superuser():
        # signal the watchdog thread to stop
        stop_watchdog_event.set()
        watchdog.join()


def interrupt_watchdog(cfg):
    """Watchdog function to be run in a separate thread inside run() in order
    to interrupt sleep() asynchronously at a job change.
    """
    global stop_watchdog_event
    log.debug("cleanup.interrupt_watchdog: enter")

    def condition(cfg):
        """Evaluate the condition to continue with the polling loop of interrupt_watchdog().
        """
        # we want to shut down this thread in case a global shutdown signal is received
        p = signal_received()
        if util.superuser():
            r = util.slurm_query_job_change(cfg)
            return (not p) and (not r)
        else:
            return (not p)

    while condition(cfg):
        log.debug("cleanup.interrupt_watchdog: polling")
        stop_watchdog_event.wait(cfg['daemon']['suspend_poll_time'])
        if stop_watchdog_event.is_set():
            log.debug("cleanup.interrupt_watchdog: stop_watchdog_event")
            break

    log.debug("cleanup.interrupt_watchdog: exit")
