# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module responsible for querying network related counters.
"""

import math

from .tool import Tool
from . import primitives as pr
from . import util
from . import log


class Network(Tool):
    def __init__(self, c):
        """Construct network."""
        super(Network, self).__init__(c, "network")

    def map_counter(self, key):
        """Remapping network counters to their default names, so that they are
        equal for every network layer (ip/ib/opa).
        """
        return {
            'rx_bytes': 'rx_bytes',
            'port_rcv_data': 'rx_bytes',
            'rcv_data': 'rx_bytes',
            'tx_bytes': 'tx_bytes',
            'port_xmit_data': 'tx_bytes',
            'xmit_data': 'tx_bytes',
            'rx_packets': 'rx_packets',
            'port_rcv_packets': 'rx_packets',
            'rcv_packets': 'rx_packets',
            'tx_packets': 'tx_packets',
            'port_xmit_packets': 'tx_packets',
            'xmit_packets': 'tx_packets',
            'ib_faults': 'net_faults',
        }.get(key, 'net_error')

    def execute(self, cfg):
        """Run network stats based on the configuration passed via cfg and return
        the outdata in a dictionary."""
        # Data is a dictionary of dictionaries: each network source has its own dictionary
        outdata = {}
        if (cfg[self.ttag]['ip']['enable']):
            cmd = "ip -s link show " + cfg[self.ttag]['ip']['link_name']
            log.debug(cmd)
            raw, _exit_status = util.run(cmd)
            out = util.get_ascii_lines(raw)
            delindex = [idx for idx, s in enumerate(out) if 'RX:' in s][0]
            rawdata = [out[delindex + 1], out[delindex + 3]]
            log.debug(rawdata)
            outdata['ip'] = self._parse_ipstat_output(rawdata)
        if (cfg[self.ttag]['ib']['enable']):
            outdata['ib'] = self._parse_ibstat_output(cfg)
        if (cfg[self.ttag]['opa']['enable']):
            cmd = "/usr/sbin/opainfo"
            log.debug(cmd)
            raw, _exit_status = util.run(cmd)
            out = util.get_ascii_lines(raw)
            delindex = [idx for idx, s in enumerate(out) if 'LID' in s][0]
            rawdata = [out[delindex + 2], out[delindex + 3]]
            log.debug(rawdata)
            outdata['opa'] = self._parse_opastat_output(rawdata)
        return self._compute_diff(cfg, outdata)

    def update_summary(self, cfg, outdata):
        """Update summary file with the network stats.
        Note: currently sent and received data from all source (Eth, IB, OPA)
        is summed up into single values.
        """
        rx_bytes = 0
        tx_bytes = 0
        for netsrc in outdata:
            if 'rx_bytes' in outdata[netsrc]:
                rx_bytes += outdata[netsrc]['rx_bytes']
            if 'tx_bytes' in outdata[netsrc]:
                tx_bytes += outdata[netsrc]['tx_bytes']
        dt = cfg["daemon"]["loop_dt"]
        rx_ratio = int(math.ceil(pr.try_div(int(rx_bytes), dt)))
        tx_ratio = int(math.ceil(pr.try_div(int(tx_bytes), dt)))
        util.update_stat_avg(cfg, 'AVG_R_NET', rx_ratio)
        util.update_stat_max(cfg, 'PEAK_R_NET', rx_ratio)
        util.update_stat_avg(cfg, 'AVG_W_NET', tx_ratio)
        util.update_stat_max(cfg, 'PEAK_W_NET', tx_ratio)

    def print_sample(self, cfg, outdata):
        """Printing data collected for each network layer."""
        for netsrc in outdata:
            msg = []
            msg.append(pr.keqv('type', netsrc))
            for key in outdata[netsrc]:
                msg.append(pr.keqdv(key, outdata[netsrc]))
            log.prefixed_message(' '.join(msg), cfg, self.ttag)

########################################
# Local helper functions

    def _parse_ipstat_output(self, rawdata):
        """Obtain the stats for TCP/IP network and return them in a dictionary."""
        # Data is a dictionary of dictionaries: each nodeid has its own dictionary
        outdata = {}
        # Number of NUMA nodes
        rxline = str(rawdata[0]).split()
        txline = str(rawdata[1]).split()
        if ('rx_bytes' in self.id_list):
            outdata[self.map_counter('rx_bytes')] = int(rxline[0])
        if ('rx_packets' in self.id_list):
            outdata[self.map_counter('rx_packets')] = int(rxline[1])
        if ('tx_bytes' in self.id_list):
            outdata[self.map_counter('tx_bytes')] = int(txline[0])
        if ('tx_packets' in self.id_list):
            outdata[self.map_counter('tx_packets')] = int(txline[1])
        if ('ib_faults' in self.id_list):
            outdata[self.map_counter('ib_faults')] = int(rxline[2]) + int(rxline[3]) + int(rxline[4]) + int(txline[2]) + int(txline[3]) + int(txline[4])
        return outdata

    def _parse_ibstat_output(self, cfg):
        """Obtain the stats for IB network and return them in a dictionary."""
        port_path = str(cfg[self.ttag]['ib']['port_path'] + "/")
        outdata = {}
        for key in self.id_list:
            with open(str(port_path + key)) as f:
                value = int(next(f))
                if ('_data') in key:
                    value *= 4
                outdata[self.map_counter(key)] = value
        return outdata

    def _parse_opastat_output(self, rawdata):
        """Obtain the stats for OPA network and return them in a dictionary."""
        # Data is a dictionary of dictionaries: each nodeid has its own dictionary
        outdata = {}
        # Number of NUMA nodes
        rxline = str(rawdata[1]).split()
        txline = str(rawdata[0]).split()
        if ('rcv_data' in self.id_list):
            outdata[self.map_counter('rcv_data')] = int(rxline[2]) * 1024 * 1024
        if ('rcv_packets' in self.id_list):
            outdata[self.map_counter('rcv_packets')] = int(rxline[5])
        if ('xmit_data' in self.id_list):
            outdata[self.map_counter('xmit_data')] = int(txline[2]) * 1024 * 1024
        if ('xmit_packets' in self.id_list):
            outdata[self.map_counter('xmit_packets')] = int(txline[5])
        return outdata

    def _compute_diff(self, cfg, newdata):
        """Compute the difference between the raw counter values."""
        diffdata = {}
        if cfg['stats']['iter'] > 1:
            for netsrc in newdata:
                diffdata[netsrc] = {}
                for key in newdata[netsrc]:
                    diffdata[netsrc][key] = max(0, newdata[netsrc][key] - self.olddata[netsrc][key])
        self.olddata = newdata
        return diffdata
