# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module to trace the physical measurements from sensors of a node.
"""

from collections import OrderedDict

from .tool import Tool
from . import primitives as pr
from . import util
from . import log


class Physics(Tool):
    def __init__(self, c):
        """Construct physics."""
        super(Physics, self).__init__(c, "physics")
        self.enabled = self.enabled and c[self.ttag]['ipmi']['enable']
        self.lockfile = c[self.ttag]['ipmi']['lockfile']

    def execute(self, cfg):
        """Perform physics process monitoring.
        """
        cmd = "ipmitool sdr"
        log.debug(cmd)
        ##################
        # CRITICAL SECTION
        try:
            util.lock(self.lockfile)
        except Exception as e:
            log.debug(e)
            return {}
        raw, _exit_status = util.run(cmd)
        util.unlock(self.lockfile)
        ##################
        rawdata = util.get_ascii_lines(raw)
        log.debug(rawdata)
        return self._parse_ipmi_output(rawdata)

    def update_summary(self, cfg, outdata):
        """Update summary file with the physics overhead stats."""
        for key in outdata:
            name = str("AVG_" + key.upper())
            util.update_stat_avg(cfg, name, outdata[key])

    def print_sample(self, cfg, outdata):
        """Printing data collected for physics."""
        msg = []
        for key in outdata:
            msg.append(pr.keqdv(key, outdata))
        log.prefixed_message(' '.join(msg), cfg, "phy")

########################################
# Local helper functions

    def _parse_ipmi_output(self, rawdata):
        """Obtain the total power."""
        outdata = OrderedDict()
        power_sum = 0
        for line in rawdata:
            if 'Watts' in line:
                l = str(line).split(' ')
                power_sum += float(l[l.index('Watts') - 1])
        outdata['power'] = power_sum
        return outdata
