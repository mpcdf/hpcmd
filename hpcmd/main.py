# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module containing the startup function main and the (infinite) main loop,
from which other operations are called.
"""

import os
import sys
import signal
import platform
from collections import OrderedDict
from . import config
from . import cleanup
from . import log
from . import util
from . import slurm
from . import version
from . import primitives as pr
from .primitives import keqv
from .perf import Perf
from .memory import Memory
from .network import Network
from .software import Software
from .filesystem import Filesystem
from .nvgpu import Nvgpu
from .system import System
# from .physics import Physics
from .sleep import Sleep

def main(arg):
    """Entry point of the hpcmd main program.

    Reads the config and performs various checks on it, handles command line
    arguments before it calls the main loop.
    """

    if arg.version:
        print(version.get_printable_version_string())
        sys.exit()

    # set up initial configuration
    cfg = config.setup(arg)
    config.extend(cfg)
    config.environment(cfg)
    config.sanitize(cfg)

    # terminate an already running instance of hpcmd and exit
    if arg.terminate:
        try:
            util.terminate(cfg)
        except Exception as e:
            print("Could not find any running instance of hpcmd.")
        sys.exit()

    # kill an already running instance of hpcmd and exit
    if arg.kill:
        try:
            util.kill(cfg)
        except Exception as e:
            print("Could not find any running instance of hpcmd.")
        sys.exit()

    if util.superuser():
        # create '/var/lib/hpcmd/' in case it does not exist
        util.md(cfg['daemon']['working_directory'] + '/')

    slurm.setup(cfg)
    config.check(cfg)

    # debug output
    if arg.debug:
        cfg['output']['debug'] = True

    # enable stdout/stderr
    if arg.stdout:
        cfg['output']['stdout'] = True
        cfg['output']['out_file'] = "stdout"
        cfg['output']['err_file'] = "stderr"

    # write a user-defined msg or 'variable=value' to hpcmd output and/or syslog and exit
    if arg.msg:
        try:
            msg = ""
            if "=" in arg.msg:
                msg_list = list(arg.msg.split('='))
                msg = keqv(msg_list[0], msg_list[1])
            else:
                msg = keqv("msg", arg.msg)
            log.prefixed_message(msg, cfg, "user_defined")
            pr.flush(sys.stdout)
        except Exception as e:
            print("Printing user-defined msg failed.")
        sys.exit()

    # option to prevent the daemon from suspending,
    # useful when running it via CLI as root
    if arg.nosuspend:
        cfg['daemon']['nosuspend'] = True
    else:
        cfg['daemon']['nosuspend'] = False

    # --- proceed with launching hpcmd ---

    out_stream, err_stream = util.get_output_streams(cfg)
    log.setup(cfg, err_stream)

    cleanup.setup(cfg)

    # initializing tools and counters
    active_tools = OrderedDict()
    toolnames = ['sleep', 'perf', 'nvgpu', 'memory', 'network', 'software', 'filesystem', 'system']
    for t in toolnames:
        classname = t.capitalize()
        tool = globals()[classname](cfg)
        tool.setup(cfg)
        log.debug(tool)
        if tool.enabled:
            active_tools[t] = tool
    # reset summary stats
    util.reset_stats_counters(cfg)

    # write a copy of the configuration, for debug purposes
    config.write(cfg, cfg["output"]["cfg_out"],
                 comment="hpcmd: configuration snapshot before entering the main loop")

    if not util.superuser():
        util.unbind()

    util.nice()
    log.debug("SETUP FINISHED")

    if (not util.superuser()) and cfg["slurm"]["enable"]:
        if util.wait_for_suspend_confirmation(cfg):
            pr.create_epilog(cfg)
            print("Launching daemon in user-mode on node {}.".format(cfg["hostname"]))
            sys.stdout.flush()
        else:
            print("Daemon-launch in user-mode failed on node {}.".format(cfg["hostname"]))
            sys.exit(1)

    # from now on, apply stream redirection
    sys.stdout = out_stream
    sys.stderr = err_stream
    if arg.daemon:
        try:
            import daemon
            from daemon import pidfile
        except:
            print("Install optional 'python-daemon' to enable standalone background daemonization. STOP.")
            sys.exit(1)
        else:
            # run hpcmd as a daemon in the background
            context = daemon.DaemonContext(
                working_directory=cfg['daemon']['working_directory'],
                pidfile=pidfile.TimeoutPIDLockFile(cfg['daemon']['pid_file'])
            )
            context.signal_map = {signal.SIGTERM: cleanup.handler}
            with context:
                loop(cfg, active_tools)
    else:
        # run hpcmd in the foreground
        os.chdir(cfg['daemon']['working_directory'])
        signal.signal(signal.SIGINT, cleanup.handler)
        signal.signal(signal.SIGTERM, cleanup.handler)
        loop(cfg, active_tools)


def loop(cfg, active_tools):
    """Main loop, infinite until a shutdown signal is received."""
    log.message("started, using {}".format(cfg["cfg_file_name"]), cfg)
    log.message("version: {}".format(cfg["hpcmd_version"]), cfg)
    log.debug("hpcmd package location: {}".format(os.path.dirname(os.path.abspath(__file__))))

    if cfg['daemon']['loop_epoch'] > 0:
        msg = "loop: epoch = {}".format(cfg['daemon']['loop_epoch'])
    else:
        msg = "loop: sleep = {}".format(cfg['daemon']['loop_sleep'])
    log.message("{}, awake = {}".format(msg, cfg['daemon']['loop_awake']), cfg)

    n_jobs = 1
    while not cleanup.signal_received():
        log.timer_start()

        msg = ""
        conditional_sleep = False
        if cfg["daemon"]["suspend"]:
            suspend = True
            msg = "suspended: node blacklisted"
        elif util.superuser() and cfg["slurm"]["enable"]:
            # do node update at each iteration because the job on the node may have changed
            n_jobs = slurm.get_n_concurrent_jobs_on_node(cfg)
            if n_jobs == 0:
                suspend = True
                conditional_sleep = True
                pr.del_job_info(cfg)
                msg = "suspended: node idle"
            elif n_jobs > 1:
                suspend = True
                conditional_sleep = True
                pr.del_job_info(cfg)
                msg = "suspended: node shared"
            else:
                suspend = util.query_suspend_file(cfg)
                if suspend:
                    conditional_sleep = True
                    pr.del_job_info(cfg)
                    msg = "suspended: suspend file"
                else:
                    slurm.update(cfg)
                    # catch the idle case that may result from the update call
                    if util.slurm_get_saved_job_id(cfg) == None:
                        suspend = True
                        conditional_sleep = True
                        pr.del_job_info(cfg)
                        msg = "suspended: node idle"
                    else:
                        msg = "monitoring job {}".format(util.slurm_get_saved_job_id(cfg))
            log.timer_restart("update slurm information")
        else:
            suspend = False
            if cfg["output"]["epilog"] and pr.headnode(cfg):
                pr.create_epilog(cfg)

        if msg:
            log.message(msg, cfg)

        if (not suspend) or cfg['daemon']['nosuspend']:
            # clean potential remnants from previous jobs once at the beginning
            if cfg["output"]["epilog"] and (not cfg["epilog_cleaned"]):
                pr.rm(cfg['output']['epilog_file'])
                cfg["epilog_cleaned"] = True
            # print the 'prolog-like' message once per job at the beginning
            if cfg["output"]["intro"] and (not cfg["intro_logged"]) and pr.headnode(cfg) and (not pr.is_multijob(cfg)):
                imsg = []
                util.intro(imsg, cfg)
                slurm.intro(imsg, cfg)
                for tool in active_tools.values():
                    tool.intro(imsg)
                log.prefixed_message(' '.join(imsg), cfg, "job_start")
                cfg["intro_logged"] = True

            cfg['stats']['iter'] += 1

            # launch measurements asynchronously
            if "perf" in active_tools:
                active_tools["perf"].spawn(cfg)
            if "nvgpu" in active_tools:
                active_tools["nvgpu"].spawn(cfg)
            # wait, and collect metrics
            for tool in active_tools.values():
                tool.run(cfg)

            if cfg["output"]["epilog"] and pr.headnode(cfg):
                log.write_summary(cfg)

        pr.flush(sys.stdout)
        pr.flush(sys.stderr)

        if cfg['daemon']['loop_epoch'] > 0:
            sleep_time = util.get_sleep_time(cfg['daemon']['loop_epoch'])
        else:
            sleep_time = cfg['daemon']['loop_sleep']

        if conditional_sleep or cfg["daemon"]["suspend"] or (not util.slurm_query_job_change(cfg)):
            cleanup.sleep(cfg, sleep_time)

        log.timer_stop("iteration end")

    else:
        cleanup.clean()
        log.message("stopped cleanly", cfg)
