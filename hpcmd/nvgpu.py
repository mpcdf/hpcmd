# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.


# Example outputs of the tools and how we call them:
#
# $ dcgmi dmon -d 3000 -e 525,1003
## Entity       VMUSG  SMOCC
#      Id
#    GPU 0           0  0.000
#    GPU 1           0  0.000
#    GPU 0           0  0.000
#    GPU 1           0  0.000
#
# $ nvidia-smi --loop-ms=3000 --format=csv,noheader,nounits --query-gpu=index,utilization.gpu,memory.used,power.draw
# 0, 0, 0, 26.24
# 1, 0, 0, 24.58
# 0, 0, 0, 26.34
# 1, 0, 0, 24.58


"""Module responsible for querying NVIDIA GPUs via DCGM and nvidia-smi

Makes the previous gpu and nvdcgm modules obsolete.
"""

from collections import OrderedDict
import re
import signal
import time

from .tool import Tool
from . import util
from . import log
from . import primitives as pr

# One Billion = One Giga
Giga = 1000000000.0

class Nvgpu(Tool):
    def __init__(self, c):
        super(Nvgpu, self).__init__(c, "nvgpu", "nvidia gpu monitoring via dcgm and nvidia-smi")

    def setup(self, cfg):
        """For slurm jobs, disable GPU monitoring in case we're not on a GPU node.
        """
        # check if we are on a a GPU node
        if self.enabled and cfg["slurm"]["enable"] and not any("gpu" in feat for feat in cfg["slurm"]["node"]["feature"]):
            log.debug("disable nvdcgm on non-gpu node")
            self.enabled = False
            cfg[self.ttag]['enable'] = False
        # check if we have `nvidia-smi` and `dcgmi dmon`
        if self.enabled:
            if not self._have_nvidia_smi():
                self.enabled = False
                cfg[self.ttag]['enable'] = False
                cfg[self.ttag]['nvidia-smi']['enable'] = False
                log.debug("`nvidia-smi` not available, disable nvgpu")
            if not self._have_dcgmi_dmon():
                cfg[self.ttag]['dcgmi']['enable'] = False
                cfg[self.ttag]['dcgmi']['dmon']['enable'] = False
                log.debug("`dcgmi dmon` not available, disable nvgpu")
        # check if our GPUs are compatible with the advanced metrics from `dcgmi dmon`
        if self.enabled:
            self.gpu_name = self._get_gpu_model()
            log.debug(self.gpu_name)
            if self.gpu_name and cfg[self.ttag]["dcgmi"]["dmon"]["enable"]:
                dev_compatible = False
                for dev in cfg[self.ttag]["dcgmi"]["dmon"]["compatible"]:
                    if dev in self.gpu_name:
                        dev_compatible = True
                        log.debug("enabling DCGM for GPU: " + str(self.gpu_name))
                        break
                if not dev_compatible:
                    log.debug("disabling DCGM for GPU: " + str(self.gpu_name))
                    cfg[self.ttag]['dcgmi']['enable'] = False
                    cfg[self.ttag]["dcgmi"]["dmon"]["enable"] = False
        if self.enabled:
            self.dcgm_dmon_events = OrderedDict()
            self.dcgm_dmon_n_events = 0
            self.dcgm_dmon_command = ""
            self.dcgm_subprocess_handle = None
            self.nvsmi_events = []
            self.nvsmi_n_events = 0
            self.nvsmi_command = ""
            self.nvsmi_subprocess_handle = None
        super(Nvgpu, self).setup(cfg)

    def specific_setup(self, cfg):
        """Set-up the nvidia commands based on the configuration passed via cfg."""
        if cfg[self.ttag]["dcgmi"]["dmon"]["enable"]:
            self.dcgmi_dmon_loopms = int(cfg[self.ttag]["dcgmi"]["dmon"]["loop-ms"])
            self.dcgmi_dmon_convert_to_rate = cfg[self.ttag]["dcgmi"]["dmon"]["convert_to_rate"]
            events = ",".join(self.dcgm_dmon_events.values())
            self.dcgm_dmon_command = "dcgmi dmon -d {} -e {}".format(self.dcgmi_dmon_loopms, events)
            log.debug(self.dcgm_dmon_command)
        if cfg[self.ttag]["nvidia-smi"]["enable"]:
            loopms = "{}".format(cfg[self.ttag]["nvidia-smi"]["loop-ms"])
            events = ",".join(["index"] + self.nvsmi_events)
            self.nvsmi_command = "nvidia-smi --format=csv,noheader,nounits --loop-ms={} --query-gpu={}".format(loopms, events)
            log.debug(self.nvsmi_command)
        # Prolog-like message
        self.intro_id_list = []
        if cfg[self.ttag]["nvidia-smi"]["enable"]:
            for (key, val) in cfg[self.ttag]["nvidia-smi"]["intro"].items():
                if val:
                    self.intro_id_list.append(key)
        self.intro_str = ",".join(self.intro_id_list)

    def specific_intro(self, msg):
        """Run the nvidia-smi command and append the intro data to the msg."""
        raw = self._execute_intro()
        out = self._parse_intro(raw)
        self._append_intro(msg, out)

    def get_events(self, cfg):
        """Collect enabled event and field names from config file."""
        if cfg[self.ttag]["dcgmi"]["dmon"]["enable"]:
            for event in cfg[self.ttag]["dcgmi"]["dmon"]["event"]:
                if cfg[self.ttag]["dcgmi"]["dmon"]["event"][event]["enable"]:
                    self.dcgm_dmon_events[event] = str(cfg[self.ttag]["dcgmi"]["dmon"]["event"][event]["field"])
            self.dcgm_dmon_n_events = len(self.dcgm_dmon_events)
        if cfg[self.ttag]["nvidia-smi"]["enable"]:
            for (key, val) in cfg[self.ttag]["nvidia-smi"]["event"].items():
                if val:
                    self.nvsmi_events.append(key)
            self.nvsmi_n_events = len(self.nvsmi_events)
        return []

    def spawn(self, cfg):
        """Launch instance of dcgm and run it in the background.
        """
        if cfg[self.ttag]["nvidia-smi"]["enable"]:
            self.nvsmi_subprocess_handle = util.popen(self.nvsmi_command)
            log.debug(self.nvsmi_subprocess_handle.poll() is None)
        if cfg[self.ttag]["dcgmi"]["dmon"]["enable"]:
            self.dcgm_subprocess_handle = util.popen(self.dcgm_dmon_command)
            log.debug(self.dcgm_subprocess_handle.poll() is None)

    def execute(self, cfg):
        """Join the nvidia-smi and dcgmi commands and return the outdata in a dictionary."""
        data = {}
        data_nvsmi = {}
        data_dcgmi = {}
        if cfg[self.ttag]["nvidia-smi"]["enable"]:
            out = util.join_and_get_output(self.nvsmi_subprocess_handle, send_sigterm=True, send_sigkill=True)
            raw = util.get_ascii_lines(out)
            data_nvsmi = self._parse_nvidia_smi_output(cfg, raw)
        if cfg[self.ttag]["dcgmi"]["dmon"]["enable"]:
            out = util.join_and_get_output(self.dcgm_subprocess_handle, send_sigterm=True, send_sigkill=True)
            raw = util.get_ascii_lines(out)
            data_dcgmi = self._parse_dcgm_dmon_output(cfg, raw)
        for dic in [data_nvsmi, data_dcgmi]:
            for gpu in dic.keys():
                if not gpu in data:
                    data[gpu] = {}
                data[gpu].update(dic[gpu])
        return data

    def update_summary(self, cfg, outdata):
        """Update summary file with AVG GPU utilization."""
        total_gpus = 0
        gpus_used = 0
        avg_gpu = 0.0
        avg_mem = 0.0
        avg_smocc = 0.0
        avg_smact = 0.0
        avg_dram_active = 0.0
        avg_fp64_active = 0.0
        avg_fp32_active = 0.0
        avg_fp16_active = 0.0
        avg_tensor_active = 0.0
#        avg_pcie_tx = 0.0
#        avg_pcie_rx = 0.0
#        avg_nvlink_tx = 0.0
#        avg_nvlink_rx = 0.0

        # Collect utilization for each device
        for gpuid in outdata:
            total_gpus += 1
            if 'utilization.gpu' in outdata[gpuid]:
                if outdata[gpuid]["utilization.gpu"] > 0.0:
                    gpus_used += 1
                    avg_gpu += outdata[gpuid]["utilization.gpu"]
            if 'memory.used' in outdata[gpuid]:
                avg_mem += outdata[gpuid]["memory.used"]
            if 'sm_active' in outdata[gpuid]:
                avg_smact += outdata[gpuid]["sm_active"]
            if 'sm_occupancy' in outdata[gpuid]:    
                avg_smocc += outdata[gpuid]["sm_occupancy"]
            if 'dram_active' in outdata[gpuid]:    
                avg_dram_active += outdata[gpuid]["dram_active"]
            if 'tensor_active' in outdata[gpuid]:
                avg_tensor_active += outdata[gpuid]["tensor_active"]
            if 'fp64_active' in outdata[gpuid]:
                avg_fp64_active += outdata[gpuid]["fp64_active"]
            if 'fp32_active' in outdata[gpuid]:
                avg_fp32_active += outdata[gpuid]["fp32_active"]
            if 'fp16_active' in outdata[gpuid]:
                avg_fp16_active += outdata[gpuid]["fp16_active"]
#            if 'pcie_tx_bytes' in outdata[gpuid]:
#                avg_pcie_tx += outdata[gpuid]["pcie_tx_bytes"]
#            if 'pcie_rx_bytes' in outdata[gpuid]:
#                avg_pcie_rx += outdata[gpuid]["pcie_rx_bytes"]
#            if 'nvlink_tx_bytes' in outdata[gpuid]:
#                avg_nvlink_tx += outdata[gpuid]["nvlink_tx_bytes"]
#            if 'nvlink_rx_bytes' in outdata[gpuid]:
#                avg_nvlink_rx += outdata[gpuid]["nvlink_rx_bytes"]

        util.update_stat_max(cfg, 'GPUS_USED', gpus_used)
        avg_gpu = float(pr.try_div(avg_gpu, total_gpus))
        util.update_stat_avg(cfg, 'AVG_GPU', avg_gpu)
        avg_mem = int(pr.try_div(avg_mem, total_gpus))
        util.update_stat_avg(cfg, 'AVG_GPU_MEM', avg_mem)
        avg_smact = float(pr.try_div(avg_smact, total_gpus))
        util.update_stat_avg(cfg, 'AVG_GPU_SMACT', avg_smact)
        avg_smocc = float(pr.try_div(avg_smocc, total_gpus))
        util.update_stat_avg(cfg, 'AVG_GPU_SMOCC', avg_smocc)
        avg_dram_active = float(pr.try_div(avg_dram_active, total_gpus))
        util.update_stat_avg(cfg, 'AVG_GPU_DRAM_ACTIVE', avg_dram_active)
        avg_tensor_active = float(pr.try_div(avg_tensor_active, total_gpus))
        util.update_stat_avg(cfg, 'AVG_GPU_TENSOR_ACTIVE', avg_tensor_active)
        avg_fp64_active = float(pr.try_div(avg_fp64_active, total_gpus))
        util.update_stat_avg(cfg, 'AVG_GPU_FP64_ACTIVE', avg_fp64_active)
        avg_fp32_active = float(pr.try_div(avg_fp32_active, total_gpus))
        util.update_stat_avg(cfg, 'AVG_GPU_FP32_ACTIVE', avg_fp32_active)
        avg_fp16_active = float(pr.try_div(avg_fp16_active, total_gpus))
        util.update_stat_avg(cfg, 'AVG_GPU_FP16_ACTIVE', avg_fp16_active)
#        avg_pcie_tx = float(pr.try_div(avg_pcie_tx, total_gpus))
#        util.update_stat_avg(cfg, 'AVG_PCIE_TX', avg_pcie_tx)
#        avg_pcie_rx = float(pr.try_div(avg_pcie_rx, total_gpus))
#        util.update_stat_avg(cfg, 'AVG_PCIE_RX', avg_pcie_rx)
#        avg_nvlink_tx = float(pr.try_div(avg_nvlink_tx, total_gpus))
#        util.update_stat_avg(cfg, 'AVG_GPU_NVLINK_TX', avg_nvlink_tx)
#        avg_nvlink_rx = float(pr.try_div(avg_nvlink_rx, total_gpus))
#        util.update_stat_avg(cfg, 'AVG_GPU_NVLINK_RX', avg_nvlink_rx)


    def print_sample(self, cfg, outdata):
        """Printing data collected for GPUs, one line per GPU device."""
        for gpu in outdata:
            msg = []
            msg.append(pr.keqv("gpu", gpu))
            for key, val in outdata[gpu].items():
                msg.append(pr.keqv(key, val))
            log.prefixed_message(' '.join(msg), cfg, "gpu")

    def _perform_averaging(self, data):
        """Average over a list of numbers, the last element containing the number of samples."""
        n_samples = data[-1]
        n_elements = len(data) - 1
        if n_samples > 1:
            for i in range(n_elements):
                data[i] /= float(n_samples)
        data.pop()

    def _parse_dcgm_dmon_output(self, cfg, rawdata):
        """Process the raw ascii output of `dcgm dmon`, sum up and avarage values."""
        ## Entity       VMUSG  SMOCC
        #      Id
        #    GPU 0           0  0.000
        #    GPU 1           0  0.000
        #    GPU 0           0  0.000
        #    GPU 1           0  0.000
        data = {}
        # sum up values
        for line in rawdata:
            line = line.strip()
            if line.startswith("GPU"):
                tokens = line.split()
                tokens.pop(0)
                gpu = "D{}".format(tokens.pop(0))
                if gpu not in data:
                    data[gpu] = []
                    for i in range(self.dcgm_dmon_n_events):
                        data[gpu].append(0.0)
                    # last element shall contain the counter
                    data[gpu].append(0)
                for i in range(self.dcgm_dmon_n_events):
                    data[gpu][i] += float(tokens[i])
                data[gpu][-1] += 1
        for gpu in data:
            self._perform_averaging(data[gpu])
        labeled_data = {}
        for gpu in data:
            labeled_data[gpu] = {}
            for key, val in zip(self.dcgm_dmon_events.keys(), data[gpu]):
                if key in self.dcgmi_dmon_convert_to_rate:
                    val = val / (float(self.dcgmi_dmon_loopms)/1000.0) / Giga
                    key = key.replace("_bytes", "_gbps")
                labeled_data[gpu][key] = val
        return labeled_data

    def _parse_nvidia_smi_output(self, cfg, rawdata):
        """Process the raw ascii output of `nvidia-smi`, sum up and avarage values."""
        # 0, 0, 0, 26.24
        # 1, 0, 0, 24.58
        # 0, 0, 0, 26.34
        # 1, 0, 0, 24.58
        data = {}
        # sum up values
        for line in rawdata:
            tokens = [tok.strip() for tok in line.split(',')]
            gpu = "D{}".format(tokens.pop(0))
            if gpu not in data:
                data[gpu] = []
                for i in range(self.nvsmi_n_events):
                    data[gpu].append(0.0)
                # last element shall contain the counter
                data[gpu].append(0)
            for i in range(self.nvsmi_n_events):
                data[gpu][i] += float(tokens[i])
            data[gpu][-1] += 1
        for gpu in data:
            self._perform_averaging(data[gpu])
        labeled_data = {}
        for gpu in data:
            labeled_data[gpu] = {}
            for key, val in zip(self.nvsmi_events, data[gpu]):
                labeled_data[gpu][key] = val
        return labeled_data

    # --- prolog-like message related local functions below ---

    def _execute_intro(self):
        """Run the nvidia-smi command to query the available GPUs."""
        command = "nvidia-smi --query-gpu={} --format=csv,noheader".format(self.intro_str)
        log.debug(command)
        raw, _exit_status = util.run(command)
        rawdata = util.get_ascii_lines(raw)
        log.debug(rawdata)
        return rawdata

    def _parse_intro(self, rawdata):
        """Obtain GPU performance numbers and return them in a dictionary.

        Note: Currently we are saving only the results for the GPU0,
        as in practice GPUs are always the same on a single node.
        """
        outdata = {}
        for i, line in enumerate(rawdata):
            log.debug(line)
            line = line.split(', ')
            gpu_id = "D{}".format(i)
            outdata[gpu_id] = {}
            for idx, key in enumerate(self.intro_id_list):
                log.debug(key)
                outdata[gpu_id][key] = line[idx]
            # save GPU model name
            # key = "name"
            # if key in outdata[gpu_id]:
                # self.gpu_name = outdata[gpu_id][key]
            # At the moment decided to print only for GPU0 if we later
            # decide to change this, just remove the following line
            break
        log.debug(outdata)
        return outdata

    def _append_intro(self, msg, outdata):
        """Append gpu 'prolog-like' info for the nodes that have gpus.
        """
        for gpuid in outdata:
            if bool(gpuid):
                msg.append(pr.keqv("gpu_device", gpuid))
            for key in outdata[gpuid]:
                msg.append(pr.keqv(str("gpu_" + key), outdata[gpuid][key]))

    def _get_gpu_model(self):
        model = ""
        command = "nvidia-smi --query-gpu=index,name --format=csv,noheader"
        raw, exit_status = util.run(command)
        if exit_status == 0:
            out = util.get_ascii_lines(raw)
            if len(out) > 0:
                tokens = out[0].split(',')
                model = tokens[1].strip()
        return model.lower()

    def _have_nvidia_smi(self):
        command = "nvidia-smi --help"
        _raw, exit_status = util.run(command)
        return exit_status == 0

    def _have_dcgmi_dmon(self):
        command = "dcgmi dmon --help"
        _raw, exit_status = util.run(command)
        return exit_status == 0
