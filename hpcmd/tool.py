# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module containing abstract class Tool and unities all the measurement tools,
together with a ToolsFactory class for creating these tools.
"""

from . import log
from . import primitives as pr


class Tool(object):
    """Abstract class that unities all the tools."""

    def __init__(self, cfg, ttag="tool", logname=None):
        """Default constructor for each tool."""
        self.ttag = ttag
        if (ttag in cfg) and ('enable' in cfg[ttag]):
            self.enabled = cfg[ttag]['enable']
        else:
            self.enabled = False
        if logname:
            self.logname = logname
        else:
            self.logname = ttag
        self.id_list = []

    def __str__(self):
        """Default print of the tool state."""
        msg = []
        msg.append("ttag: " + str(self.ttag) +
                   "; logname: " + str(self.logname) +
                   "; Enabled: " + str(self.enabled))
        if len(self.id_list) != 0:
            msg.append("id_list: " + str(self.id_list))
        return "\n".join(msg)

    def setup(self, cfg):
        """[wrapper] Setup the events based on the configuration passed via cfg."""
        if self.enabled:
            try:
                self.sanity_check(cfg[self.ttag])
                self.id_list = self.get_events(cfg)
                self.specific_setup(cfg)
            except Exception as e:
                log.exception(e)
                log.debug("setup error: disable monitoring " + self.logname)
                self.enabled = False
                cfg[self.ttag]['enable'] = False

    def sanity_check(self, cfg):
        """Verify that enabled dictionary prints at least every sample of final summary.
        If not, disable the tool.
        """
        if (not cfg['print_samples']) and (not cfg['print_summary']):
            log.debug("enable: {}; print_samples: {}; print_summary: {}"
                      .format(cfg['enable'], cfg['print_samples'], cfg['print_summary']))
            raise RuntimeError("Tool enabled, but nothing should be printed.")

    def get_events(self, cfg):
        """Get true events from a tool dictionary cfg.
        """
        event_str = ""
        event_id_list = []
        for items in list(cfg[self.ttag].keys()):
            nested_dict = cfg[self.ttag][items]
            if isinstance(nested_dict, dict) and nested_dict['enable'] and 'events' in nested_dict.keys():
                _keys = list(nested_dict['events'].keys())
                for key in _keys:
                    if nested_dict['events'][key]:
                        event_str = "%s,%s" % (event_str, key)
        event_str = event_str[1:]
        log.debug(event_str)
        event_id_list = list(event_str.split(','))
        log.debug(event_id_list)
        return event_id_list

    def specific_setup(self, cfg):
        """[empty] Part of the setup specific to a particular tool."""
        pass

    def map_counter(self, key):
        """[virtual] Map counter name to more human-readable name."""
        raise NotImplementedError()

    def intro(self, imsg):
        """[wrapper] Add prolog-like type of message."""
        if self.enabled:
            try:
                self.specific_intro(imsg)
            except Exception as e:
                log.exception(e)
                log.debug("intro error: disable {}".format(self.logname))
                self.enabled = False
        else:
            log.debug("{} disabled".format(self.logname))

    def specific_intro(self, imsg):
        """[empty] Part of the intro specific to a particular tool."""
        pass

    def run(self, cfg):
        """[wrapper] Run tool."""
        if self.enabled:
            try:
                outdata = self.execute(cfg)
                if outdata:
                    if pr.headnode(cfg) and cfg[self.ttag]['print_summary']:
                        self.update_summary(cfg, outdata)
                    if cfg[self.ttag]['print_samples']:
                        self.print_sample(cfg, outdata)
                else:
                    log.debug("{}: misconfigured, no counters available or first iteration is not written".format(self.logname))
                log.timer_restart(self.logname)
            except Exception as e:
                log.exception(e)
        else:
            log.debug("{} disabled".format(self.logname))

    def execute(self, cfg):
        """[virtual] Run command, collect useful lines and parse them."""
        raise NotImplementedError()

    def update_summary(self, cfg, outdata):
        """[virtual] Update data and message for job summary."""
        raise NotImplementedError()

    def print_sample(self, cfg, outdata):
        """[virtual] Print sampled output as a message to syslog."""
        raise NotImplementedError()
