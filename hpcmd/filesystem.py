# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module responsible for querying filesystem related counters.
"""

import os
import math

from .tool import Tool
from . import primitives as pr
from . import util
from . import log
from . import cleanup


class Filesystem(Tool):
    def __init__(self, c):
        """Construct filesystem."""
        super(Filesystem, self).__init__(c, "filesystem")

    def specific_setup(self, cfg):
        """Specific setup for filesystem based on the configuration passed via cfg."""
        self.fs_names = cfg[self.ttag]['gpfs']['fs_names'].split(",")
        self.gpfs_file_name = "hpcmd_gpfs.cmd"
        if cfg[self.ttag]['gpfs']['enable']:
            if not util.superuser():
                cfg[self.ttag]['gpfs']['enable'] = False
                raise RuntimeError("Can query gpfs info only as a root.")
            self._write_gpfs_input_file(cfg)
            cleanup.remove_list.append(cfg[self.ttag]['gpfs']['cmd_path'])

    def map_counter(self, key):
        """Remapping gpfs counters to their default names written in input
        configuration files.
        """
        return {
            '_n_': 'ip',
            '_nn_': 'node',
            '_rc_': 'rc',
            '_t_': 'timestamp',
            '_tu': 'tu',
            '_cl_': 'cluster',
            '_fs_': 'filesystem',
            '_d_': 'disks',
            '_br_': 'bytes_read',
            '_bw_': 'bytes_written',
            '_oc_': 'opens',
            '_cc_': 'closes',
            '_rdc_': 'reads',
            '_wc_': 'writes',
            '_dir_': 'readdir',
            '_iu_': 'inode_updates',
        }.get(key, 'gpfs_error')

    def execute(self, cfg):
        """Run the mmpmon command based on the configuration passed via command file and return
        the outdata in a dictionary."""
        # Data is a dictionary of dictionaries: each nodeid and fs combination has its own dictionary
        rawdata = {}
        if cfg[self.ttag]['gpfs']['enable']:
            # Get information about memory stats
            command = "/usr/lpp/mmfs/bin/mmpmon -p -i " + cfg[self.ttag]['gpfs']['cmd_path']
            log.debug(command)
            raw, exit_status = util.run(command)
            if exit_status == 0:
                out = util.get_ascii_lines(raw)
                delindex = [idx for idx, s in enumerate(out) if '_fs_io_s_' in s][0]
                rawdata = out[delindex:]
                log.debug(rawdata)
        outdata = self._parse_gpfs_output(cfg, rawdata)
        return self._compute_diff(cfg, outdata)

    def update_summary(self, cfg, outdata):
        """Update summary file with the filesystem stats."""
        if cfg[self.ttag]['gpfs']['enable'] and cfg[self.ttag]['gpfs']['print_summary']:
            for fsid in outdata:
                dt = cfg["daemon"]["loop_dt"]
                if 'bytes_read' in outdata[fsid]:
                    r_ratio = int(math.ceil(pr.try_div(int(outdata[fsid]['bytes_read']), dt)))
                    util.update_stat_avg(cfg, str('AVG_R_' + fsid).upper(), r_ratio)
                    util.update_stat_max(cfg, str('PEAK_R_' + fsid).upper(), r_ratio)
                if 'bytes_written' in outdata[fsid]:
                    w_ratio = int(math.ceil(pr.try_div(int(outdata[fsid]['bytes_written']), dt)))
                    util.update_stat_avg(cfg, str('AVG_W_' + fsid).upper(), w_ratio)
                    util.update_stat_max(cfg, str('PEAK_W_' + fsid).upper(), w_ratio)
                if 'opens' in outdata[fsid]:
                    util.update_stat_avg(cfg, str('AVG_O_' + fsid).upper(), int(outdata[fsid]['opens']))
                    util.update_stat_max(cfg, str('PEAK_O_' + fsid).upper(), int(outdata[fsid]['opens']))
                if 'closes' in outdata[fsid]:
                    util.update_stat_avg(cfg, str('AVG_O_' + fsid).upper(), int(outdata[fsid]['closes']))
                    util.update_stat_max(cfg, str('PEAK_O_' + fsid).upper(), int(outdata[fsid]['closes']))

    def print_sample(self, cfg, outdata):
        """Printing data collected for gpfs, one line per node and filesystem."""
        if cfg[self.ttag]['gpfs']['enable'] and cfg[self.ttag]['gpfs']['print_samples']:
            for fsid in outdata:
                msg = []
                msg.append(pr.keqv("fsid", fsid))
                for key in outdata[fsid]:
                    msg.append(pr.keqv(str("gpfs_" + key), outdata[fsid][key]))
                log.prefixed_message(' '.join(msg), cfg, "gpfs")

########################################
# Local helper functions

    def _write_gpfs_input_file(self, cfg):
        """Write input file read by mmpmon."""
        cfg[self.ttag]['gpfs']['cmd_path'] = \
            os.path.join(cfg['daemon']['working_directory'], self.gpfs_file_name)
        with open(cfg[self.ttag]['gpfs']['cmd_path'], 'w') as fp:
            fp.write('nlist new ')
            fp.write(cfg['hostname'])
            fp.write('\n')
            fp.write('fs_io_s')
            fp.write('\n')

    def _parse_gpfs_output(self, cfg, rawdata):
        """Obtain the names and numbers from mmpmon output lines and return them in a dictionary."""
        outdata = {}
        for line in rawdata:
            line = line.split()
            log.debug(line)
            # Retrieve filesystem name from the output line produced by mmpmon:
            # Note: For example, draco_u and draco_ptmp are the raw strings (GPFS
            # fs names), we extract u and ptmp here. In the future, we might want
            # to use the real fs names.
            fsid = (line[14].split("_"))[-1]
            log.debug(fsid)
            if (fsid in self.fs_names) or (self.fs_names == ["all"]):
                outdata[fsid] = {}
                for k in range(1, len(line), 2):
                    key = self.map_counter(line[k])
                    value = str(line[k + 1])
                    if key in self.id_list:
                        outdata[fsid][key] = value
        return outdata

    def _compute_diff(self, cfg, newdata):
        """Compute the difference between the raw counter values."""
        diffdata = {}
        if cfg['stats']['iter'] > 1:
            for fsid in newdata:
                diffdata[fsid] = {}
                for key in newdata[fsid]:
                    diffdata[fsid][key] = max(0, int(newdata[fsid][key]) - int(self.olddata[fsid][key]))
        self.olddata = newdata
        return diffdata
