# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""hpcmd's initialization functions.
"""

import os
import socket
import yaml
import getpass

from . import log
from . import util
from . import version
from . import primitives as pr


def get_default_file_name(suffix=".yaml"):
    """Return the name of the default config file that is presently used."""
    config_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config")
    cfg_file_name = os.path.join(config_dir, "default" + suffix)

    log.debug(cfg_file_name)
    return cfg_file_name


def get_config_file_name(arg, cfg, suffix=".yaml"):
    """Return the name of the config file that is presently used."""
    if (arg.config):
        cfg_file_name = arg.config
    else:
        cluster = cfg["cluster"]
        config_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config")
        cfg_systemd_file_name = os.path.join(config_dir, cluster + "_systemd" + suffix)
        if util.superuser() and os.path.isfile(cfg_systemd_file_name):
            cfg_file_name = cfg_systemd_file_name
        else:
            cfg_file_name = os.path.join(config_dir, cluster + suffix)
    log.debug(cfg_file_name)
    return cfg_file_name


def get_cluster_name(cfg):
    """Return cluster name, obtained either from the environment or from the hostname.
    """
    if cfg["setup"]["cluster_detection_by_environment"]["enable"]:
        var = cfg["setup"]["cluster_detection_by_environment"]["variable"]
        if var in os.environ:
            cluster = os.environ[var].lower()
            return cluster
    if cfg["setup"]["cluster_detection_by_hostname"]["enable"]:
        hostname = socket.gethostname()
        map = cfg["setup"]["cluster_detection_by_hostname"]["mapping"]
        for key, val in map.items():
            if hostname.startswith(key):
                cluster = val
                return cluster
    # set a minimal working default
    cluster = "minimal"
    return cluster


def read(file_name):
    """Read YAML configuration file.
    """
    with open(file_name, 'r') as fp:
        cfg = yaml.safe_load(fp)
    cfg["cfg_file_name"] = file_name
    return cfg


def write(cfg, file_name, comment=None):
    """Write cfg configuration object to YAML file.
    """
    with open(file_name, "w") as fp:
        if comment is not None:
            fp.write("# {}\n".format(comment))
        yaml.safe_dump(cfg, fp, default_flow_style=False, indent=2)


def check(cfg):
    """Check a configuration object for misconfiguration."""
    # Enforce that user mode only uses loop_awake/loop_sleep,
    # whereas superuser mode may use both, including loop_epoch.
    if not util.superuser():
        assert(cfg['daemon']['loop_epoch'] == 0)
        assert(cfg['daemon']['loop_awake'] > 0)
    if cfg['daemon']['loop_epoch'] > 0:
        assert(86400 % int(cfg['daemon']['loop_epoch']) == 0)
        assert(cfg['daemon']['loop_epoch'] > cfg['daemon']['loop_awake'])


def extend(cfg):
    """Extend configuration settings with some important variables."""
    cfg["hostname"] = socket.gethostname()
    cfg["hpcmd_version"] = version.get_printable_version_string()
    cfg["output"]["local_user"] = getpass.getuser()
    cfg["intro_logged"] = False
    cfg["epilog_cleaned"] = False



def environment(cfg):
    """Set/override configuration settings from environment variables."""
    if "HPCMD_DEBUG" in os.environ:
        cfg['daemon']['debug'] = pr.ntype(os.environ["HPCMD_DEBUG"])
    if "HPCMD_AWAKE" in os.environ:
        cfg['daemon']['loop_awake'] = pr.ntype(os.environ["HPCMD_AWAKE"])
    if "HPCMD_SLEEP" in os.environ:
        cfg['daemon']['loop_sleep'] = pr.ntype(os.environ["HPCMD_SLEEP"])
    if "HPCMD_PREFIX" in os.environ:
        cfg['output']['prefix'] = pr.ntype(os.environ["HPCMD_PREFIX"])


def sanitize(cfg):
    """Fix a configuration object. For transitions during implementation/debugging."""
    if "working_directory" not in cfg['daemon']:
        cfg['daemon']['working_directory'] = util.get_default_working_directory()
    cfg['daemon']['pid_file'] = util.get_default_pid_file_name()
    cfg['daemon']['suspend_file'] = os.path.join(cfg['daemon']['trigger_directory'], "suspend")
    cfg['daemon']['suspend_confirmation_file'] = os.path.join(cfg['daemon']['trigger_directory'], "service_suspended")
    cfg['daemon']['suspend_poll_time'] = 2.5
    # Prefix the output files (by default empty)
    if cfg['output']['out_file'] != 'stdout':
        util.prefix(cfg['output']['out_file'], cfg['output']['prefix'])
    if cfg['output']['err_file'] != 'stderr':
        util.prefix(cfg['output']['err_file'], cfg['output']['prefix'])
    util.prefix(cfg['output']['epilog_file'], cfg['output']['prefix'])
    util.prefix(cfg['output']['cfg_out'], cfg['output']['prefix'])
    # --- Adapt sampling intervals,
    #     CI jobs may use shorter sampling intervals, anything else may not:
    if 'GITLAB_CI' not in os.environ:
        # --- catch too high sampling frequencies ---
        # (1) perf shall run for at least some seconds
        if cfg['daemon']['loop_awake'] < 25.0:
            cfg['daemon']['loop_awake'] = 25.0
            print("hpcmd: HPCMD_AWAKE increased to {}".format(cfg['daemon']['loop_awake']))
        # (2) sleep at least some seconds, if in user mode
        if cfg['daemon']['loop_epoch'] == 0 and cfg['daemon']['loop_sleep'] < 5.0:
            cfg['daemon']['loop_sleep'] = 5.0
            print("hpcmd: HPCMD_SLEEP increased to {}".format(cfg['daemon']['loop_sleep']))
        # (3) an epoch should be at least some seconds longer than (1)
        #     to give the other measurements and the logging enough time.
        if cfg['daemon']['loop_epoch'] > 0 and cfg['daemon']['loop_epoch'] < 30.0:
            cfg['daemon']['loop_epoch'] = 30.0
            print("hpcmd: HPCMD_EPOCH increased to {}".format(cfg['daemon']['loop_epoch']))
    # While (loop_epoch > 0) causes hpcmd to work in epoch mode, we cannot use this variable
    # when we work in user mode (with alive and sleep) for computations involving the length
    # of the measurement interval.  Therefore, we introduce loop_dt newly as follows:
    if cfg['daemon']['loop_epoch'] > 0:
        cfg['daemon']['loop_dt'] = cfg['daemon']['loop_epoch']
    else:
        cfg['daemon']['loop_dt'] = cfg['daemon']['loop_awake'] + cfg['daemon']['loop_sleep']
    # TEMPORARY HACK: adjust some tools for specific MPCDF needs
    patch_mpcdf(cfg)


def patch_mpcdf(cfg):
    """Minor adjustments specific to MPCDF clusters."""
    if cfg["cluster"] == "draco":
        if cfg["hostname"] in util.expand_nodelist("dra[0833-0896],dra[0913-0962]"):
            # everything is fine, we're on a broadwell which supports flop counters
            pass
        else:
            # need to disable certain counters because they are bogus on haswell
            cfg['perf']['fp']['enable'] = False
            for key in ["ALGO-INT", "FP-SCALAR", "FP-VECTOR", "GFLOPS"]:
                cfg['perf']['derived']['metrics'][key] = False
    elif cfg["cluster"] == "tok":
        if cfg["hostname"] in util.expand_nodelist("tok[001-044]"):
            # these tok nodes have Ethernet and no OmniPath
            cfg['network']['opa']['enable'] = False
            cfg['network']['ip']['enable'] = True
        else:
            # other tok nodes have OmniPath
            cfg['network']['opa']['enable'] = True
            cfg['network']['ip']['enable'] = False


def setup(arg):
    """Create configuration object used throughout the code, based
    on the default and the cluster-specific configuration files.
    """
    # read default configuration
    def_cfg_name = get_default_file_name()
    def_cfg = read(def_cfg_name)
    def_cfg["cluster"] = get_cluster_name(def_cfg)
    # read, update, and clean configuration
    cfg_file_name = get_config_file_name(arg, def_cfg)
    cfg = read(cfg_file_name)
    if def_cfg["base"]["enable"] != cfg["base"]["enable"]:
        def_cfg = util.activate(def_cfg, cfg["base"]["enable"])
    if def_cfg["base"]["print_samples"] != cfg["base"]["print_samples"]:
        def_cfg = util.activate(def_cfg, cfg["base"]["print_samples"])
    if def_cfg["base"]["print_summary"] != cfg["base"]["print_summary"]:
        def_cfg = util.activate(def_cfg, cfg["base"]["print_summary"])
    cfg = util.merge_dicts(def_cfg, cfg)
    return cfg
