# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module responsible for querying memory related counters.
"""

from collections import OrderedDict

from .tool import Tool
from . import util
from . import log
from . import primitives as pr


class Memory(Tool):
    def __init__(self, c):
        """Construct memory."""
        super(Memory, self).__init__(c, "memory")

    def execute(self, cfg):
        """Run the numastat command based on the configuration passed via cfg.
        To allow multiple sources and make code simpler, parsing is done directly
        after the command execution, which is a bit different then in other tools
        that use 'parse' function.
        """
        rawdata = []
        # Data is a dictionary of dictionaries: each nodeid has its own dictionary
        outdata = OrderedDict()
        if cfg[self.ttag]['generic']['enable']:
            # Get information about memory stats
            command = "numastat -m"
            # log.debug(command)
            raw, exit_status = util.run(command)
            if exit_status == 0:
                out = util.get_ascii_lines(raw)
                delindex = [idx for idx, s in enumerate(out) if '----' in s][0]
                rawdata = rawdata + out[delindex + 1:]
                # log.debug(rawdata)
        if cfg[self.ttag]['numa']['enable']:
            # Get information about NUMA stats
            command = "numastat -s"
            # log.debug(command)
            raw, exit_status = util.run(command)
            if exit_status == 0:
                out = util.get_ascii_lines(raw)
                delindex = [idx for idx, s in enumerate(out) if '----' in s][0]
                rawdata = rawdata + out[delindex + 1:]
                # log.debug(rawdata)
        if rawdata:
            outdata = self._parse_numastat_output(outdata, rawdata)
        if cfg[self.ttag]['rss']['enable']:
            # Get information about memory rss
            user = pr.get_user(cfg)
            if user is not None:
                command = "ps -U {} -u {} -o rss".format(user, user)
                # log.debug(command)
                raw, exit_status = util.run(command)
                if exit_status == 0:
                    out = util.get_ascii_lines(raw, skip_n=1)
                    rawdata = list(map(int, out))
                    # log.debug(rawdata)
                    self._parse_rss_output(outdata, rawdata)
        return outdata

    def update_summary(self, cfg, outdata):
        """Update summary file with the peak RSS memory consumption."""
        if ('S0' in outdata) and ('NodeRSS' in outdata['S0']):
            util.update_stat_max(cfg, 'MEM_PEAK', outdata['S0']['NodeRSS'])

    def print_sample(self, cfg, outdata):
        """Printing data collected with numastat tool, one line per NUMA node."""
        for numanodeid in outdata:
            msg = []
            if bool(numanodeid):
                msg.append(pr.keqv("numaid", numanodeid))
            for key in outdata[numanodeid]:
                msg.append(pr.keqdv(key, outdata[numanodeid]))
            log.prefixed_message(' '.join(msg), cfg, self.ttag)

########################################
# Local helper functions

    def _parse_numastat_output(self, outdata, rawdata):
        """Obtain the names and numbers from numastat output lines and return them in a dictionary."""
        n_numa_nodes = int(len(rawdata[0].split())) - 2
        for i in range(0, n_numa_nodes):
            outdata[str("S" + str(i))] = OrderedDict()
        rest_counters = len(self.id_list)
        for line in rawdata:
            line = line.split()
            key = line[0]
            if key in self.id_list:
                for i in range(0, n_numa_nodes):
                    outdata[str("S" + str(i))][key] = float(line[i + 1])
                rest_counters -= 1
                if (rest_counters == 0):
                    log.debug("{} _parse_numastat_output: finished with all counters".format(self.logname))
                    break
        return outdata

    def _parse_rss_output(self, outdata, rawdata):
        """Obtain the total RSS in MB and return it in existing dictionary for S0 socket."""
        if "S0" not in outdata:
            outdata["S0"] = OrderedDict()
        outdata["S0"]["NodeRSS"] = sum(rawdata) / 1024
        log.debug(outdata["S0"]["NodeRSS"])
