# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module responsible for querying software information about the
executable name and occupied/empty cores.
"""

from collections import OrderedDict
import os

from .tool import Tool
from . import primitives as pr
from . import util
from . import log


class Software(Tool):
    def __init__(self, c):
        """Construct software."""
        super(Software, self).__init__(c, "software")

    def specific_setup(self, cfg):
        """Set up the total number of cores based on the configuration passed via cfg."""
        if cfg[self.ttag]['threads']['enable']:
            self.sanity_check(cfg[self.ttag]['threads'])
        if cfg["slurm"]["enable"] and isinstance(cfg["slurm"]["node"]["sockets"], int) and isinstance(cfg["slurm"]["node"]["corespersocket"], int):
            log.debug("using cpu core specification from slurm.conf")
            self.num_groups = cfg["slurm"]["node"]["sockets"]
            self.num_cores = self.num_groups * cfg["slurm"]["node"]["corespersocket"]
        else:
            log.message("using cpu core specification from hpcmd config file", cfg)
            self.num_groups = int(cfg[self.ttag]['threads']['sockets'])
            self.num_cores = int(cfg[self.ttag]['threads']['cores'])

        # E.g. on Cascade Lake we have sub-socket NUMA domains which SLURM sees as sockets. We combine them per physical socket.
        self.num_groups = self.num_groups // int(cfg[self.ttag]['threads']['combine_sockets'])

        if self.num_cores % self.num_groups:
            raise RuntimeError("Number of cores must be divisible by number of groups! Please check the configuration.")
        self.load_file = cfg[self.ttag]['loadavg']['load_file']
        self.default_paths = tuple(cfg[self.ttag]['libs']['default_paths'])

    def specific_intro(self, imsg):
        """Add 'prolog-like' info about the cores and sockets."""
        imsg.append(pr.keqv("cores", self.num_cores))
        imsg.append(pr.keqv("sockets", self.num_groups))

    def execute(self, cfg):
        """Run the ps command to obtain thread stats based on the configuration
        passed via cfg and return the thread data in a dictionary."""
        # Data is a dictionary of dictionaries
        outdata = {}
        user = pr.get_user(cfg)
        # Information about the executable name
        if cfg[self.ttag]['executable']['enable']:
            # Get information on the most expensive process on a node
            ps_user, ps_pid, ps_executable = self._get_ps_exe_information(user)
            if (ps_user is not None) and (ps_user != "root") and (ps_executable is not None):
                outdata["exe"] = ps_executable
                if cfg[self.ttag]['executable']['environ']['enable'] and pr.headnode(cfg) and cfg["helper_stats"]["log_environ"]:
                    ps_environ = util.get_env(ps_pid)
                    log.debug(ps_environ)
                    whitelist = cfg[self.ttag]['executable']['environ']['whitelist']
                    outdata["environ"] = util.filter_dictionary(ps_environ, whitelist)
                    log.debug(outdata["environ"])
        # Information about user threads
        if cfg[self.ttag]['threads']['enable']:
            # Get information about thread stats
            # Alternative could be to use "mpstat -P ALL 1 1"
            command = "ps -U " + user + " -u " + user + " -L -opsr"
            log.debug(command)
            raw, exit_status = util.run(command)
            if exit_status == 0:
                out = util.get_ascii_lines(raw, skip_n=1)
                rawdata = list(map(int, out))
                # log.debug(rawdata)
                self._parse_thread_output(outdata, rawdata)
        if cfg[self.ttag]['loadavg']['enable'] and cfg['stats']['iter'] > 1:
            # Get information about load from /proc/loadavg
            rawdata = util.get_lines_from_file(self.load_file)[0]
            log.debug(rawdata)
            self._parse_loadavg_output(outdata, rawdata)
        # Information about (Python) libraries
        pids = []
        if (cfg[self.ttag]['libs']['enable'] and pr.headnode(cfg)) or (cfg[self.ttag]['pylibs']['enable'] and pr.headnode(cfg)):
            pids = util.get_user_processes(user)
            log.debug(pids)
        if cfg[self.ttag]['libs']['enable'] and pr.headnode(cfg):
            self._read_and_parse_libs(outdata, pids)
        if cfg[self.ttag]['pylibs']['enable'] and pr.headnode(cfg):
            outdata["pylibs"] = util.get_all_pylibs(pids)
            log.debug(outdata["pylibs"])
        return outdata

    def update_summary(self, cfg, outdata):
        """Update summary file with the software stats."""
        # write the name of the executable (if available)
        if cfg[self.ttag]['executable']['enable'] and "exe" in outdata:
            util.update_stat_mode(cfg, 'exe', 'exe_list', outdata["exe"])
        if cfg[self.ttag]['loadavg']['enable'] and cfg['stats']['iter'] > 1:
            if "load_5min" in outdata['loadavg']:
                util.update_stat_avg(cfg, 'AVG_LOAD', outdata['loadavg']['load_5min'])
            peak = 0
            for key in outdata['loadavg']:
                if outdata['loadavg'][key] > peak:
                    peak = outdata['loadavg'][key]
            util.update_stat_max(cfg, 'PEAK_LOAD', peak)
        if cfg[self.ttag]['threads']['enable'] and "threads" in outdata:
            # write the number of empty cores per node
            ec = 0
            for cpuid in outdata['threads']:
                if "empty_cores" in outdata['threads'][cpuid]:
                    ec += outdata['threads'][cpuid]["empty_cores"]
            util.update_stat_min(cfg, 'min_empty_cores', ec)
            cfg["stats"]['cores'] = self.num_cores
        # Updating the list of new libraries
        if cfg[self.ttag]['libs']['enable'] and pr.headnode(cfg):
            outdata['libs'] = util.update_unique_set(cfg, 'old_libs', outdata['libs'])
        if cfg[self.ttag]['pylibs']['enable'] and pr.headnode(cfg):
            outdata['pylibs'] = util.update_unique_set(cfg, 'old_pylibs', outdata['pylibs'])

    def print_sample(self, cfg, outdata):
        """Printing data collected for software."""
        msg = []
        # Find executable name run by the job
        if cfg[self.ttag]['executable']['enable'] and "exe" in outdata:
            msg.append(pr.keqdv("exe", outdata))
        # Find load
        if cfg[self.ttag]['loadavg']['enable'] and cfg['stats']['iter'] > 1:
            for key in outdata['loadavg']:
                msg.append(pr.keqdv(key, outdata['loadavg']))
        # Print executable name and load
        if len(msg) > 0:
            log.prefixed_message(' '.join(msg), cfg, "exe")
        # Printing data collected for threads, one line per core/socket
        if cfg[self.ttag]['threads']['enable'] and "threads" in outdata:
            for cpuid in outdata['threads']:
                msg = []
                if bool(cpuid):
                    msg.append(pr.keqv("cpu", cpuid))
                for key in outdata['threads'][cpuid]:
                    msg.append(pr.keqdv(key, outdata['threads'][cpuid]))
                log.prefixed_message(' '.join(msg), cfg, "thread")
        # Printing new libraries
        if cfg[self.ttag]['libs']['enable'] and pr.headnode(cfg):
            for l in outdata['libs']:
                msg = []
                if cfg[self.ttag]['executable']['enable'] and "exe" in outdata:
                    msg.append(pr.keqdv("exe", outdata))
                msg.append(pr.keqv("path", l))
                msg.append(pr.keqv("lib", l.split('/')[-1]))
                log.prefixed_message(' '.join(msg), cfg, "libs")
        if cfg[self.ttag]['pylibs']['enable'] and pr.headnode(cfg):
            for l in outdata['pylibs']:
                msg = []
                if cfg[self.ttag]['executable']['enable'] and "exe" in outdata:
                    msg.append(pr.keqdv("exe", outdata))
                msg.append(pr.keqv("pylib", l))
                log.prefixed_message(' '.join(msg), cfg, "pylibs")
        # Print environment once
        if cfg[self.ttag]['executable']['environ']['enable'] and pr.headnode(cfg) and cfg["helper_stats"]["log_environ"] and "environ" in outdata:
            tokens = ["{}={}".format(key, val) for (key, val) in outdata["environ"].items()]
            tokens.sort()
            combined_tokens = util.combine_string_tokens(tokens, max_length=980)
            for tok in combined_tokens:
                log.prefixed_message(pr.keqv("environment", tok), cfg, tag="env")
            cfg["helper_stats"]["log_environ"] = False


########################################
# Local helper functions

    def _get_ps_exe_information(self, ps_exe_user):
        """Return (user, pid, executable_name) from the first line of the sorted ps command.

        Remark: This ps based solution turns out to be 4x faster than top based solution.
        """
        user = None
        pid = None
        executable = None
        try:
            cmd = "ps -u {} --cols 256 -o pid,cmd --sort -cputime".format(ps_exe_user)
            raw, _exit_status = util.run(cmd)
            out = util.get_ascii_lines(raw)
            log.debug(out)
            # It may be that there is no further line,
            # in case top_user does not run any process.
            # This is handled gracefully via the try...except block.
            line_items = out[1].split()
            log.debug(line_items)
            _pid = os.path.basename(line_items[0])
            _executable = os.path.basename(line_items[1])
            # Assign only if no exception has occurred.
            pid = int(_pid)
            user = ps_exe_user
            executable = _executable
        except Exception:
            pass
        log.debug(str(user) + "," + str(executable))
        return (user, pid, executable)

    def _parse_thread_output(self, outdata, rawdata):
        """Obtain the stats for threads and return them in a dictionary."""
        outdata['threads'] = OrderedDict()
        # Increment number of threads per core for each thread
        core_histogram = [[0 for i in range(int(self.num_cores / self.num_groups))] for j in range(self.num_groups)]
        c = 0
        for t in rawdata:
            if (t < 0) or (t >= 2 * self.num_cores):
                # Error, but for now ignore it, not to crash whole script
                log.debug("Core is out of scope, ignoring.")
                continue
            elif (t >= self.num_cores):
                # In case of hyperthreading (2 threads per core)
                g = int(pr.try_div(t - self.num_cores, self.num_cores / self.num_groups))
                c = int((t - self.num_cores) - (self.num_cores / self.num_groups) * g)
            else:
                # Regular case
                g = int(pr.try_div(t, self.num_cores / self.num_groups))
                c = int(t - (self.num_cores / self.num_groups) * g)
            core_histogram[g][c] += 1
        log.debug(core_histogram)
        cpuid = ""
        for g in range(self.num_groups):
            cpuid = "S" + str(g)
            outdata['threads'][cpuid] = {}
            total_threads = 0
            empty_cores = 0
            for c in range(len(core_histogram[g])):
                if core_histogram[g][c] > 0:
                    total_threads += core_histogram[g][c]
                else:
                    empty_cores += 1
            outdata['threads'][cpuid]["total_threads"] = total_threads
            outdata['threads'][cpuid]["empty_cores"] = empty_cores
            msg = "Total number of threads: {}; Empty cores: {}".format(
                outdata['threads'][cpuid]["total_threads"], outdata['threads'][cpuid]["empty_cores"])
            log.debug(msg)

    def _parse_loadavg_output(self, outdata, rawdata):
        """Obtain load values from loadavg file."""
        elements = rawdata.split()
        outdata['loadavg'] = {}
        for idx, key in enumerate(self.id_list):
            log.debug(key)
            outdata['loadavg'][key] = float(elements[idx])

    def _read_and_parse_libs(self, outdata, pids):
        """Read and parse 'maps' file for each user process."""
        outdata['libs'] = set()
        for pid in pids:
            # log.debug(pid)
            try:
                with open("/proc/" + str(pid) + "/maps", 'r') as fp:
                    for line in fp:
                        l = line.split()
                        # log.debug(l)
                        if l[4] == '0':
                            continue
                        if not l[5].startswith(self.default_paths):
                            # log.debug(l[5])
                            outdata['libs'].add(l[5])
            except Exception:
                # It is not a big deal if a process meanwhile disappeared, just ignore it
                pass
