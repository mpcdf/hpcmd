# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""hpcmd's collection of primitive operations.

Note: To avoid cyclic dependencies, no import of other hpcmd modules is
allowed here!
"""

import os


def flush(stream, fsync=False):
    """Flush stream, optionally force sync to filesystem.
    """
    stream.flush()
    if fsync:
        os.fsync(stream.fileno())


def try_div(x, y):
    """Division with protection for division by zero.
    """
    try:
        return x / y
    except ZeroDivisionError:
        return y


def keqv(key, val):
    """Return a string of the form 'key="val"' where val is quoted.
    """
    key = key.replace(".", "_")
    return '{}="{}"'.format(key, val)


def keqdv(key, d):
    """Return a string of the form 'key="d[key]"' where d[key] is quoted.
    """
    return keqv(key, d[key])


def rm(file):
    """Remove a file.  If the file does not exist, no error is raised."""
    try:
        os.remove(file)
    except OSError:
        pass


def has_job_info(cfg):
    """Return if SLURM is enabled and job info is available.
    """
    return cfg['slurm']['enable'] and ("job" in cfg["slurm"])


def del_job_info(cfg):
    """Delete job information, if present.
    """
    if has_job_info(cfg):
        del cfg["slurm"]["job"]


def get_user(cfg):
    """Return the relevant user name, in case of a SLURM job it is job owner.
    """
    if has_job_info(cfg):
        try:
            user = cfg["slurm"]["job"]["job_user"]
        except Exception:
            user = None
    else:
        user = cfg["output"]["local_user"]
    return user


def headnode(cfg):
    """Return True if a SLURM job is active and we're on the headnode of that job.
    For non-SLURM mode, always return True.
    """
    return (not cfg['slurm']['enable']) or (has_job_info(cfg) and cfg["slurm"]["job"]["headnode"])


def is_multijob(cfg):
    """Return if multiple jobs are running in parallel should be traced
    """
    return cfg['output']['local_slurm_jobs_tag']


def get_multijobs(default_dir="/var/spool/slurmd/"):
    """Return single string containing all jobids of multiple jobs running in parallel
    """
    jobids = []
    subdirs = get_immediate_subdirectories(default_dir)
    for s in subdirs:
        if "job" in s:
            jobids.append(s.strip("job"))
    return ','.join(jobids)


def create_trigger_file(file_name):
    """Create a trigger file with completely open permissions."""
    if not os.path.isfile(file_name):
        directory = os.path.dirname(file_name)
        old_mask = os.umask(0o000)
        if not os.path.isdir(directory):
            os.makedirs(directory)
        with open(file_name, 'w'):
            pass
        os.umask(old_mask)


def create_epilog(cfg):
    """Create trigger file for the epilog script.
    """
    create_trigger_file(cfg['output']['epilog_file'])


def remove_epilog(cfg):
    """Remove trigger file for the epilog script.
    """
    rm(cfg['output']['epilog_file'])


def ntype(s):
    """Convert a string 's' to its native Python datatype, if possible.
    """
    true_strings = ["true", "t", "yes", "y"]
    false_strings = ["false", "f", "no", "n"]

    def is_bool(s):
        return s.lower() in true_strings + false_strings

    def is_true(s):
        return s.lower() in true_strings

    try:
        r = int(s)
    except Exception:
        try:
            r = float(s)
        except Exception:
            if is_bool(s):
                return is_true(s)
            else:
                return s
        else:
            return r
    else:
        return r


def mode(l):
    """Mode function (implemented to avoid import of the 'statistics' module)."""
    return max(set(l), key=l.count)


def get_immediate_subdirectories(a_dir):
    """Get immediate subdirectories."""
    return [name for name in os.listdir(a_dir) if os.path.isdir(os.path.join(a_dir, name))]
