# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""hpcmd's collection of utility functions.

Notes
-----
* To avoid cyclic dependencies, imports of other hpcmd
  modules should be kept as rare as possible.
* Small functions not requiring imports should be moved
  into hpcmd.primitives.
"""


import os
import sys
import pwd
import subprocess
import multiprocessing
import signal
import time
from datetime import datetime, timedelta
import argparse
import re
import shlex

from . import log
from . import primitives as pr

# string constants
env_exe = "/usr/bin/env"


def split(string):
    """Split a string but preserve quoted substrings,
    keeping the default behaviour for anything else.
    """
    if ("'" in string) or ('"' in string):
        # assume that the string is quoted
        string_split = shlex.split(string)
    else:
        string_split = string.split()
    return string_split


def popen(command, environment={}):
    """Execute command as a subprocess and return the handle.
    """
    environment_seq = [key + "=" + val for key, val in environment.items()]
    command_seq = [env_exe] + environment_seq + split(command)
    p = subprocess.Popen(command_seq, shell=False, stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT, universal_newlines=True,
                         close_fds=True)
    return p


def subprocess_has_ended(subprocess_handle, timeout=5.0, poll_interval=0.1):
    """Poll and return if a subprocess has ended within the timeout.
    """
    t = 0.0
    q = False
    while True:
        if t >= timeout:
            break
        q = subprocess_handle.poll() is None
        if q:
            break
        else:
            time.sleep(poll_interval)
            t += poll_interval
    return q


def send_shutdown_signal_cascade(subprocess_handle, send_sigterm=False, send_sigkill=False):
    """Send the signals SIGINT, SIGTERM, SIGKILL to a subprocess.
    """
    subprocess_handle.send_signal(signal.SIGINT)
    if not subprocess_has_ended(subprocess_handle) and send_sigterm:
        subprocess_handle.send_signal(signal.SIGTERM)
        if not subprocess_has_ended(subprocess_handle) and send_sigkill:
            subprocess_handle.send_signal(signal.SIGKILL)


def join_and_get_output(subprocess_handle, signal_to_child=False, send_sigterm=False, send_sigkill=False):
    """Join a subprocess previously spawned via popen, and return the output.
    """
    if signal_to_child:
        child_pids = get_child_pids(subprocess_handle.pid)
        for p in child_pids:
            try:
                os.kill(p, signal.SIGINT)
            except:
                pass
    else:
        send_shutdown_signal_cascade(subprocess_handle, send_sigterm, send_sigkill)
    subprocess_handle.wait()
    out, _err = subprocess_handle.communicate()
    return out


def run(command, environment={}):
    """Execute command as a subprocess.

    Return a tuple containing the unified output (stdout,stderr) as raw string,
    and as the second element the exit status.
    """
    p = popen(command, environment)
    # note that err is empty
    out, _err = p.communicate()
    p.wait()
    # return stdout.decode('utf-8', 'ignore'), p.returncode
    return out, p.returncode


def get_ascii_lines(raw, skip_n=0):
    """Split a raw string into a list of stripped lines, only retaining ASCII characters.

    Returns the list of lines in ASCII encoding.
    """
    lines = [line.strip().encode('ascii', errors='ignore').decode() for line in raw.splitlines()]
    return lines[skip_n:]


def unbind():
    """Disable any process binding.

    The present implementation is based on the tool `taskset`.

    Future (Python 3 only) implementations could use os.sched_* routines instead.
    """
    try:
        core_list = "0-{}".format(multiprocessing.cpu_count() - 1)
        cmd = "taskset -c -p {} {}".format(core_list, os.getpid())
        stdout, _exit_code = run(cmd)
        log.debug("".join(stdout))
    except Exception:
        pass


def get_hpcmd_pid(cfg):
    """Return the PID of a running hpcmd daemon.  No exception is caught."""
    with open(cfg['daemon']['pid_file'], 'r') as fp:
        lines = tuple(fp)
        pid = int(lines[0])
        return pid


def get_child_pids(pid):
    """Return the process ids of the children of the process with 'pid', obtained
    from the Linux proc file system.  Works well to obtain the process id of
    the sleep command when invoking 'perf ... sleep'.  Other use cases may fail,
    especially when the spawning process uses multiple threads!
    """
    child_pids = []
    proc_file = "/proc/{}/task/{}/children".format(pid, pid)
    # In rare cases it may be that the process is already gone,
    # so we need to protect the read-in using a try...except.
    try:
        _pids = []
        with open(proc_file, "r") as fp:
            for line in fp:
                child = int(line.rstrip())
                _pids.append(child)
        child_pids = _pids
    except Exception:
        pass
    return child_pids


def get_environment(pid):
    """Read the complete environment for a process ID directly from the Linux
    /proc file system.

    For simplicity, this function currently uses the 'sed' tool to
    read the file.

    TODO: Implement the reading in Python.
    """
    env = {}
    try:
        cmd = "sed -e s/\\x0/\\n/g /proc/{}/environ".format(pid)
        raw, _exit_status = run(cmd)
        out = get_ascii_lines(raw)
        for line in out:
            if "=" in line:
                key, val = line.split("=", 1)
                env[key] = val
    except Exception as e:
        log.exception(e)
        return {}
    else:
        return env

def get_env(pid):
    envs_path = f"/proc/{pid}/environ"
    env = {}

    try:
        with open(envs_path, 'rb') as f:
            content = f.read().replace(b'\x00', b'\n').decode('utf-8')

        lines = content.splitlines()

        for line in lines:
            if "=" in line:
                key, val = line.split("=", 1)
                env[key] = val
    except Exception as e:
        log.exception(e)
        return {}
    else:
        return env


def get_pylibs(pid):
    """Read the Python (.so) libraries for a process ID directly from the Linux /proc file system.
    """
    maps_path = f"/proc/{pid}/maps"
    libraries = set()

    try:
        with open(maps_path, 'r') as f:
            lines = f.readlines()

        for line in lines:
            match = re.search(r'site-packages/([^/]+)', line)
            if match:
                libraries.add(match.group(1))
            match = re.search(r'lib-dynload/([^/]+)', line)
            if match:
                libraries.add(match.group(1).split('.')[0])
            match = re.search(r'lib/python\d+\.\d+/[^/]+/([^/]+)', line)
            if match:
                libraries.add(match.group(1).split('.')[0])
    except Exception as e:
        # some maps files may be inaccessible, so we ignore any errors here
        return []
    else:
        return list(libraries)


def get_all_pylibs(pids):
    """Return a list of all unique Python libraries across all specified PIDs.
    """
    pid_library = set()

    for pid in pids:
        pid_library.update(get_pylibs(pid))

    return list(pid_library)


def filter_dictionary(dict_in, whitelist):
    """Scan a dictionary for certain keys contained in whitelist, and return a new dictionary containing only those key-value pairs.
    """
    dict_out = {}
    for key in dict_in.keys():
        if key.startswith(tuple(whitelist)):
            dict_out[key] = dict_in[key]
    return dict_out


def get_process_ids(exe):
    """Return the process ids of an executable in a list, implemented simply by
    wrapping the Linux tool `pgrep`.
    """
    pids = []
    try:
        cmd = "pgrep {}".format(exe)
        raw, exit_status = run(cmd)
        if exit_status == 0:
            out = get_ascii_lines(raw)
            pids = list(map(int, out))
    except Exception as e:
        log.exception(e)
        return []
    else:
        return pids


def get_user_processes(user):
    """Return the process ids of a user in a list, implemented simply by
    wrapping the Linux tool `pgrep`.
    """
    pids = []
    try:
        command = "ps -U {} -u {} -o pid".format(user, user)
        raw, exit_status = run(command)
        if exit_status == 0:
            out = get_ascii_lines(raw, skip_n=1)
            pids = list(map(int, out))
    except Exception as e:
        log.exception(e)
        return []
    else:
        return pids


def get_sec(time_str):
    """Convert a string of the form H:M:S to seconds.
    """
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)


def get_process_footprint(pid):
    """Return the cpu time [s] and RSS [KB] of a process as a tuple of integers.
    """
    try:
        cmd = "ps -h -p {} -o cputime,rss".format(pid,)
        raw, exit_status = run(cmd)
        if exit_status:
            raise RuntimeError("command `" + cmd + "` exited with error")
        else:
            out = get_ascii_lines(raw)
            cpu_raw, rss_raw = out[0].split()
            cpu = get_sec(cpu_raw)
            rss = int(rss_raw)
            return cpu, rss
    except Exception as e:
        log.exception(e)
        return 0, 0


def terminate(cfg):
    """Send SIGTERM to a running hpcmd daemon."""
    pid = get_hpcmd_pid(cfg)
    print("Sending SIGTERM to hpcmd process {} ...".format(pid))
    os.kill(pid, signal.SIGTERM)


def kill(cfg):
    """Send SIGKILL to a running hpcmd daemon."""
    pid = get_hpcmd_pid(cfg)
    print("Sending SIGKILL to hpcmd process {} ...".format(pid))
    os.kill(pid, signal.SIGKILL)
    pr.rm(cfg['daemon']['pid_file'])


def get_output_streams(cfg):
    """Perform redirection according to the config object and return the
    out, err streams.
    """
    if cfg['output']['out_file'] == 'stdout':
        out_stream = sys.stdout
    else:
        out_stream = open(cfg['output']['out_file'], 'a+')  # append preferred
    if cfg['output']['err_file'] == 'stderr':
        err_stream = sys.stderr
    else:
        err_stream = open(cfg['output']['err_file'], 'a+')
    return (out_stream, err_stream)


def get_sleep_time(epoch_s=300):
    """Calculate and return the time until the start of the next epoch.

    The next epoch is defined by the closest point in time in the future with
    an integral multiple of 'epoch_s' seconds, starting from midnight. As a
    side constraint the total number of seconds on a day (86400) needs to be
    divisible by 'epoch_s'.

    Use case is to use the return value as the sleep time until the
    next measurement.
    """
    # we need two datetime objects, now and last midnight
    now = datetime.now()
    day = now.replace(microsecond=0, second=0, minute=0, hour=0)
    # using arithmetic in seconds, determine the start of the next epoch
    today_s = (now - day).total_seconds()
    curr_epoch = int(today_s) // epoch_s
    next_epoch = curr_epoch + 1
    next_epoch_start = day + timedelta(seconds=next_epoch * epoch_s)
    # calculate and return the sleep time, i.e. the difference in seconds
    sleep_time_s = (next_epoch_start - now).total_seconds()
    return sleep_time_s


def get_timestamp():
    """Return a time stamp as a string."""
    return '{:%Y%m%d%H%M%S}'.format(datetime.now())


def get_suspend_file_information(suspend_file):
    """Return the ownership and age of a suspend file."""
    usr = pwd.getpwuid(os.stat(suspend_file).st_uid).pw_name
    age = time.time() - os.path.getmtime(suspend_file)
    return (usr, age)


def query_suspend_file(cfg, t_rotten=86400, confirm=True):
    """Check if a suspend file exists, delete and ignore any files older
    than 't_rotten'.
    """
    q = False
    suspend_file = cfg['daemon']['suspend_file']
    # check if the suspend file is present and take action accordingly
    if os.path.isfile(suspend_file):
        try:
            _usr, age = get_suspend_file_information(suspend_file)
        except Exception:
            pass
        else:
            if age > t_rotten:
                pr.rm(suspend_file)
            else:
                q = True
    else:
        pass
    # signal begin (end) of suspend to the user via file creation (deletion)
    if confirm:
        if q:
            pr.create_trigger_file(cfg['daemon']['suspend_confirmation_file'])
        else:
            pr.rm(cfg['daemon']['suspend_confirmation_file'])
    return q


def wait_for_suspend_confirmation__disabled(cfg, n_max=25):
    """When the systemd service is running on the node, return True if the
    suspend confirmation file exists, if it does not exist after n_max tries,
    return False.  In case the service is not running, return True immediately.

    Note: Does not work yet due to pid file handling by systemd. Moreover, the
    sleep phase of the systemd service would not be covered.
    """
    service_pid_file_name = get_default_pid_file_name(uid=0)
    if os.path.isfile(service_pid_file_name):
        i = 0
        while (i < n_max):
            if os.path.isfile(cfg['daemon']['suspend_confirmation_file']):
                return True
            else:
                time.sleep(cfg['daemon']['suspend_poll_time'])
            i += 1
        else:
            return False
    else:
        return True


def wait_for_suspend_confirmation(cfg):
    """Simple version to wait for the suspend of a running daemon, based
    on sleep().
    """
    time.sleep(2.0 * cfg['daemon']['suspend_poll_time'])
    return True


def nice(n=5):
    """Change the priority of the present process."""
    os.nice(n)


def superuser():
    """Check if the present process is running with superuser permissions."""
    return (os.geteuid() == 0)


def md(resource):
    """Create a directory (for a file), if necessary.  The behaviour highly
    depends on the resource (string) parameter.

    If resource is of the form 'string',
        nothing is done.
    If resource is of the form 'string/',
        a directory labeled 'string' is created.
    If resource is of the form 'string/foo',
        a directory labeled 'string' is created.
    If resource is of the form 'string/foo/',
        a directory structure 'string/foo' is created.
    """
    folder = os.path.dirname(resource)
    if (len(folder) == 0):
        return
    if not os.path.exists(folder):
        try:
            os.makedirs(folder)
        except OSError:
            pass


def cliarg_get():
    """Handle command line arguments, return them as argparse args object."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="specify configuration file", type=str, metavar="file_name")
    parser.add_argument("-g", "--debug", help="enable debug mode", action="store_true")
    parser.add_argument("-n", "--nosuspend", help="override suspend detection", action="store_true")
    parser.add_argument("-p", "--profile", help="run cProfile on hpcmd", action="store_true")
    parser.add_argument("-s", "--stdout", help="enable output to stdout/stderr", action="store_true")
    mutex = parser.add_argument_group('optional, mutually exclusive arguments')
    mutex_g = mutex.add_mutually_exclusive_group()
    mutex_g.add_argument("-d", "--daemon", help="fork hpcmd to background daemon", action="store_true")
    mutex_g.add_argument("-k", "--kill", help="kill background daemon", action="store_true")
    mutex_g.add_argument("-t", "--terminate", help="terminate background daemon", action="store_true")
    mutex_g.add_argument("-m", "--msg", help="write a user-defined 'message' or 'variable=value'", type=str, metavar="message")
    mutex_g.add_argument("-v", "--version", help="print version information and exit", action="store_true")
    args = parser.parse_args()
    return args


def get_default_pid_file_name(uid=os.geteuid()):
    """Return the full path to the PID file at a standard location."""
    if uid > 0:
        file_name = "/var/tmp/hpcmd_{}.pid".format(uid)
    else:
        file_name = "/var/run/hpcmd.pid"
    return file_name


def get_default_working_directory():
    """Return the full path to the working directory for hpcmd."""
    uid = os.geteuid()
    if uid > 0:
        # running as a regular user we most likely want hpcmd to run in the current working directory
        path = os.getcwd()
    else:
        # running the daemon as root we want the working directory below "/var/lib"
        path = "/var/lib/hpcmd"
    return path


def get_lines_from_file(file_name, keywords=None, comment_char=None):
    """Return the lines of file_name, optionally filtered by keywords,
    optionally stripped from comments.
    """
    try:
        with open(file_name, 'r') as fp:
            lines = fp.read().splitlines()
    except Exception:
        lines = []
    if comment_char is not None:
        new_lines = []
        for line in lines:
            prefix = line.split(comment_char)[0].strip()
            if len(prefix) > 0:
                new_lines.append(prefix)
        del lines
        lines = new_lines
    if keywords is None:
        return lines
    else:
        return [x for x in lines if keywords in x]


def merge_dicts(a, b, path=None):
    """Merging two dictionaries (recursively).
    """
    if path is None:
        path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_dicts(a[key], b[key], path + [str(key)])
            elif a[key] == b[key]:
                pass  # same leaf value
            else:
                # 'Modified %s' % '.'.join(path + [str(key)])
                a[key] = b[key]
        else:
            # 'Added %s' % '.'.join(path + [str(key)])
            a[key] = b[key]
    return a


def activate(d, value):
    """Set/override global 'enable' configuration settings for a
    dictionary 'd' (recursively).
    """
    for key in d:
        if isinstance(d[key], dict):
            activate(d[key], value)
        elif key == 'enable':
            d[key] = value
    return d


def expand_nodelist(raw):
    """Expand a string in SLURM list/range notation into a list of
    individual strings.

    E.g. raw = "dra[0609-0620,0624,0629-0631,0633-0635,0637],drag[0609-0620,0624,0629-0631,0633-0635,0637]"

    Uses sub-functions for internal modularization.

    Returns list of strings with hostnames.
    """

    def _separate_nodelist(raw):
        """Separate a string (list of nodes) in SLURM notation into individual items,
        while keeping the ranges, by replacing outer commas with spaces.

        Returns
        -------
        list of strings
        """
        sep = []
        in_range = False
        for c in raw:
            if c == '[':
                in_range = True
            elif c == ']':
                in_range = False
            elif c == ',':
                if not in_range:
                    c = ' '
            sep.append(c)
        return ''.join(sep).split()

    def _expand_range(raw):
        """Expand a numerical range into a full list, keeping the width of the numbers.

        Example input
        -------------
        [001-013,099]

        Returns
        -------
        list of strings (integer numbers padded with 0)
        """
        tokens = raw.lstrip("[").rstrip("]").replace(',', ' ').split()
        exp = []
        for item in tokens:
            if '-' in item:
                p, q = item.split('-')
                n_digits = len(p)
                fmt = "{:0" + str(n_digits) + "d}"
                for idx in range(int(p), int(q) + 1):
                    exp.append(fmt.format(idx))
            else:
                exp.append(item)
        return exp

    def _expand_node_list_item(raw):
        """
        Expand a single item from a SLURM node list into a full list of nodes.

        Example input
        -------------
        dra[001-047,049,050-051]

        Returns
        -------
        list of strings with hostnames
        """
        exp = []
        if ('[' in raw) and (']' in raw):
            tok = raw.replace('[', ' ').replace(']', ' ').split()
            n = len(tok)
            if n == 1:
                pfx = ''
                rng = _expand_range(tok[0])
                sfx = ''
            elif n == 2:
                pfx = tok[0]
                rng = _expand_range(tok[1])
                sfx = ''
            elif n == 3:
                pfx = tok[0]
                rng = _expand_range(tok[1])
                sfx = tok[2]
            else:
                raise ValueError()
            for num in rng:
                exp.append(pfx + str(num) + sfx)
        else:
            exp.append(raw)
        return exp

    # --- body of expand_nodelist() below ---

    # log.debug(raw)
    exp = []
    tokens = _separate_nodelist(raw)
    for item in tokens:
        exp += _expand_node_list_item(item)
    # log.debug(exp)
    return exp


def slurm_get_running_job_id(cfg, get_start_time=True):
    """Return the job id of the job running currently on the node,
    moreover get the start time of that job in UNIX epoch seconds.

    (Moved out of slurm.py for module management reasons.)
    """
    jobid = None
    job_start_time = None
    try:
        for d in sorted(os.listdir(cfg["slurm"]["spool_dir"]), reverse=True):
            # We expect a string e.g. of the form 'dra0415_5462825.*', where
            # 5462825 is SLURM_JOB_ID.
            if d.startswith(cfg['hostname']):
                jobid = int(d.split('.')[0].split('_')[1])
                if get_start_time:
                    job_start_time = os.path.getmtime(os.path.join(cfg["slurm"]["spool_dir"], d))
                break
    except Exception:
        pass
    return jobid, job_start_time


def slurm_get_saved_job_id(cfg):
    """Return the job id potentially saved in cfg, or return None.
    """
    try:
        job_id = cfg["slurm"]["job"]["jobid"]
    except Exception:
        job_id = None
    return job_id


def slurm_query_job_change(cfg):
    """Check if the currently running SLURM job has changed.

    Returns True if the job has changed, otherwise False.

    (Moved out of slurm.py for module management reasons.)
    """
    jobid, _job_start_time = slurm_get_running_job_id(cfg, get_start_time=False)
    if pr.has_job_info(cfg):
        # cases:
        # 1) jobid has changed from a numerical value to a new numerical value
        # 2) jobid has changed from a numerical value to None
        q = jobid != cfg["slurm"]["job"]["jobid"]
    else:
        # 3) jobid has changed from None to a numerical value
        # 4) jobid stays None
        q = jobid is not None
    return q


def slurm_query_job_end(cfg):
    """Check if the current SLURM job has ended

    Returns True if the job has ended, otherwise False.

    (Moved out of slurm.py for module management reasons.)
    """
    jobid, _job_start_time = slurm_get_running_job_id(cfg)
    if pr.has_job_info(cfg):
        # check if jobid has changed from a numerical value to None
        q = jobid is None
    else:
        q = False
    return q


def wait_for_file(file_name, poll_time=60.0, max_iter=0, cfg=None):
    """Wait until the file specified by file_name exists, and return True.

    The file is checked for every poll_time seconds.

    The parameter max_iter can be used to specify the maximum number of
    iterations.  In case this number is hit, False is returned.

    In case a cfg object is passed, a log line is written at each poll.
    """
    i = 0
    while (not os.path.isfile(file_name)):
        if (max_iter > 0) and (i >= max_iter):
            return False
        if cfg is not None:
            msg = "polling for {}".format(file_name)
            log.message(msg, cfg)
        time.sleep(poll_time)
        i += 1
    else:
        return True


def update_stat_max(cfg, name, new_value):
    """Initialize or update with a max value one stat."""
    it = cfg['stats']['iter']
    if (it == 1) or (name not in cfg['stats']):
        cfg['stats'][name] = new_value
    else:
        cfg['stats'][name] = max(cfg['stats'][name], new_value)


def update_stat_min(cfg, name, new_value):
    """Initialize or update with a min value one stat."""
    it = cfg['stats']['iter']
    if (it == 1) or (name not in cfg['stats']):
        cfg['stats'][name] = new_value
    else:
        cfg['stats'][name] = min(cfg['stats'][name], new_value)


def update_stat_avg(cfg, name, new_value):
    """Initialize or update with an avg value one stat."""
    it = cfg['stats']['iter']
    if (it == 1) or (name not in cfg['stats']):
        cfg['stats'][name] = new_value
    else:
        cfg['stats'][name] = pr.try_div(cfg['stats'][name] * (it - 1) + new_value, it)


def update_stat_acc(cfg, name, new_value):
    """Initialize or update with an accumulated  value one stat."""
    it = cfg['stats']['iter']
    if (it == 1) or (name not in cfg['stats']):
        cfg['stats'][name] = new_value
    else:
        cfg['stats'][name] += new_value


def update_stat_mode(cfg, name, list_name, new_value):
    """Initialize or update with a mode value one stat."""
    it = cfg['stats']['iter']
    if list_name not in cfg['helper_stats']:
        cfg['helper_stats'][list_name] = []
    if (it == 1) or (name not in cfg['stats']):
        cfg['stats'][name] = new_value
    else:
        cfg['helper_stats'][list_name].append(new_value)
        cfg['stats'][name] = pr.mode(cfg['helper_stats'][list_name])


def update_unique_set(cfg, old_list, new_values):
    """Initialize or update unique set of values."""
    if old_list not in cfg['helper_stats']:
        cfg['helper_stats'][old_list] = set()
    new_unique_values = set()
    for l in new_values:
        if l not in cfg['helper_stats'][old_list]:
            cfg['helper_stats'][old_list].add(l)
            new_unique_values.add(l)
    return new_unique_values


def intro(msg, cfg):
    """Add 'prolog-like' general info."""
    if cfg['output']['userid']:
        msg.append(pr.keqv("userid", pr.get_user(cfg)))
    if not superuser():
        msg.append(pr.keqv("opmode", "user"))
    else:
        msg.append(pr.keqv("opmode", "systemd"))
    msg.append(pr.keqv("epoch", cfg["daemon"]["loop_dt"]))
    if cfg["daemon"]["loop_awake"]:
        msg.append(pr.keqv("awake", cfg["daemon"]["loop_awake"]))


def reset_stats_counters(cfg):
    """Reset stats counters for the current job or execution to zero.
    """
    cfg["helper_stats"] = {}
    cfg["helper_stats"]["log_environ"] = True
    cfg["stats"] = {}
    cfg["stats"]["iter"] = 0
    if cfg["slurm"]["enable"] and "job" in cfg["slurm"] and "job_num_nodes" in cfg["slurm"]["job"]:
        cfg["stats"]['nnodes'] = cfg["slurm"]["job"]["job_num_nodes"]


def lock(lock_file):
    """Lock function.

    Copied from load-sensor.py and then modified.
    """
    tmp_lock_file = "{}.{}".format(lock_file, os.getpid())
    log.debug("lock: lock_file={}, tmp_lock_file={}".format(lock_file, tmp_lock_file))

    #  create temporary lock
    old_umask = os.umask(0o22)
    try:
        with open(tmp_lock_file, 'w') as fp:
            fp.write('%d\n' % os.getpid())
    except Exception:
        pr.rm(tmp_lock_file)
        raise RuntimeError("cannot create tmp_lock_file")
    finally:
        os.umask(old_umask)

    #  Make attempts to slide lock into place
    log.debug('lock: sliding temporary lock into place ...')
    for attempt in [1, 2]:
        log.debug('lock: attempt=%d' % attempt)
        try:
            os.link(tmp_lock_file, lock_file)
            log.debug('lock: succeeded into sliding temporary lock into place')
            break
        except Exception:
            log.debug('lock: failed into sliding temporary lock into place, examining existing lock')
            holding_pidstr = ""
            try:
                with open(lock_file) as fp:
                    holding_pidstr = fp.readline()
            except Exception:
                log.error('lock: failed to open and read a presumed-to-be-existing lock file')
            else:
                log.debug('lock: holding_pidstr=[%s]' % holding_pidstr)
                if not re.search('^[1-9][0-9]*\n$', holding_pidstr):
                    os.unlink(tmp_lock_file)
                    raise RuntimeError('lock: existing file contents look invalid')
                else:
                    holding_pid = int(holding_pidstr.rstrip())
                    log.debug('lock: contents look good (holding_pid=%d)' % holding_pid)
                    log.debug('lock: checking if pid %d is running ...' % holding_pid)
                    if os.path.isdir('/proc/%d' % (holding_pid)):
                        os.unlink(tmp_lock_file)
                        raise RuntimeError('lock: pid %d is running' % holding_pid)
                    else:
                        log.debug('lock: pid %d is not running; checking attempt number ...' % holding_pid)
                        if attempt == 1:
                            pr.rm(lock_file)
                        else:
                            raise RuntimeError("failed on attempt 2")
    pr.rm(tmp_lock_file)


def unlock(lock_file):
    """Unlock function.

    Copied from load-sensor.py and then modified.
    """
    log.debug('unlock: lock_file=%s' % lock_file)
    pr.rm(lock_file)


def prefix(dest, prefix):
    """Prefix a path to the file"""
    dest = os.path.join(prefix, dest)


def combine_string_tokens(tokens, max_length):
    """Combine string tokens from a list into a new list of longer strings with max_length.
    """
    combined_tokens = []
    combined_string = ""
    for tok in tokens:
        if len(tok) > max_length:
            log.debug("skip {}".format(tok))
            continue
        elif len(tok) + 1 + len(combined_string) > max_length:
            if combined_string:
                combined_tokens.append(combined_string)
            combined_string = tok
        else:
            if combined_string:
                combined_string = combined_string + " " + tok
            else:
                combined_string = tok
    if combined_string:
        combined_tokens.append(combined_string)
    return combined_tokens
