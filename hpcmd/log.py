# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""hpcmd's collection of functions for writing output messages,
measuring time, and logging of internal exceptions and errors.

Note: This module is imported in most of the other hpcmd modules.
"""

import syslog
import logging
from timeit import default_timer as timer

from . import primitives as pr
from .primitives import keqv
from .primitives import keqdv

start_time = 0
end_time = 0


def setup(cfg, err_stream):
    """Set up the logging to syslog and the verbosity of debug messages."""
    global timer_start
    global timer_stop
    global timer_restart
    global debug
    if cfg['output']['debug']:
        debug_lvl = str(cfg['output']['debug_lvl'])
    else:
        debug_lvl = logging.WARNING
        # Exploiting Python's dynamically typing feature: Redefine functions to
        # disable debug- and timing-related messages at minimal cost.
        timer_start = _timer_start_stub
        timer_stop = _timer_stop_stub
        timer_restart = _timer_restart_stub
        debug = _debug_stub

    # Remove all handlers associated with the root logger object.
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(stream=err_stream, level=debug_lvl,
                        format='%(levelname)s - %(asctime)s - %(message)s')


def timer_start():
    """Start timer."""
    global start_time
    start_time = timer()


def _timer_start_stub():
    """Stub function for timer_start()."""
    pass


def timer_stop(msg):
    """Stop timer and print elapsed time."""
    global end_time
    end_time = timer()
    logging.info("Time in [s] to {}: {}".format(msg, (end_time - start_time)))


def _timer_stop_stub(msg):
    """Stub function for timer_stop()."""
    pass


def timer_restart(msg):
    """Stop timer and print elapsed time. Then start the timer again."""
    timer_stop(msg)
    timer_start()


def _timer_restart_stub(msg):
    """Stub function for timer_restart()."""
    pass


def debug(msg):
    """Write debugging information, if enabled."""
    logging.debug(msg)


def _debug_stub(msg):
    """Stub function for debug()."""
    pass


def error(msg):
    """Write error information, always enabled."""
    logging.error(msg)


def exception(e):
    """Write exception information, always enabled."""
    logging.exception(e)


def warning(msg):
    """Write warning information, always enabled."""
    logging.warning(msg)


def _create_prefix_list(cfg, tag):
    """Return a list of strings suitable to prefix a hpcmd syslog message.
    """
    pfx_tokens = []
    if tag:
        pfx_tokens.append(keqv(cfg['output']['tagid'], tag))
    if pr.has_job_info(cfg):
        pfx_tokens.append(keqv("jobid", cfg["slurm"]["job"]["printable_jobid"]))
        pfx_tokens.append(keqdv("nodeid", cfg["slurm"]["job"]))
    elif pr.is_multijob(cfg):
        pfx_tokens.append(keqv("jobid", pr.get_multijobs()))
    return pfx_tokens


def prefixed_message(msg_string, cfg, tag=''):
    """Write a prefixed message to the log."""
    msg = _create_prefix_list(cfg, tag)
    msg.append(msg_string)
    message(' '.join(msg), cfg)


def _get_string_list_joined_size(tokens):
    """Function to get the blank-joined length of a list of strings.
    """
    s = 0
    if tokens:
        s += len(tokens) - 1
        for tok in tokens:
            s += len(tok)
    return s


def prefixed_message_multiline(msg_tokens, cfg, tag='', msg_max_length=980):
    """Write a prefixed message to the log, taking care of a maximum line width.
    """
    pfx_tokens = _create_prefix_list(cfg, tag)
    msg_string = " ".join(pfx_tokens)
    pfx_length = len(msg_string)
    for tok in msg_tokens:
        if pfx_length + 1 + len(tok) > msg_max_length:
            continue
        elif len(tok) + 1 + len(msg_string) > msg_max_length:
            if msg_string:
                message(msg_string, cfg)
            msg_string = " ".join(pfx_tokens + [tok])
        else:
            msg_string = " ".join([msg_string, tok])
    if msg_string:
        message(msg_string, cfg)


def message(msg, cfg):
    """Write a string message, to syslog and/or to the standard output stream."""
    if cfg['output']['syslog']:
        syslog.syslog(msg)
    if cfg['output']['stdout']:
        print(msg)


def write_summary(cfg):
    """Update trigger file for the epilog script.
    Writing values previously collected by 'update_summary' functions.
    """
    if cfg['stats']['iter'] > 1:
        try:
            msg = []
            for key in cfg['stats']:
                msg.append(keqdv(key, cfg['stats']))
            pr.create_epilog(cfg)  # needs to be in place to safely catch job changes on the node
            with open(cfg['output']['epilog_file'], 'w') as fp:
                fp.write(" ".join(msg))
                pr.flush(fp, fsync=True)
        except Exception as e:
            exception(e)
    else:
        debug("not a headnode or first iteration")
