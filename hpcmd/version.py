# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt

"""hpcmd's version number and helper routines for versioning.

This module contains the version number of hpcmd at a single central location.
The version number is read by hpcmd, setup.py, and by Sphinx when generating
the docs, and by the OBS build script.
"""


ver = (0, 8, 0)


def get_version_string():
    """Return the full version number."""
    return '.'.join(map(str, ver))


def get_short_version_string():
    """Return the version number without the patchlevel."""
    return '.'.join(map(str, ver[:-1]))


def get_printable_version_string():
    """Return a nicely formatted version string, if possible with git hash."""
    version_string = "hpcmd " + get_version_string()
    try:
        from . import githash
    except Exception:
        pass
    else:
        version_string += " (git: " + githash.human_readable + ")"
    return version_string
