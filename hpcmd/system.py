# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module to trace the cpu time and memory usage of system processes.
"""

from collections import OrderedDict

from .tool import Tool
from . import primitives as pr
from . import util
from . import log


class System(Tool):
    def __init__(self, c):
        """Construct system."""
        super(System, self).__init__(c, "system")
        self.enabled = self.enabled and c[self.ttag]['services']['enable']

    def execute(self, cfg):
        """Perform system process monitoring.
        """
        outdata = OrderedDict()
        for key in self.id_list:
            pids = util.get_process_ids(key)
            cpu = 0
            rss = 0
            for pid in pids:
                c, r = util.get_process_footprint(pid)
                cpu += c
                rss += r
            if len(pids) > 0:
                outdata[key] = OrderedDict()
                outdata[key]["cputime"] = cpu
                outdata[key]["rss"] = rss
        return outdata

    def update_summary(self, cfg, outdata):
        """Update summary file with the system overhead stats."""
        for srv in outdata:
            for key in outdata[srv]:
                name = str("MAX_" + srv.upper() + "_" + key.upper())
                util.update_stat_max(cfg, name, outdata[srv][key])

    def print_sample(self, cfg, outdata):
        """Printing data collected for system services."""
        for srv in outdata:
            msg = []
            msg.append(pr.keqv("service", srv))
            for key in outdata[srv]:
                msg.append(pr.keqdv(key, outdata[srv]))
            log.prefixed_message(' '.join(msg), cfg, "sys")
