# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""Module responsible for querying processor-related counters using
the perf tool.  Rewritten to support the spawn() method.

Notes
-----
* Aggregated (derived) metrics such as GFLOP/s and memory bandwidth
  and several others are all computed in this module as well.
* Helper functions for perf tool are also in this module.
"""

import os
import re

from .tool import Tool
from . import util
from . import log
from . import primitives as pr

# One Billion = One Giga
Giga = 1000000000.0

# string labels known by `perf list` that are used to detect fp event capabilities automatically
fp_patterns_intel = {
    'fp_arith_inst_retired.scalar_double',
    'fp_arith_inst_retired.scalar_single',
    'fp_arith_inst_retired.128b_packed_double',
    'fp_arith_inst_retired.128b_packed_single',
    'fp_arith_inst_retired.256b_packed_double',
    'fp_arith_inst_retired.256b_packed_single',
    'fp_arith_inst_retired.512b_packed_double',
    'fp_arith_inst_retired.512b_packed_single',
}
fp_patterns_amd = {
    'fp_ret_sse_avx_ops.all',
    'fp_ops_retired_by_type.scalar_all',
    # 'fp_ops_retired_by_type.vector_all',
    'fp_ops_retired_by_width.pack_128_uops_retired',
    'fp_ops_retired_by_width.pack_256_uops_retired',
    'fp_ops_retired_by_width.pack_512_uops_retired',
    'fp_ops_retired_by_width.scalar_uops_retired',
}
fp_patterns_arm = set()
fp_patterns = fp_patterns_intel.union(fp_patterns_amd, fp_patterns_arm)

# -- standalone functions used by the Perf() class -->

def check_perf_tool():
    """Check if we can actually run perf."""
    command = "perf -h"
    _raw, exit_status = util.run(command)
    if exit_status > 0:
        raise RuntimeError("The perf userland tool is not operational.  Check perf installation and kernel support.")

def check_perf_event_paranoid():
    """Check if we are allowed to use perf counters, relevant to non-root invocation."""
    if not util.superuser():
        threshold = 0
        with open("/proc/sys/kernel/perf_event_paranoid", 'r') as fp:
            val = int(fp.readline())
        if val <= threshold:
            pass
        else:
            raise RuntimeError("You may not have permission to collect system-wide stats. " +
                                "Consider adapting /proc/sys/kernel/perf_event_paranoid or running as root.")

def get_cpu_vendorid():
    """Return the "Vendor ID" of the CPU in the system, obtained via `lscpu`.
    Return values are, for example, 'AuthenticAMD' or 'GenuineIntel', or None.
    """
    vendorid = None
    command = "lscpu"
    raw, _exit_status = util.run(command)
    rawdata = util.get_ascii_lines(raw)
    for line in rawdata:
        if line.startswith("Vendor ID:"):
            key, val = line.split(':', 1)
            vendorid = val.strip()
    return vendorid

def get_cpu_vendor():
    """Return the vendor of the CPU in the system, based on the more clunky "Vendor ID".
    Return values are, for example, 'amd' or 'intel', or None.
    """
    vendorid = get_cpu_vendorid()
    if vendorid == "AuthenticAMD":
        vendor = "amd"
    elif vendorid == "GenuineIntel":
        vendor = "intel"
    else:
        vendor = None
    return vendor

def get_perf_list():
    """List of events supported by perf."""
    command = "perf list"
    raw, _exit_status = util.run(command)
    rawdata = util.get_ascii_lines(raw)
    return rawdata

def map_counter_intel_skylake(key):
    """Mapping back perf-specific values to reasonably named flop counters.
    """
    return {
        'fp_arith_inst_retired.scalar_double': 'fp_d',
        'r5301c7': 'fp_d',
        'fp_arith_inst_retired.scalar_single': 'fp_s',
        'r5302c7': 'fp_s',
        'fp_arith_inst_retired.128b_packed_double': 'fp_128d',
        'r5304c7': 'fp_128d',
        'fp_arith_inst_retired.128b_packed_single': 'fp_128s',
        'r5308c7': 'fp_128s',
        'fp_arith_inst_retired.256b_packed_double': 'fp_256d',
        'r5310c7': 'fp_256d',
        'fp_arith_inst_retired.256b_packed_single': 'fp_256s',
        'r5320c7': 'fp_256s',
        'fp_arith_inst_retired.512b_packed_double': 'fp_512d',
        'r5340c7': 'fp_512d',
        'fp_arith_inst_retired.512b_packed_single': 'fp_512s',
        'r5380c7': 'fp_512s',
    }.get(key, key)

def map_counter_amd_zen4(key):
    """Mapping back perf-specific values to reasonably named flop counters.
    """
    return {
        # all FLOPs and scalar FLOPS
        'fp_ret_sse_avx_ops.all': 'fp_ret_all',
        'fp_ops_retired_by_type.scalar_all': 'fp_ret_scalar',
        # micro-ops by vector width
        'fp_ops_retired_by_width.scalar_uops_retired': 'fp_sclu',
        'fp_ops_retired_by_width.pack_128_uops_retired': 'fp_128u',
        'fp_ops_retired_by_width.pack_256_uops_retired': 'fp_256u',
        'fp_ops_retired_by_width.pack_512_uops_retired': 'fp_512u',
    }.get(key, key)

class Perf(Tool):
    def __init__(self, c):
        super(Perf, self).__init__(c, "perf")

    def setup(self, cfg):
        super(Perf, self).setup(cfg)

    def specific_setup(self, cfg):
        """Setup the perf event string and command based on the configuration passed via cfg."""
        check_perf_tool()
        check_perf_event_paranoid()
        self.vendor = get_cpu_vendor()
        perflist = get_perf_list()
        self._detect_fp(cfg, perflist)
        self._detect_imc_uncore(cfg, perflist)
        self.id_str = ",".join(self.id_list)
        # idea: sleep until perf/sleep gets terminated by a signal
        self.perf_command = "perf stat -x , -a -e {} {} --no-big-num sleep 86400".format(self.id_str, cfg[self.ttag]['agg-counts'])
        self.perf_subprocess_handle = None
        log.debug(self.perf_command)

    def spawn(self, cfg):
        """Launch instance of perf and run it in the background.
        """
        self.perf_subprocess_handle = util.popen(self.perf_command)
        log.debug(self.perf_subprocess_handle.poll() is None)

    def execute(self, cfg):
        """Join the perf command and return the data in a dictionary."""
        out = util.join_and_get_output(self.perf_subprocess_handle, signal_to_child=True)
        raw = util.get_ascii_lines(out)
        log.debug("perf.execute, raw: " + str(raw))
        return self._parse_perf_output(cfg, raw)



    def update_summary(self, cfg, outdata):
        """Update summary file with the perf overhead stats."""
        if cfg[self.ttag]['derived']['enable']:
            cm = cfg[self.ttag]['derived']['metrics']
            if cm['GFLOPS'] and cm['FP-VECTOR'] and cm['FP-SCALAR']:
                tot_gf = 0.
                sockets = 0
                # Retrieveing measured metrics for this iteration, and sum up with previous iterations
                for cpuid in outdata:
                    tot_gf += outdata[cpuid]['GFLOPS']
                    util.update_stat_acc(cfg, 'FP-VECTOR', outdata[cpuid]['FP-VECTOR'])
                    util.update_stat_acc(cfg, 'FP-SCALAR', outdata[cpuid]['FP-SCALAR'])
                    # AI is already computed correctly for the whole node,
                    # so no need to sum up multiple times
                    if (sockets == 0) and cm['ALGO-INT'] and ('ALGO-INT' in outdata[cpuid]):
                        util.update_stat_avg(cfg, 'AI', outdata[cpuid]['ALGO-INT'])
                    sockets += 1
                util.update_stat_avg(cfg, 'GF', tot_gf)
                # Computing average vectorization ratio
                v = pr.try_div(float(cfg['stats']['FP-VECTOR']),
                               float(cfg['stats']['FP-VECTOR'] +
                                     cfg['stats']['FP-SCALAR']))
                cfg['stats']['VEC_RATIO'] = round(v, 2)
                # Computing balance ratio for the socket with the least workload
                avg_socket_gf = pr.try_div(tot_gf, sockets)
                r = 1
                for cpuid in outdata:
                    r = pr.try_div(outdata[cpuid]['GFLOPS'], avg_socket_gf)
                    r = round(r, 2)
                    util.update_stat_min(cfg, 'SOCKET_BALANCE', r)

    def print_sample(self, cfg, outdata):
        """Printing data collected for perf services."""
        for cpuid in outdata:
            msg = []
            if bool(cpuid):
                msg.append(pr.keqv("cpu", cpuid))
            for key in outdata[cpuid]:
                if ("uncore_" not in key):
                    msg.append(pr.keqdv(key, outdata[cpuid]))
            log.prefixed_message(' '.join(msg), cfg, self.ttag)

    # class helper functions
    def _flops(self, cfg, data, cpuid):
        if (self.vendor == 'intel'):
            self._flops_intel(cfg, data, cpuid)
        elif (self.vendor == 'amd'):
            self._flops_amd(cfg, data, cpuid)
        else:
            raise NotImplementedError("unknown cpu vendor")

    def _flops_amd(self, cfg, data, cpuid):
        """Compute GFLOP rates from raw counter values."""
        d = data[cpuid]
        cfg_metrics = cfg[self.ttag]['derived']['metrics']
        # to avoid redundancy in the output we remove the following keys
        fp_ret_all = int(d.pop('fp_ret_all', 0))
        fp_ret_scalar = int(d.pop('fp_ret_scalar', 0))
        fp_ret_vector = fp_ret_all - fp_ret_scalar
        if fp_ret_vector < 0:
            fp_ret_vector = 0
        flops = fp_ret_all
        if cfg_metrics['GFLOPS']:
            FLOP = float(flops)
            interval = float(cfg['daemon']['loop_awake'])
            data[cpuid]['GFLOPS'] = pr.try_div(float(FLOP / Giga), interval)
        if cfg_metrics['FP-SCALAR']:
            data[cpuid]['FP-SCALAR'] = fp_ret_scalar
        if cfg_metrics['FP-VECTOR']:
            data[cpuid]['FP-VECTOR'] = fp_ret_vector

    def _flops_intel(self, cfg, data, cpuid):
        """Compute GFLOP rates from raw counter values."""
        d = data[cpuid]
        cfg_metrics = cfg[self.ttag]['derived']['metrics']
        # SCALAR
        if (('fp_s') in d):
            fp_s = int(d['fp_s'])
        else:
            fp_s = 0
        if (('fp_d') in d):
            fp_d = int(d['fp_d'])
        else:
            fp_d = 0
        # VECTOR SSE
        if (('fp_128s') in d):
            fp_128s = int(d['fp_128s'])
        else:
            fp_128s = 0
        if (('fp_128d') in d):
            fp_128d = int(d['fp_128d'])
        else:
            fp_128d = 0
        # VECTOR AVX
        if (('fp_256s') in d):
            fp_256s = int(d['fp_256s'])
        else:
            fp_256s = 0
        if (('fp_256d') in d):
            fp_256d = int(d['fp_256d'])
        else:
            fp_256d = 0
        # VECTOR AVX512
        if (('fp_512s') in d):
            fp_512s = int(d['fp_512s'])
        else:
            fp_512s = 0
        if (('fp_512d') in d):
            fp_512d = int(d['fp_512d'])
        else:
            fp_512d = 0
        if cfg_metrics['GFLOPS']:
            FLOP = float(1 * (fp_s + fp_d)
                       + 2 * (fp_128d)
                       + 4 * (fp_128s + fp_256d)
                       + 8 * (fp_256s + fp_512d)
                       + 16 * fp_512s)
            interval = float(cfg['daemon']['loop_awake'])
            data[cpuid]['GFLOPS'] = pr.try_div(float(FLOP / Giga), interval)
        if cfg_metrics['FP-SCALAR']:
            data[cpuid]['FP-SCALAR'] = int(fp_s + fp_d)
        if cfg_metrics['FP-VECTOR']:
            data[cpuid]['FP-VECTOR'] = int(fp_128s + fp_128d
                                         + fp_256s + fp_256d
                                         + fp_512s + fp_512d)

    def _mem_bw(self, cfg, data, cpuid):
        if (self.vendor == 'intel'):
            self._mem_bw_intel(cfg, data, cpuid)
        elif (self.vendor == 'amd'):
            self._mem_bw_amd(cfg, data, cpuid)
        else:
            raise NotImplementedError("unknown cpu vendor")

    def _mem_bw_intel(self, cfg, data, cpuid):
        """Compute derived estimation of memory bandwidth for Intel CPUs."""
        d = data[cpuid]
        if 'uncore_imc/data_reads/' in d:
            reads = float(d['uncore_imc/data_reads/'])
        elif 'uncore_imc_0/cas_count_read/' in d:
            reads = sum([float(v) for k, v in d.items() if 'cas_count_read' in k])
        else:
            reads = 0.
        if 'uncore_imc/data_writes/' in d:
            writes = float(d['uncore_imc/data_writes/'])
        elif 'uncore_imc_0/cas_count_write/' in d:
            writes = sum([float(v) for k, v in d.items() if 'cas_count_write' in k])
        else:
            writes = 0.
        # data is in MiB so transferring to GB
        total_transferred_gb = ((reads + writes) * 1024. * 1024.)/Giga
        interval = float(cfg['daemon']['loop_awake'])
        data[cpuid]['MEM-BW'] = pr.try_div(total_transferred_gb, interval)

    def _mem_bw_amd(self, cfg, data, cpuid):
        """Compute derived estimation of memory bandwidth for AMD CPUs.

        As of now this requires the uncore events supported by perf which requires new kernels.
        """
        pass

    def _aggregate(self, cfg, data):
        """Aggregate data derived from counters."""
        if cfg[self.ttag]['derived']['enable']:
            tot_gflops = 0.
            tot_membw = 0.
            tot_ai = 0.
            cfg_metrics = cfg[self.ttag]['derived']['metrics']
            for cpuid in data:
                d = data[cpuid]
                # log.debug(d)
                if cfg_metrics['IPC'] and 'cycles' in d and 'instructions' in d:
                    data[cpuid]['IPC'] = pr.try_div(float(d['instructions']), float(d['cycles']))
                if cfg_metrics['BR-MISS-RATIO'] and 'branches' in d and 'branch-misses' in d:
                    data[cpuid]['BR-MISS-RATIO'] = pr.try_div(float(d['branch-misses']), float(d['branches']))
                if cfg_metrics['CACHE-MISS-RATIO'] and 'cache-misses' in d and 'cache-references' in d:
                    data[cpuid]['CACHE-MISS-RATIO'] = pr.try_div(float(d['cache-misses']), float(d['cache-references']))
                if cfg_metrics['GFLOPS'] or cfg_metrics['FP-SCALAR'] or cfg_metrics['FP-VECTOR']:
                    self._flops(cfg, data, cpuid)
                if cfg_metrics['MEM-BW']:
                    self._mem_bw(cfg, data, cpuid)
                if cfg_metrics['GFLOPS']:
                    tot_gflops += data[cpuid]['GFLOPS']
                if cfg_metrics['MEM-BW'] and ('MEM-BW' in data[cpuid]):
                    tot_membw += data[cpuid]['MEM-BW']
            # ALGO-INT is the same (avg) for all sockets as it is hard to now which memory access belongs to whom
            if cfg_metrics['GFLOPS'] and cfg_metrics['MEM-BW'] and cfg_metrics['ALGO-INT'] and (tot_membw > 0.):
                tot_ai = pr.try_div(tot_gflops, tot_membw)
                for cpuid in data:
                    data[cpuid]['ALGO-INT'] = tot_ai
        # always return data dictionary, potentially extended by derived values above
        return data

    def _parse_perf_output(self, cfg, rawdata):
        """Obtain the names and numbers from perf output rawdata and return them in a dictionary."""
        # Data is a dictionary of dictionaries: each cpuid has its own dictionary
        outdata = {}
        # When using aggregate counts, counter value is not in the same position
        pos = 0
        cpuid = ""
        if cfg[self.ttag]['agg-counts']:
            pos = 2
        # catch the output of `sleep` that is written when `sleep` receives SIGINT
        if rawdata[0].startswith("sleep"):
            del rawdata[0]
        for line in rawdata:
            line = line.split(',')
            if (pos > 0):
                cpuid = line[0]
            if cpuid not in outdata:
                outdata[cpuid] = {}
            keypfx = line[pos + 2]
            if keypfx in self.id_list:
                key = self._nopostfix(keypfx, cfg)
                if '.' in line[pos]:
                    val = float(line[pos])
                else:
                    val = int(line[pos])
                if (self.vendor == "intel"):
                    key = map_counter_intel_skylake(key)
                elif (self.vendor == "amd"):
                    key = map_counter_amd_zen4(key)
                outdata[cpuid][key] = val
        return self._aggregate(cfg, outdata)

    def _postfix(self, key, cfg):
        """Prepending most of the perf counters with the 'key'.
        """
        if cfg[self.ttag]['postfix']:
            if ("power" not in key) and ("uncore" not in key):
                return str(key + ":" + str(cfg[self.ttag]['postfix']))
        return key

    def _nopostfix(self, key, cfg):
        """Removing prepanded 'key' from most of the perf counters.
        """
        if cfg[self.ttag]['postfix']:
            if (("power" not in key) and ("uncore" not in key)):
                return (key.split(":"))[0]
        return key

    def _detect_fp(self, cfg, rawdata):
        """Detect all fp events inside perf list."""
        if ('fp' in cfg[self.ttag]) and (cfg[self.ttag]['fp']['enable']) and (cfg[self.ttag]['fp']['autodetect']):
            for line in rawdata:
                words = line.split()
                for w in words:
                    if any(x in w for x in fp_patterns):
                        self.id_list.append(w)

    def _detect_imc_uncore(self, cfg, rawdata):
        """Detect all uncore_imc events inside perf list."""
        if (self.vendor == 'intel'):
            self._detect_imc_uncore_intel(cfg, rawdata)
        elif (self.vendor == 'amd'):
            # memory bandwidth counters are not yet available via perf for AMD
            pass
        else:
            pass

    def _detect_imc_uncore_intel(self, cfg, rawdata):
        """Detect all uncore_imc events inside perf list."""
        if ('uncore_imc' in cfg[self.ttag]) and (cfg[self.ttag]['uncore_imc']['enable']):
            uncore_imc_pattern = re.compile(r"^uncore_imc_\d+/.*$|^uncore_imc/.*$")
            cas_count_pattern = re.compile(r"^.*cas_count_read.*$|^.*cas_count_write.*$")
            for line in rawdata:
                words = line.split()
                for w in words:
                    if uncore_imc_pattern.match(w) and cas_count_pattern.match(w):
                        self.id_list.append(w)
