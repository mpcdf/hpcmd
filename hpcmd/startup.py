#!/usr/bin/env python
# -*- Mode: python; tab-width: 4; indent-tabs-mode:nil; coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 fileencoding=utf-8
#
# This file is part of the hpcmd (HPC monitoring daemon) package.
#
# Copyright (c)   Max Planck Computing and Data Facility, Garching, Germany
#                 hpcmd is written by Klaus Reuter and Luka Stanisic
#
# Released under the MIT License, see the file LICENSE.txt.

"""hpcmd --- HPC Monitoring Daemon, main program.

A a daemon that measures performance data periodically using various Linux
tools and writes the result data to syslog, stdout, or arbitrary files.
"""

import os
import platform
from . import main
from . import util


def startup():
    # parse command line arguments
    arg = util.cliarg_get()
    if "HPCMD_PROFILE" in os.environ:
        arg.profile = bool(os.environ["HPCMD_PROFILE"])
    if arg.profile:
        import cProfile
        import pstats
        profile = cProfile.Profile()
        profile.enable()
        main.main(arg)
        profile.disable()
        profile.create_stats()
        # write profile into a text file located in the home directory
        home_directory = os.path.expanduser("~")
        host_name = platform.node()
        time_stamp = util.get_timestamp()
        file_name = "hpcmd_" + host_name + "_" + time_stamp + ".profile"
        pro_file = os.path.join(home_directory, file_name)
        with open(pro_file, 'w') as fp:
            stats = pstats.Stats(profile, stream=fp)
            stats.sort_stats('time')
            stats.print_stats()
    else:
        main.main(arg)
